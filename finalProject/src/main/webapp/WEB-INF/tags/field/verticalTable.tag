<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ tag import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>

<jsp:directive.attribute name="task.id" required="true" description="task id" />
<jsp:directive.attribute name="task.taskName" required="true" description="task id" />
<jsp:directive.attribute name="task.dateDeadline" required="true" description="task id" />

          <div class="demo-card-square mdl-card mdl-shadow--2dp">
                          <div class="demo-card-square mdl-card__title mdl-card--expand">
                              <h2 class="mdl-card__title-text">
                                  <a
                                      href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId="+"${task.id}">
                                      <c:out value="${task.taskName}" /> </a>
                              </h2>
                          </div>

                          <div class="mdl-card__actions mdl-card--border">
                              <fmt:message key="button.project.findExecutor" />
                                             <form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
                                               <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                                               <input type="hidden" name="commandName" value="searchUserCommand"/>
                                                <input type="hidden" name="task.id" value="${task.id}"/>
                                                 <label class="mdl-button mdl-js-button mdl-button--icon" for="_searchField"+"${task.taskName}">
                                                   <i class="material-icons">search</i>
                                                 </label>
                                                 <div class="mdl-textfield__expandable-holder">
                                                   <input class="mdl-textfield__input" type="text" id="_searchField"+"${task.taskName}" name ="searchField">
                                                   <label class="mdl-textfield__label" for="_searchField+${task.taskName}">Expandable Input</label>
                                                 </div>
                                               </div>
                                             </form>
                              <fmt:message key="button.project.changeStatus" />
                              <button id="menu"+"${task.taskName}" class="mdl-button mdl-js-button mdl-button--icon">
                                  <i class="material-icons">more_vert</i>
                              </button>
                              <ul class="mdl-menu mdl-menu--top-right mdl-js-menu mdl-js-ripple-effect"
                                  for="menu"+"${task.taskName}">
                                  <li class="mdl-menu__item">
                                      <a
                                          href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId="+"${task.id}"+"&statusId=1">
                                          <fmt:message key="project.status.created" /></a>
                                  </li>
                                  <li class="mdl-menu__item">
                                      <a
                                          href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId="+"${task.id}"+"&statusId=2">
                                          <fmt:message key="project.status.active" /></a>
                                  </li>
                                  <li class="mdl-menu__item">
                                      <a
                                          href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId="+"${task.id}"+"&statusId=3">
                                          <fmt:message key="project.status.complete" /></a>
                                  </li>
                                  <li class="mdl-menu__item">
                                      <a
                                          href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId="+"${task.id}"+"&statusId=4">
                                          <fmt:message key="project.status.archived" /></a>
                                  </li>
                              </ul>
                          </div>

                      </div>
