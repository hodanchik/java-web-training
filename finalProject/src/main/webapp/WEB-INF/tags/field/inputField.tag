<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="fieldName" required="true" description="field name to display" />
<jsp:directive.attribute name="beanName" required="true" description="some object" />
<jsp:directive.attribute name="pattern" required="false" description="pattern string" />
<jsp:directive.attribute name="type" required="true" description="field type" />
<jsp:directive.attribute name="fieldPlace" required="true" description="place of field" />
<c:if test="${not empty fieldName and not empty beanName}">
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
     <c:set var="mdl-textfield__input" value="valid" />


        <p class="${valid}">
            <c:set var="bean" value="${requestScope[beanName]}" />
            <c:set var="inpId" value="input_${beanName}_${fieldName}" />

            <label class="mdl-textfield__label" for="${inpId}">
                <fmt:message key="${beanName}.field.${fieldName}" />:
            </label>
            <input class="mdl-textfield__input" id="${inpId}" name="${beanName}.${fieldName}" type="${type}"
                pattern="${pattern}" value="${bean[fieldName]}">


            <span class="mdl-textfield__error">
                <fmt:message key="error.${fieldPlace}.field.${fieldName}" />
             </span>
            <c:if test="${not empty validationErrors and not empty validationErrors[fieldName]}">
            <c:set var="error" value="${validationErrors[fieldName]}" />
                                <fmt:message key="${error}"/>
                     </c:if>
        </p>
    </div>
</c:if>