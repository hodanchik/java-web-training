<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:directive.attribute name="link" required="true" description="link" />
<jsp:directive.attribute name="buttonName" required="false" description="button name" />
<jsp:directive.attribute name="buttonNumberName" required="false" description="button number name" />

 <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
 href="${link}">
 <span><fmt:message key="${buttonName}"/></span>
  <span><${buttonNumberName}/></span>
 </a>
