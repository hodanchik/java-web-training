<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>

<c:choose>
   <c:when test="${not empty currentUser}">
   <nav:link link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.LOGOUT_CMD_NAME}" linkName="links.user.logout"/></a>
   <nav:link link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_USER_PAGE_CMD_NAME}" linkName="links.user.mainPage"/></a>
</c:when>
   <c:otherwise>
    <nav:link link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.REGISTER_VIEW_CMD_NAME}" linkName="links.user.registration"/></a>
    <nav:link link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.LOGIN_VIEW_CMD_NAME}" linkName="links.user.login"/></a>
   </c:otherwise>
</c:choose>

