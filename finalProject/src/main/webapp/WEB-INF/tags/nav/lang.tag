<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>


<span class="mdl-navigation">

<nav:link link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_LANG_CMD_NAME}&lang=ru" linkName="links.lang.ru" /><br/>
<nav:link link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_LANG_CMD_NAME}&lang=en" linkName="links.lang.en" /><br/>

</span>
