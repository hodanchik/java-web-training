<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:directive.attribute name="link" required="true" description="link" />
<jsp:directive.attribute name="linkName" required="true" description="link name" />

<a class="mdl-navigation__link" href="${link}">
<span><fmt:message key="${linkName}"/></span>
</a>
