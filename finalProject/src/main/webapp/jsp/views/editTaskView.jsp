<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<form name="editTask" method="POST" action="${pageContext.request.contextPath}/">
    <input type="hidden" name="commandName" value="editTaskCommand" />
     <input type="hidden" name="taskId" value="${task.id}" />
     <input type="hidden" name="task.executorId" value="${task.executorId}" />
      <input type="hidden" name="page" value="1"/>
     <field:inputField fieldName="taskName" beanName="task" fieldPlace = "createTask" type="text" pattern=".{1,35}" /><br/>
 <br/>
    <br/>
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
       <input class="mdl-textfield__input" type="date" pattern="^(((0[1-9]|[12]\d|3[01])\.(0[13578]|1[02])\.((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\.(0[13456789]|1[012])\.((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\.02\.((19|[2-9]\d)\d{2}))|(29\.02\.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$"
       id="dateDeadline" required = "true" name="task.dateDeadline" value="${task.dateDeadline}">
       <label class="mdl-textfield__label" for="dateDeadline"><fmt:message key="task.field.dateDeadline"/></label>
       <span class="mdl-textfield__error"><fmt:message key="error.createTask.field.dateDeadline"/></span>
        <br/>
           <br/>
       <c:if test="${not empty validationErrors and not empty validationErrors['dateDeadline']}">
            <c:set var="error" value="${validationErrors['dateDeadline']}" />
                                <fmt:message key="${error}"/>
                     </c:if>
     </div>
  <br/>
    <br />
    <fmt:message key="project.status" />:<br />
    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_task.statusId1">
        <input type="radio" id="_task.statusId1" class="mdl-radio__button" name="task.statusId" value="1"
            <c:choose>
        <c:when test="${task.statusId == 1}"> checked>
        </c:when>
        </c:choose>
        <span class="mdl-radio__label">
            <fmt:message key="project.status.created" /></span>
    </label>
    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_task.statusId2">
        <input type="radio" id="_task.statusId2" class="mdl-radio__button" name="task.statusId" value="2"
            <c:choose>
        <c:when test="${task.statusId == 2}"> checked>
        </c:when>
        </c:choose>
        <span class="mdl-radio__label">
            <fmt:message key="project.status.active" /></span>
    </label>
    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_task.statusId3">
        <input type="radio" id="_task.statusId3" class="mdl-radio__button" name="task.statusId" value="3"
            <c:choose>
        <c:when test="${task.statusId == 3}"> checked>
        </c:when>
        </c:choose>
        <span class="mdl-radio__label">
            <fmt:message key="project.status.complete" /></span>
    </label>
    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_task.statusId4">
        <input type="radio" id="_task.statusId4" class="mdl-radio__button" name="task.statusId" value="4"
            <c:choose>
        <c:when test="${task.statusId == 4}"> checked>
        </c:when>
        </c:choose>
        <span class="mdl-radio__label">
            <fmt:message key="project.status.archived" /></span>
    </label>
    <br />
    <br />
    <br />
    <br />
    <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit"
        value=<fmt:message key="button.project.editProject" /> />
</form>