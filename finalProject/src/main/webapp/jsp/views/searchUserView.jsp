<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>


 <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${taskId}&page=${1}"
  buttonName="button.task.returnTask" />
</br>
</br>

<form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
  <input type="hidden" name="commandName" value="searchUserCommand"/>
   <input type="hidden" name="taskId" value="${taskId}"/>
    <label class="mdl-button mdl-js-button mdl-button--icon" for="_searchField">
      <i class="material-icons">search</i>
    </label>
    <div class="mdl-textfield__expandable-holder">
      <input class="mdl-textfield__input" type="text" id="_searchField" name ="searchField">
      <label class="mdl-textfield__label" for="_searchField">Expandable Input</label>
    </div>
  </div>
</form>
<c:choose>
<c:when test="${not empty allFoundUsersList}">
<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>
 <fmt:message key="button.action.selectUser" var="selectUser"/>
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric"></th>
            <th><fmt:message key="user.field.login"/></th>
            <th><fmt:message key="user.field.firstName"/></th>
            <th><fmt:message key="user.field.lastName"/></th>
            <th><fmt:message key="user.field.email"/></th>
            <th><fmt:message key="user.field.phone"/></th>
            <th><fmt:message key="user.field.role"/></th>
            <th><fmt:message key="user.field.skillLevel"/></th>
            <th><fmt:message key="links.admin.action"/></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${allFoundUsersList}" var="user">
                  <tr>
                      <td class="mdl-data-table__cell--non-numeric">
                       <td><c:out value="${user.login}"/></td>
                      <td><c:out value="${user.firstName}"/></td>
                      <td><c:out value="${user.lastName}"/></td>
                      <td><c:out value="${user.email}"/></td>
                      <td><c:out value="${user.phone}"/></td>
                      <td><fmt:message key="user.field.role.${user.roleName}"/></td>
                      <td><fmt:message key="user.field.skillLevel.${user.skillLevelName}"/></td>
                      <td>
                        <form name="SelectUserForm" action="${pageContext.request.contextPath}/" method="POST">
                        <input type="hidden" name="commandName" value="selectUserForTask"/>
                        <input type="hidden" name="user.id" value="${user.id}"/>
                        <input class="mdl-button mdl-js-button" type="submit" value=${selectUser}/>
                        </form>
                       </td>
                   </tr>
  </c:forEach>

  </tbody>
  </table>
 </c:when>
 <c:otherwise>
 <h4><fmt:message key="message.search.usersNotFound"/> </h4>
 </c:otherwise>
 </c:choose>