<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--8-col">
        <form name="editUserForm" method="POST" action="${pageContext.request.contextPath}/">
            <input type="hidden" name="commandName" value="editUserCommand" />
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <fmt:message key="user.field.login" /> <input class="mdl-textfield__input" <input type="hidden"
                    name="user.login" id="_user.login" value="${user.login}">
                <input type="hidden" name="user.id" value="${user.id}" />
                <div>${user.login}</div>
            </div> <br />
            <field:inputField fieldName="firstName" beanName="user" fieldPlace="registration" type="text"
                pattern="^[_a-zA-Zа-яА-ЯёЁ ]{1,30}$" /><br />
            <field:inputField fieldName="lastName" beanName="user" fieldPlace="registration" type="text"
                pattern="^[_a-zA-Zа-яА-ЯёЁ ]{1,30}$" /><br />
            <field:inputField fieldName="email" beanName="user" fieldPlace="registration" type="text"
                pattern="^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\.[a-zA-Z]{2,6}$" /><br />
            <field:inputField fieldName="phone" beanName="user" fieldPlace="registration" type="text"
                pattern="^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$" /><br />

            <br />
            <fmt:message key="user.field.role" />:<br />
            <c:choose>
                <c:when test="${currentUser.role_id == 1}">
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.roleId1">
                        <input type="radio" id="_user.roleId1" class="mdl-radio__button" name="user.roleId" value="1"
                            <c:choose>
                        <c:when test="${user.role_id == 1}"> checked>
                 </c:when>
            </c:choose>
            <span class="mdl-radio__label">
                <fmt:message key="user.field.role.admin" /></span>
                 </c:when>
            </c:choose>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.roleId2">
                <input type="radio" id="_user.roleId2" class="mdl-radio__button" name="user.roleId" value="2" <c:choose>
                <c:when test="${user.role_id == 2}"> checked>
                </c:when>
                </c:choose>
                <span class="mdl-radio__label">
                    <fmt:message key="user.field.role.manager" /></span>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.roleId3">
                <input type="radio" id="_user.roleId3" class="mdl-radio__button" name="user.roleId" value="3" <c:choose>
                <c:when test="${user.role_id == 3}"> checked>
                </c:when>
                </c:choose>
                <span class="mdl-radio__label">
                    <fmt:message key="user.field.role.developer" /></span>
            </label>

            <br />
            <fmt:message key="user.field.skillLevel" />:<br />
            <c:choose>
                <c:when test="${currentUser.role_id == 1}">
                    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId1">
                        <input type="radio" id="_user.skillLevelId1" class="mdl-radio__button" name="user.skillLevelId"
                            value="1" <c:choose>
                        <c:when test="${user.skillLevel_id == 1}"> checked>
                        </c:when>
            </c:choose>
            <span class="mdl-radio__label">
                <fmt:message key="user.field.skillLevel.superadmin" /></span>
            </c:when>
            </c:choose>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId2">
                <input type="radio" id="_user.skillLevelId2" class="mdl-radio__button" name="user.skillLevelId"
                    value="2" <c:choose>
                <c:when test="${user.skillLevel_id == 2}"> checked>
                </c:when>
                </c:choose>
                <span class="mdl-radio__label">
                    <fmt:message key="user.field.skillLevel.junior" /></span>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId3">
                <input type="radio" id="_user.skillLevelId3" class="mdl-radio__button" name="user.skillLevelId"
                    value="3" <c:choose>
                <c:when test="${user.skillLevel_id == 3}"> checked>
                </c:when>
                </c:choose>
                <span class="mdl-radio__label">
                    <fmt:message key="user.field.skillLevel.middle" /></span>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId4">
                <input type="radio" id="_user.skillLevelId4" class="mdl-radio__button" name="user.skillLevelId"
                    value="4" <c:choose>
                    <c:when test="${user.skillLevel_id == 4}"> checked>
                    </c:when>
                </c:choose>
                <span class="mdl-radio__label">
                    <fmt:message key="user.field.skillLevel.senior" /></span>
            </label>
            <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId5">
                <input type="radio" id="_user.skillLevelId5" class="mdl-radio__button" name="user.skillLevelId"
                    value="5" <c:choose>
                <c:when test="${user.skillLevel_id == 5}"> checked>
                </c:when>
                </c:choose>
                <span class="mdl-radio__label">
                    <fmt:message key="user.field.skillLevel.lead" /></span>
            </label>
            <br />

            <fmt:message var="edit_label" key="button.admin.action.edit" />
            <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                type="submit" value="${edit_label}" />

        </form>
    </div>
    <div class="mdl-cell mdl-cell--4-col">
        <form name="changeUserPasswordForm" method="POST" action="${pageContext.request.contextPath}/">
            <input type="hidden" name="commandName" value="changeUserPasswordCommand" />
            <input type="hidden" name="user.id" value="${user.id}" />
            <field:inputField fieldName="password" beanName="user" fieldPlace="registration" type="password"
                pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" /><br />
            <br />
            <fmt:message var="changePassword_label" key="button.admin.action.changePassword" />
            <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                type="submit" value="${changePassword_label}" />
    </div>
</div>