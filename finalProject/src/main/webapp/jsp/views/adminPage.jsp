<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>

<fmt:message key="button.user.editProfile" var="editInformation"/>
<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--10-col"><fmt:message key="general.text.welcomeWord" /> ${currentUser.firstName}</div>
</div>
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--4-col">
     <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_OWN__PROJECT_LIST_CMD_NAME}" buttonName="button.user.viewOwnProject" />
        <br/>
        <br/>
     <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CREATE_PROJECT_VIEW_NAME}" buttonName="button.user.createNewProject" />
          <br/>
          <br/>
  </div>
 <div class="mdl-cell mdl-cell--4-col">
     <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_PROJECTS_CMD_NAME}" buttonName="button.user.viewAllProject" />
        <br/>
        <br/>
        <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_USERS_CMD_NAME}" buttonName="links.user.list"/>
         <br/>
         <br/>
        <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_TASKS_CMD_NAME}" buttonName="button.user.viewAllTasks" />
         <br/>
         <br/>
        <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=1" buttonName="button.user.viewAllComments" />

 </div>
  <div class="mdl-cell mdl-cell--4-col">
       <form name="editProfile" action="${pageContext.request.contextPath}/" method="GET">
        <input type="hidden" name="commandName" value="editUserView"/>
        <input type="hidden" name="user.id" value="${currentUser.id}"/>
        <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" value="${editInformation}"/>
       </form>
  </div>
</div>
