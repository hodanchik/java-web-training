<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>

<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--4-col">
    </div>
    <div class="mdl-cell mdl-cell--4-col">
        <h4>
            <fmt:message key="project.page.welcome" /> ${project.projectName}<h4>
    </div>
</div>
<div class="mdl-grid">
<div class=" mdl-cell mdl-cell--2-col">
            </div>
    <div class="mdl-cell mdl-cell--5-col">
       <strong> <fmt:message key="project.status" />:
        <fmt:message key="project.status.${project.statusName}" /></strong>
        </br>
        </br>
        <strong><fmt:message key="general.description" />: ${project.description}</strong>
        </br>
        </br>
    </div>
    <div class="mdl-cell mdl-cell--5-col">
        <c:choose>
            <c:when test="${currentUser.role_id<=2}">
            <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.EDIT_PROJECT_VIEW_NAME}&projectId=${project.id}"
             buttonName="button.project.editProject" />
             </c:when>
        </c:choose>
        </br>
         <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CREATE_TASK_VIEW_NAME}&projectId=${project.id}"
                     buttonName="button.project.createTask" />
      </div>
</div>

</br>
<c:choose>
    <c:when test="${not empty allTasksOfProject}">
        <div class="mdl-grid">
            <div class=" mdl-cell mdl-cell--2-col">
            </div>
            <div class=" mdl-cell mdl-cell--2-col">
             <strong>   <fmt:message key="project.status.created" /></strong>
            </div>
            <div class=" mdl-cell mdl-cell--2-col">
             <strong>   <fmt:message key="project.status.active" /></strong>
            </div>
            <div class=" mdl-cell mdl-cell--2-col">
              <strong>  <fmt:message key="project.status.complete" /></strong>
            </div>
            <div class="mdl-cell mdl-cell--2-col">
               <strong> <fmt:message key="project.status.archived" /></strong>
            </div>
            <div class="mdl-cell mdl-cell--2-col">
            </div>
        </div>
    </c:when>
</c:choose>
<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--2-col">
    </div>
    <div class="mdl-cell mdl-cell--2-col">
        <c:forEach var="task" items="${allTasksOfProject['1']}">
            <div class="demo-card-square mdl-card mdl-shadow--2dp">
              <div class="mdl-card__menu">

                <form name="deleteTaskForm" action="${pageContext.request.contextPath}/" method="POST">
                                <input type="hidden" name="commandName" value="deleteTask" />
                                <input type="hidden" name="taskId" value="${task.id}" />
                                 <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                  <i class="material-icons">highlight_off</i>
                </form>


              </div>
                <div class="demo-card-square mdl-card__title mdl-card--expand">
                    <h2 class="mdl-card__title-text">
                        <a
                            href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${task.id}&page=${1}">
                            <c:out value="${task.taskName}" /> </a>
                    </h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <fmt:message key="task.deadline" />:
                    <fmt:formatDate value="${task.dateDeadline}" type="date" />
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <fmt:message key="button.project.findExecutor" />
                    <form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                            <input type="hidden" name="commandName" value="searchUserCommand" />
                            <input type="hidden" name="taskId" value="${task.id}" />
                            <label class="mdl-button mdl-js-button mdl-button--icon"
                                for="_searchField+${task.taskName}">
                                <i class="material-icons">search</i>
                            </label>
                            <div class="mdl-textfield__expandable-holder">
                                <input class="mdl-textfield__input" type="text" id="_searchField+${task.taskName}"
                                    name="searchField">
                                <label class="mdl-textfield__label" for="_searchField+${task.taskName}">Expandable
                                    Input</label>
                            </div>
                        </div>
                    </form>

                    <fmt:message key="button.project.changeStatus" />
                    <button id="menu+${task.taskName}" class="mdl-button mdl-js-button mdl-button--icon">
                        <i class="material-icons">more_vert</i>
                    </button>
                    <ul class="mdl-menu mdl-menu--top-right mdl-js-menu mdl-js-ripple-effect"
                        for="menu+${task.taskName}">
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=1">
                                <fmt:message key="project.status.created"/></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=2">
                                 <fmt:message key="project.status.active" /> </a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=3">
                                <fmt:message key="project.status.complete" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=4">
                                <fmt:message key="project.status.archived" /></a>
                        </li>
                    </ul>
                </div>

            </div>
        </c:forEach>
    </div>

    <div class="mdl-cell mdl-cell--2-col">
        <c:forEach var="task" items="${allTasksOfProject['2']}">
            <div class="demo-card-square mdl-card mdl-shadow--2dp">
                <div class="mdl-card__menu">

                            <form name="deleteTaskForm" action="${pageContext.request.contextPath}/" method="POST">
                                            <input type="hidden" name="commandName" value="deleteTask" />
                                            <input type="hidden" name="taskId" value="${task.id}" />
                                             <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                              <i class="material-icons">highlight_off</i>
                            </form>


                          </div>
                <div class="demo-card-square mdl-card__title mdl-card--expand">
                    <h2 class="mdl-card__title-text">
                        <a
                            href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${task.id}&page=${1}">
                            <c:out value="${task.taskName}" /> </a>
                    </h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <fmt:message key="task.deadline" />:
                    <fmt:formatDate value="${task.dateDeadline}" type="date" />
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <fmt:message key="button.project.findExecutor" />
                    <form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                            <input type="hidden" name="commandName" value="searchUserCommand" />
                            <input type="hidden" name="taskId" value="${task.id}" />
                            <label class="mdl-button mdl-js-button mdl-button--icon"
                                for="_searchField+${task.taskName}">
                                <i class="material-icons">search</i>
                            </label>
                            <div class="mdl-textfield__expandable-holder">
                                <input class="mdl-textfield__input" type="text" id="_searchField+${task.taskName}"
                                    name="searchField">
                                <label class="mdl-textfield__label" for="_searchField+${task.taskName}">Expandable
                                    Input</label>
                            </div>
                        </div>
                    </form>
                    <fmt:message key="button.project.changeStatus" />
                    <button id="menu+${task.taskName}" class="mdl-button mdl-js-button mdl-button--icon">
                        <i class="material-icons">more_vert</i>
                    </button>
                    <ul class="mdl-menu mdl-menu--top-right mdl-js-menu mdl-js-ripple-effect"
                        for="menu+${task.taskName}">
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=1">
                                <fmt:message key="project.status.created" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=2">
                                <fmt:message key="project.status.active" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=3">
                                <fmt:message key="project.status.complete" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=4">
                                <fmt:message key="project.status.archived" /></a>
                        </li>
                    </ul>
                </div>

            </div>
        </c:forEach>
    </div>

    <div class="mdl-cell mdl-cell--2-col">
        <c:forEach var="task" items="${allTasksOfProject['3']}">
                    <div class="demo-card-square mdl-card mdl-shadow--2dp">
                        <div class="mdl-card__menu">

                                    <form name="deleteTaskForm" action="${pageContext.request.contextPath}/" method="POST">
                                                    <input type="hidden" name="commandName" value="deleteTask" />
                                                    <input type="hidden" name="taskId" value="${task.id}" />
                                                     <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                                      <i class="material-icons">highlight_off</i>
                                    </form>


                                  </div>
                <div class="demo-card-square mdl-card__title mdl-card--expand">
                    <h2 class="mdl-card__title-text">
                        <a
                            href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${task.id}&page=${1}">
                            <c:out value="${task.taskName}" /> </a>
                    </h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <fmt:message key="task.deadline" />:
                    <fmt:formatDate value="${task.dateDeadline}" type="date" />
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <fmt:message key="button.project.findExecutor" />
                    <form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                            <input type="hidden" name="commandName" value="searchUserCommand" />
                            <input type="hidden" name="taskId" value="${task.id}" />
                            <label class="mdl-button mdl-js-button mdl-button--icon"
                                for="_searchField+${task.taskName}">
                                <i class="material-icons">search</i>
                            </label>
                            <div class="mdl-textfield__expandable-holder">
                                <input class="mdl-textfield__input" type="text" id="_searchField+${task.taskName}"
                                    name="searchField">
                                <label class="mdl-textfield__label" for="_searchField+${task.taskName}">Expandable
                                    Input</label>
                            </div>
                        </div>
                    </form>
                    <fmt:message key="button.project.changeStatus" />
                    <button id="menu+${task.taskName}" class="mdl-button mdl-js-button mdl-button--icon">
                        <i class="material-icons">more_vert</i>
                    </button>
                    <ul class="mdl-menu mdl-menu--top-right mdl-js-menu mdl-js-ripple-effect"
                        for="menu+${task.taskName}">
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=1">
                                <fmt:message key="project.status.created" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=2">
                                <fmt:message key="project.status.active" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=3">
                                <fmt:message key="project.status.complete" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=4">
                                <fmt:message key="project.status.archived" /></a>
                        </li>
                    </ul>
                </div>

            </div>
        </c:forEach>
    </div>

    <div class="mdl-cell mdl-cell--2-col">
        <c:forEach var="task" items="${allTasksOfProject['4']}">
            <div class="demo-card-square mdl-card mdl-shadow--2dp">
                <div class="mdl-card__menu">

                            <form name="deleteTaskForm" action="${pageContext.request.contextPath}/" method="POST">
                                            <input type="hidden" name="commandName" value="deleteTask" />
                                            <input type="hidden" name="taskId" value="${task.id}" />
                                             <button class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                                              <i class="material-icons">highlight_off</i>
                            </form>


                          </div>
                <div class="demo-card-square mdl-card__title mdl-card--expand">
                    <h2 class="mdl-card__title-text">
                        <a
                            href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${task.id}&page=${1}">
                            <c:out value="${task.taskName}" /> </a>
                    </h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <fmt:message key="task.deadline" />:
                    <fmt:formatDate value="${task.dateDeadline}" type="date" />
                </div>
                <div class="mdl-card__actions mdl-card--border">
                    <fmt:message key="button.project.findExecutor" />
                    <form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                            <input type="hidden" name="commandName" value="searchUserCommand" />
                            <input type="hidden" name="taskId" value="${task.id}" />
                            <label class="mdl-button mdl-js-button mdl-button--icon"
                                for="_searchField+${task.taskName}">
                                <i class="material-icons">search</i>
                            </label>
                            <div class="mdl-textfield__expandable-holder">
                                <input class="mdl-textfield__input" type="text" id="_searchField+${task.taskName}"
                                    name="searchField">
                                <label class="mdl-textfield__label" for="_searchField+${task.taskName}">Expandable
                                    Input</label>
                            </div>
                        </div>
                    </form>
                    <fmt:message key="button.project.changeStatus" />
                    <button id="menu+${task.taskName}" class="mdl-button mdl-js-button mdl-button--icon">
                        <i class="material-icons">more_vert</i>
                    </button>

                    <ul class="mdl-menu mdl-menu--top-right mdl-js-menu mdl-js-ripple-effect"
                        for="menu+${task.taskName}">
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=1">
                                <fmt:message key="project.status.created" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=2">
                                <fmt:message key="project.status.active" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=3">
                                <fmt:message key="project.status.complete" /></a>
                        </li>
                        <li class="mdl-menu__item">
                            <a
                                href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CHANGE_TASK_STATUS_COMMAND}&taskId=${task.id}&statusId=4">
                                <fmt:message key="project.status.archived" /></a>
                        </li>
                    </ul>
                </div>

            </div>
        </c:forEach>
    </div>
    <div class="mdl-cell mdl-cell--2-col">
    </div>
</div>