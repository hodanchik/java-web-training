<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>

<h3><fmt:message key="error.page.serverError.message"/></h3>
</br>

<div>
    <img src="${pageContext.request.contextPath}/static/style/images/400.jpg" width="80%">
</div>
