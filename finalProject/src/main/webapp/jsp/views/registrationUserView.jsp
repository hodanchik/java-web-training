<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<c:if test="${not empty message}">
<fmt:message key="${message}"/>
</c:if>
<form name="registrationForm" method="POST" action="${pageContext.request.contextPath}/">
<input type="hidden" name="commandName" value="registerUserSave"/>
 <field:inputField fieldName="login" beanName="user" fieldPlace = "registration" type="text" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$" /><br/>
  <field:inputField fieldName="password" beanName="user"  fieldPlace = "registration" type="password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"/><br/>
  <field:inputField fieldName="firstName" beanName="user" fieldPlace = "registration" type="text" pattern="^[_a-zA-Zа-яА-ЯёЁ ]{1,30}$" /><br/>
   <field:inputField fieldName="lastName" beanName="user" fieldPlace = "registration" type="text" pattern="^[_a-zA-Zа-яА-ЯёЁ ]{1,30}$"/><br/>
   <field:inputField fieldName="email" beanName="user" fieldPlace = "registration" type="text" pattern="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$" /><br/>
   <field:inputField fieldName="phone" beanName="user"  fieldPlace = "registration" type="text" pattern="^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$"/><br/>
<fmt:message key="user.field.role"/>:<br/>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.roleId2">
    <input type="radio" id="_user.roleId2" class="mdl-radio__button" name="user.roleId" value="2" checked>
    <span class="mdl-radio__label"><fmt:message key="user.field.role.manager"/></span>
  </label>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.roleId3">
    <input type="radio" id="_user.roleId3" class="mdl-radio__button" name="user.roleId" value="3">
    <span class="mdl-radio__label"><fmt:message key="user.field.role.developer"/></span>
  </label>

 <br/><fmt:message key="user.field.skillLevel"/>:<br/>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId2">
    <input type="radio" id="_user.skillLevelId2" class="mdl-radio__button" name="user.skillLevelId" value="2" checked>
    <span class="mdl-radio__label"><fmt:message key="user.field.skillLevel.junior"/></span>
  </label>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId3">
    <input type="radio" id="_user.skillLevelId3" class="mdl-radio__button" name="user.skillLevelId" value="3">
    <span class="mdl-radio__label"><fmt:message key="user.field.skillLevel.middle"/></span>
  </label>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId4">
    <input type="radio" id="_user.skillLevelId4" class="mdl-radio__button" name="user.skillLevelId" value="4">
    <span class="mdl-radio__label"><fmt:message key="user.field.skillLevel.senior"/></span>
  </label>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_user.skillLevelId5">
    <input type="radio" id="_user.skillLevelId5" class="mdl-radio__button" name="user.skillLevelId" value="5">
    <span class="mdl-radio__label"><fmt:message key="user.field.skillLevel.lead"/></span>
  </label>
  <br/>
<fmt:message var="register_label" key="button.user.registration"/>
 <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" value="${register_label}" />

</form><hr/>