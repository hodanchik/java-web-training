<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>


<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric"></th>
      <fmt:message key="general.delete" var="delete"/>
            <th><fmt:message key="general.task"/></th>
            <th><fmt:message key="comment.date"/></th>
            <th><fmt:message key="comment.userLogin"/></th>
            <th><fmt:message key="comment.text" /></th>
            <th colspan=3><fmt:message key="links.admin.action"/></th>

    </tr>
  </thead>
<tbody>
    <c:forEach items="${allComments}" var="comment">
        <tr>
            <td class="mdl-data-table__cell--non-numeric">
            <td>
                <a
                    href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${comment.taskId}&page=${1}">
                    <c:out value="${comment.taskName}" /></a>
            </td>

            <td>
                <fmt:formatDate value="${comment.createDate}" type="both" />
            </td>

            <td>
                <a
                    href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_USER_PAGE_INFO}&infoUserId=${comment.creatorId}">
                    <c:out value="${comment.creatorLogin}" /> </a>
            </td>
            <td>
                <c:out value="${comment.text}" />
            </td>
             <td>
                    <form name="deleteCommentForm" action="${pageContext.request.contextPath}/" method="POST">
                    <input type="hidden" name="commandName" value="deleteComment"/>
                    <input type="hidden" name="commentId" value="${comment.id}"/>
                    <input type="hidden" name="page" value="${page}"/>
                    <input class="mdl-button mdl-js-button" type="submit" value=${delete}>
                    </form>
                  </td>
            </td>
        </tr>
    </c:forEach>


</tbody>
</table>

<c:if  test="${maxPage > 1}">

<c:choose>
  <c:when test="${page >1 }">
         <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=${page-1}"><fmt:message key="button.general.pagination.back"/></a>
   </c:when>
</c:choose>
<c:if test="${page > 1}">
 <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=${1}">1</a>
 ...            <c:forEach begin="${page}" end="${page+2}" var="p">
                  <c:if test="${p < maxPage}">
               <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=${p}">${p}</a>
                </c:if>
              </c:forEach>
</c:if>
<c:if test ="${page == 1}">
  <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=${page}">${page}</a>
</c:if>
  <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=${maxPage}">${maxPage}</a>

<c:choose>
    <c:when test="${page != maxPage}">
    <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME}&page=${page+1}"><fmt:message key="button.general.pagination.next"/></a>
     </c:when>
</c:choose>
</c:if>