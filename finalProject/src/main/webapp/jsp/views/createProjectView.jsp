<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<form name="registrationProject" method="POST" action="${pageContext.request.contextPath}/">
<input type="hidden" name="commandName" value="createProject"/>

<field:inputField fieldName="projectName" beanName="project" fieldPlace = "createProject" type="text" pattern=".{1,45}" /><br/>
<field:inputField fieldName="description" beanName="project"  fieldPlace = "createProject" type="text" pattern=".{1,350}"/><br/>
  </br>
  </br>
  </br>
 <fmt:message key="general.create" var="create"/>
<br/><input type="submit" value=${create}>
</form><hr/>

