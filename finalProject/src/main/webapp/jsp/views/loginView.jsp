<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<c:if test="${not empty message}">
<fmt:message key="${message}"/>
</c:if>

<form name="loginForm" method="POST" action="${pageContext.request.contextPath}/">
<input type="hidden" name="commandName" value="loginUser"/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="text" id="login" name="user.login" value="${user.login}">
    <label class="mdl-textfield__label" for="sample3"><fmt:message key="user.field.login" /></label>
  </div>
    <br/>
    <br/>
  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
    <input class="mdl-textfield__input" type="password" id="password" name="user.password" value="${user.login}">
    <label class="mdl-textfield__label" for="sample3"><fmt:message key="user.field.password" /></label>
  </div>
  <br/>
<fmt:message var="login_label" key="button.user.login"/>
<input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" value="${login_label}" />
</form><hr/>
</body></html>