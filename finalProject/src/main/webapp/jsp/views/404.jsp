<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3><fmt:message key="error.page.forbidden.message"/></h3>
</br>
 <nav:button link="${pageContext.request.contextPath}"
  buttonName="links.user.mainPage" />

<div>
    <img src="${pageContext.request.contextPath}/static/style/images/404.jpg" width="80%">
</div>

