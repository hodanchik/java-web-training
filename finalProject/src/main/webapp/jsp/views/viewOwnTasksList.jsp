<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>


<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric"></th>
             <fmt:message key="general.name" var="name"/>
             <fmt:message key="project.field.projectName" var="projectName"/>
             <fmt:message key="task.deadline" var="deadline"/>
             <fmt:message key="project.status" var="status"/>

            <th><abbr title="${name}">${name}</abbr></th>
            <th><abbr title="${projectName}">${projectName}</abbr></th>
            <th><abbr title="${deadline}">${deadline}</abbr></th>
            <th><abbr title="${status}">${status}</abbr></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${ownTasksList}" var="task">

                  <tr>
                      <td class="mdl-data-table__cell--non-numeric">
                     <td>
                     <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${task.id}&page=${1}">
                      <c:out value="${task.taskName}" /></a>
                     </td>
                     <td>
                           <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.PROJECT_PAGE_NAME}&projectId=${task.projectId}">
                           <c:out value="${task.projectName}"/>
                     </td>
                     </a>
                      <td><c:out value="${task.dateDeadline}"/></td>
                      <td><c:out value="${task.statusName}"/></td>
                  </tr>
              </c:forEach>

  </tbody>
</table>
