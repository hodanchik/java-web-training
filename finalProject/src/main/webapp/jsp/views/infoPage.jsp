<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="mdl-grid">
    <div class=" mdl-cell mdl-cell--1-col">
    </div>
    <div class=" mdl-cell mdl-cell--4-col">
        <strong>  <fmt:message key="user.field.login" /> :  ${infoUser.login} </strong>
        </br></br>
        <strong> <fmt:message key="user.field.firstName" />:  ${infoUser.firstName} </strong>
        </br></br>
        <strong>  <fmt:message key="user.field.lastName" />:  ${infoUser.lastName} </strong>
        </br></br>
        <strong>  <fmt:message key="user.field.email" />:  ${infoUser.email} </strong>
    </div>
    <div class=" mdl-cell mdl-cell--4-col">
        <strong>  <fmt:message key="user.field.phone" />:  ${infoUser.phone} </strong>
        </br></br>
        <strong>  <fmt:message key="user.field.role" />: <fmt:message key="user.field.role.${infoUser.roleName}"/> </strong>
        </br></br>
        <strong>  <fmt:message key="user.field.skillLevel" />: <fmt:message key="user.field.skillLevel.${infoUser.skillLevelName}"/> </strong>
        </br></br>
        <strong>  <fmt:message key="user.field.account.status" />: <fmt:message key="user.field.account.status.${infoUser.accStatusName}"/> </strong>
    </div>
</div>