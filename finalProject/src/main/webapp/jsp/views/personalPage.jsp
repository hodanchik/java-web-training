<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>

<fmt:message key="button.user.editProfile" var="editInformation"/>
<fmt:message key="button.user.viewOwnTask" var="viewMyTasks"/>
<div class="mdl-grid">
  <div class="mdl-cell mdl-cell--10-col"><fmt:message key="general.text.welcomeWord"/> ${currentUser.firstName}</div>
<c:choose>
   <c:when test="${currentUser.role_id == 2}">
   <div class="mdl-cell mdl-cell--4-col">
   <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_OWN__PROJECT_LIST_CMD_NAME}" buttonName="button.user.viewOwnProject" />
     </br>
     </br>
   <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_OWN_TASKS_LIST_CMD_NAME}" buttonName="button.user.viewOwnTask" />
  </div>
  </br>
  <div class="mdl-cell mdl-cell--4-col">
  <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.CREATE_PROJECT_VIEW_NAME}" buttonName="button.user.createNewProject" />
   </div>
  </c:when>
   <c:otherwise>
     <div class="mdl-cell mdl-cell--4-col">
       <nav:button link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_OWN_TASKS_LIST_CMD_NAME}" buttonName="button.user.viewOwnTask" />
     </div>
 </c:otherwise>
 </c:choose>
  </br>
<div class="mdl-cell mdl-cell--4-col">
   <form name="editProfile" action="${pageContext.request.contextPath}/" method="GET">
   <input type="hidden" name="commandName" value="editUserView"/>
   <input type="hidden" name="user.id" value="${currentUser.id}"/>
    <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" value="${editInformation}"/>
    </form>
</div>
</div>
</body>
</html>