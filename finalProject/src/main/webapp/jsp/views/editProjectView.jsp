<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<form name="editProject" method="POST" action="${pageContext.request.contextPath}/">
<input type="hidden" name="commandName" value="editProjectCommand"/>
<input type="hidden" name="projectId" value="${project.id}"/>

<field:inputField fieldName="projectName" beanName="project" fieldPlace = "createProject" type="text" pattern=".{1,45}" /><br/>
<field:inputField fieldName="description" beanName="project"  fieldPlace = "createProject" type="text" pattern=".{1,350}"/><br/>
<br/>
<br/>
<fmt:message key="project.status"/>:<br/>
 <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_project.statusId1">
    <input type="radio" id="_project.statusId1" class="mdl-radio__button" name="project.statusId" value="1"
     <c:choose>
     <c:when test="${project.statusId == 1}"> checked>
     </c:when>  </c:choose>
    <span class="mdl-radio__label"><fmt:message key="project.status.created"/></span>
  </label>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_project.statusId2">
    <input type="radio" id="_project.statusId2" class="mdl-radio__button" name="project.statusId" value="2"
     <c:choose>
     <c:when test="${project.statusId == 2}"> checked>
     </c:when>  </c:choose>
    <span class="mdl-radio__label"><fmt:message key="project.status.active"/></span>
  </label>
  <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_project.statusId3">
    <input type="radio" id="_project.statusId3" class="mdl-radio__button" name="project.statusId" value="3"
    <c:choose>
     <c:when test="${project.statusId == 3}"> checked>
     </c:when>  </c:choose>
    <span class="mdl-radio__label"><fmt:message key="project.status.complete"/></span>
  </label>
    <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="_project.statusId4">
      <input type="radio" id="_project.statusId4" class="mdl-radio__button" name="project.statusId" value="4"
      <c:choose>
       <c:when test="${project.statusId == 4}"> checked>
       </c:when>  </c:choose>
      <span class="mdl-radio__label"><fmt:message key="project.status.archived"/></span>
    </label>
<br/>
<br/>
<br/>
<br/>
 <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" type="submit" value=<fmt:message key="button.project.editProject" /> />
</form>
