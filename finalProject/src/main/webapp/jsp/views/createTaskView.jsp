<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="field" tagdir="/WEB-INF/tags/field" %>

<form name="createTask" method="POST" action="${pageContext.request.contextPath}/">
 <input type="hidden" name="commandName" value="createTask"/>
 <input type="hidden" name="page" value="1"/>
  <field:inputField fieldName="taskName" beanName="task" fieldPlace = "createTask" type="text" pattern=".{1,35}" /><br/>
   </br>
      </br>
     <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
       <input class="mdl-textfield__input" type="date" pattern="^(((0[1-9]|[12]\d|3[01])\.(0[13578]|1[02])\.((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\.(0[13456789]|1[012])\.((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\.02\.((19|[2-9]\d)\d{2}))|(29\.02\.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$"
       id="dateDeadline" required = "true" name="task.dateDeadline" value="${date}">
       <label class="mdl-textfield__label" for="dateDeadline"><fmt:message key="task.field.dateDeadline"/></label>
       <span class="mdl-textfield__error"><fmt:message key="error.createTask.field.dateDeadline"/></span>
        <br/>
           <br/>
       <c:if test="${not empty validationErrors and not empty validationErrors['dateDeadline']}">
            <c:set var="error" value="${validationErrors['dateDeadline'] }" />
                                <fmt:message key="${error}"/>
                     </c:if>
     </div>
  </br>
   <input type="hidden" name="projectId" value="${currentProject.id}"/>
  <br/><input type="submit" value=<fmt:message key="general.create"/>>
</form>

