<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>

<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--6-col">
        <fmt:message key="task.page.welcome" />: ${currentTask.taskName}
        </br>
        </br>
        <fmt:message key="project.status" />:
        <fmt:message key="project.status.${currentTask.statusName}" />
        </br>
        </br>
        <fmt:message key="task.deadline" />:
        <fmt:formatDate value="${currentTask.dateDeadline}" type="date" />
        </br>
        </br>
    </div>
    <div class="mdl-cell mdl-cell--4-col">
        <fmt:message key="task.field.executor" />:
        <a
            href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_USER_PAGE_INFO}&infoUserId=${currentTask.executorId}">
            <c:out value="${currentTask.executorLogin}" /></a>
        </br>
        </br>
        <fmt:message key="button.project.findExecutor" />
        <form name="searchForm" method="GET" action="${pageContext.request.contextPath}/">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                <input type="hidden" name="commandName" value="searchUserCommand" />
                <input type="hidden" name="taskId" value="${currentTask.id}" />
                <label class="mdl-button mdl-js-button mdl-button--icon" for="_searchField+${currentTask.taskName}">
                    <i class="material-icons">search</i>
                </label>
                <div class="mdl-textfield__expandable-holder">
                    <input class="mdl-textfield__input" type="text" id="_searchField+${currentTask.taskName}"
                        name="searchField">
                    <label class="mdl-textfield__label" for="_searchField+${currentTask.taskName}">Expandable
                        Input</label>
                </div>
            </div>
        </form>
    </div>
    <div class="mdl-cell mdl-cell--2-col">
        <fmt:message key="button.task.editTask" var="editTask" />
        <nav:button
                    link="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.PROJECT_PAGE_NAME}&projectId=${currentTask.projectId}"
                    buttonName="button.comment.returnProject" />
                     </br>
                     </br>
        <c:choose>
            <c:when test="${currentUser.role_id<=2}">
                <form name="editTaskForm" action="${pageContext.request.contextPath}/" method="GET">
                    <input type="hidden" name="commandName" value="editTaskView" />
                    <input type="hidden" name="taskId" value="${currentTask.id}" />
                    <input class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"
                    type="submit" value="${editTask}">
                </form>
           </c:when>
        </c:choose>
    </div>
</div>

<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--4-col">
        <fmt:message key="button.comment.addComment" />
        <form name="createComment" method="POST" action="${pageContext.request.contextPath}/">
            <input type="hidden" name="commandName" value="createComment" />
            <input type="hidden" name="taskId" value="${currentTask.id}" />
            <input type="hidden" name="page" value="${maxPage}"/>
           <div class="mdl-textfield mdl-js-textfield">
                <textarea class="mdl-textfield__input" type="text" rows="3" maxrows="24" name="comment.text"
                    id="_comment.text" maxlength="800"></textarea>
                <label class="mdl-textfield__label" for="_comment.text">
                    <fmt:message key="comment.field.nameField"/></label>
            </div>
            <input type="submit" value=<fmt:message key="button.comment.addComment" />>
        </form>
     </div>
<div class="mdl-cell mdl-cell--8-col">
        <c:choose>
            <c:when test="${not empty allCommentByTask}">
                <fmt:message key="comment.allComment" />
                <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric"></th>
                            <th>
                                <fmt:message key="comment.date" />
                            </th>
                            <th>
                                <fmt:message key="comment.userLogin" />
                            </th>
                            <th>
                                <fmt:message key="comment.text" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${allCommentByTask}" var="comment">
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">
                                <td>
                                    <fmt:formatDate value="${comment.createDate}" type="both" />
                                </td>
                                <td>
                                    <a
                                        href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.VIEW_USER_PAGE_INFO}&infoUserId=${comment.creatorId}">
                                        <c:out value="${comment.creatorLogin}" />

                                </a>
                                 </td>
                                 <td>
                                 <div class=".book">
                                 <c:out value="${comment.text}" />
                                </div>
                                </td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>


<c:if  test="${maxPage > 1}">

<c:choose>
  <c:when test="${page >1 }">
         <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${currentTask.id}&page=${page-1}"><fmt:message key="button.general.pagination.back"/></a>
  </c:when>
</c:choose>

<c:if test="${page > 1}">
        <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${currentTask.id}&page=${1}">1</a>
        ...
              <c:forEach begin="${page}" end="${page+2}" var="p">
                  <c:if test="${p < maxPage}">
                <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${currentTask.id}&page=${p}">${p}</a>
                  </c:if>
              </c:forEach>
</c:if>
<c:if test ="${page == 1}">
  <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${currentTask.id}&page=${page}">${page}</a>
</c:if>
 <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${currentTask.id}&page=${maxPage}">${maxPage}</a>

<c:choose>
    <c:when test="${page != maxPage}">
    <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${currentTask.id}&page=${page+1}"><fmt:message key="button.general.pagination.next"/></a>
    </c:when>
</c:choose>

</c:if>







            </c:when>
        </c:choose>


    </div>
</div>