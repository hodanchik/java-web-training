<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<body>
<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
    </c:otherwise>
</c:choose>
<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>
 <fmt:message key="button.admin.action.edit" var="editUser"/>
 <fmt:message key="button.admin.action.block" var="blockUser"/>
 <fmt:message key="button.admin.action.unblock" var="unblockUser"/>
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric"></th>
            <th><fmt:message key="user.field.login"/></th>
            <th><fmt:message key="user.field.firstName"/></th>
            <th><fmt:message key="user.field.lastName"/></th>
            <th><fmt:message key="user.field.email"/></th>
            <th><fmt:message key="user.field.phone"/></th>
            <th><fmt:message key="user.field.role"/></th>
            <th><fmt:message key="user.field.skillLevel"/></th>
            <th><fmt:message key="user.field.account.status"/></th>
           <th colspan=3><fmt:message key="links.admin.action"/></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${allUsersList}" var="user">
                  <tr>
                      <td class="mdl-data-table__cell--non-numeric">
                       <td><c:out value="${user.login}"/></td>
                      <td><c:out value="${user.firstName}"/></td>
                      <td><c:out value="${user.lastName}"/></td>
                      <td><c:out value="${user.email}"/></td>
                      <td><c:out value="${user.phone}"/></td>
                      <td><fmt:message key="user.field.role.${user.roleName}"/></td>
                      <td><fmt:message key="user.field.skillLevel.${user.skillLevelName}"/></td>
                       <td><fmt:message key="user.field.account.status.${user.accStatusName}"/></td>

<td>
 <c:if test = "${user.role_id !=1}">
                            <form name="editUserForm" action="${pageContext.request.contextPath}/" method="GET">
                            <input type="hidden" name="commandName" value="editUserView"/>
                            <input type="hidden" name="user.id" value="${user.id}"/>
                            <input class="mdl-button mdl-js-button" type="submit" value=${editUser}/>
                            </form>
 </td>

 <c:choose>
     <c:when test="${user.accStatusId==1}">
                            <td>
                            <form name="deleteUserForm" action="${pageContext.request.contextPath}/" method="POST">
                            <input type="hidden" name="commandName" value="blockUser"/>
                              <input type="hidden" name="user.id" value="${user.id}"/>
                              <input class="mdl-button mdl-js-button" type="submit" value=${blockUser}/>
                             </form>
                             </td>
     </c:when>
     <c:otherwise>
      <td>
            <form name="deleteUserForm" action="${pageContext.request.contextPath}/" method="POST">
            <input type="hidden" name="commandName" value="unBlockUser"/>
            <input type="hidden" name="user.id" value="${user.id}"/>
            <input class="mdl-button mdl-js-button" type="submit" value=${unblockUser}/>
            </form>
      </td>
     </c:otherwise>
     </c:choose>
      </c:if>
                  </tr>
              </c:forEach>

  </tbody>
</table>
</body>
</html>