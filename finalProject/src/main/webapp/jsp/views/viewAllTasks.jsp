<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>

<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric"></th>
       <fmt:message key="general.delete" var="delete"/>
            <th><fmt:message key="general.name"/></th>
            <th><fmt:message key="project.field.projectName"/></th>
            <th><fmt:message key="task.deadline"/></th>
            <th><fmt:message key="project.status" /></th>
            <th><fmt:message key="task.field.executor"/></th>
            <th colspan=3><fmt:message key="links.admin.action"/></th>

    </tr>
  </thead>
  <tbody>
  <c:forEach items="${allTasksList}" var="task">

                  <tr>
                      <td class="mdl-data-table__cell--non-numeric">
                     <td>
                       <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.TASK_PAGE_NAME}&taskId=${task.id}&page=${1}">
                                         <c:out value="${task.taskName}" /></a>
                     </td>
                     <td>
                           <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.PROJECT_PAGE_NAME}&projectId=${task.projectId}">
                           <c:out value="${task.projectName}"/>
                     </td>
                     </a>
                      <td><fmt:formatDate value="${task.dateDeadline}" type="both"/></td>
                      <td><fmt:message key="project.status.${task.statusName}" /></td>
                      <td>
                      <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.EDIT_USER_VIEW}&user.id=${task.executorId}">
                      <c:out value="${task.executorLogin}"/>
                       </td>
                        <fmt:message key="button.task.editTask" var="editTask" />
                          <td>
                               <form name="deleteTaskForm" action="${pageContext.request.contextPath}/" method="POST">
                               <input type="hidden" name="commandName" value="deleteTask"/>
                               <input type="hidden" name="taskId" value="${task.id}"/>
                                <input class="mdl-button mdl-js-button" type="submit" value=${delete}>
                               </form>
                             </td>
                    </tr>
              </c:forEach>

  </tbody>
</table>