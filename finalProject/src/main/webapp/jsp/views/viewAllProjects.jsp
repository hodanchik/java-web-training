<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>


<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
  <thead>
    <tr>
      <th class="mdl-data-table__cell--non-numeric"></th>
            <fmt:message key="project.field.projectName" var="projectName"/>
            <fmt:message key="project.field.description" var="projectDescription"/>
            <fmt:message key="project.status" var="projectStatus"/>
            <fmt:message key="button.project.editProject" var="editProject"/>
            <fmt:message key="button.project.deleteProject" var="deleteProject"/>
            <th><abbr title="${projectName}">${projectName}</abbr></th>
            <th><abbr title="${projectDescription}">${projectDescription}</abbr></th>
            <th><abbr title="${projectStatus}">${projectStatus}</abbr></th>
            <th colspan=3><fmt:message key="links.admin.action"/></th>
    </tr>
  </thead>
  <tbody>
  <c:forEach items="${allProjectsList}" var="project">

    <tr>
                      <td class="mdl-data-table__cell--non-numeric">
                       <td>
                        <a href="?${ApplicationConstants.CMD_REQ_PARAMETER}=${ApplicationConstants.PROJECT_PAGE_NAME}&projectId=${project.id}">
                       <c:out value="${project.projectName}"/></td>
                       </a>
                      <td><c:out value="${project.description}"/></td>
                      <td><c:out value="${project.statusName}"/></td>
      <td>
        <form name="deleteProjectForm" action="${pageContext.request.contextPath}/" method="POST">
        <input type="hidden" name="commandName" value="deleteProject"/>
        <input type="hidden" name="projectId" value="${project.id}"/>
        <input class="mdl-button mdl-js-button" type="submit" value=${deleteProject}/>
        </form>
      </td>
    </tr>
  </c:forEach>

  </tbody>
</table>
</body>