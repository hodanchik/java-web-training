<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/nav" %>
<%@ page import="by.epam.khadanovich.tracker.application.ApplicationConstants" %>

<!DOCTYPE html>
<html>

<head>
	<title>TASK TRACKER</title>
	 <link rel="stylesheet" href="static/style/material.min.css">
        <link rel="stylesheet" href="static/style/icons.css">
        <script src="static/style/material.min.js"></script>

</head>
<body>
<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>
<div class="demo-layout-transparent mdl-layout mdl-js-layout">
  <header class="mdl-layout__header mdl-layout__header--scroll">
   <div class="mdl-layout__header-row">
    <span class="mdl-layout-title"><h2><fmt:message key="app.name"/></h2></span>
                  <div class="mdl-layout-spacer"></div>
 <nav class="mdl-navigation">
                <nav:navLinks/>
                <nav:lang/>
 </nav>

              </div>
  </header>
  <div class="mdl-layout__drawer">
  <nav class="mdl-navigation">
                 <nav:navLinks/>
    </nav>
  </div>
 <main class="mdl-layout__content">
            <div class="page-content">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--1-col">
                    </div>
                    <div class="mdl-cell mdl-cell--10-col">
                        <c:choose>
                            <c:when test="${not empty viewName}">
                                <jsp:include page="views/${viewName}.jsp" />
                            </c:when>
                            <c:otherwise>
                                <div class="mdl-grid">
                                    <div class="mdl-cell mdl-cell--12-col">
                                      <h4> <fmt:message key="app.welcomeText"/></h4>
                                    </div>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                    <div class="mdl-cell mdl-cell--1-col">
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>