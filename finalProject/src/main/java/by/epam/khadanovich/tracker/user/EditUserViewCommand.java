package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = EDIT_USER_VIEW)
public class EditUserViewCommand implements Command {

    private final static Logger LOG = Logger.getLogger(EditUserViewCommand.class);
    private SecurityService securityService;
    private UserService userService;

    public EditUserViewCommand(SecurityService securityService, UserService userService) {
        this.securityService = securityService;
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            long userId = Long.parseLong(req.getParameter("user.id"));
            long currentUserId = (Long) req.getSession().getAttribute("user.id");
            if (securityService.canAccessToUserPage(currentUserId, userId)) {
                UserDto userForEdit = userService.getById(userId);
                req.setAttribute("user", userForEdit);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_USER_VIEW);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            } else {
                LOG.info("Command EditUserViewCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Edit user view error", e);
            throw new CommandException(e);
        }
    }
}
