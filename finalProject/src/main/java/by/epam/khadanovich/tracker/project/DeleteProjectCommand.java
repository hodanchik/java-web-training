package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.DELETE_PROJECT_COMMAND;

@Bean(name = DELETE_PROJECT_COMMAND)
public class DeleteProjectCommand implements Command {
    private final static Logger LOG = Logger.getLogger(DeleteProjectCommand.class);
    private ProjectService projectService;

    public DeleteProjectCommand(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long projectId = Long.parseLong(req.getParameter("projectId"));
            projectService.delete(projectId);
            String location = req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_ALL_PROJECTS_CMD_NAME;
            RequestUtil.sendRedirect(resp, location);
        } catch (ServiceException e) {
            LOG.error("Failed delete project", e);
            throw new CommandException("Failed delete project", e);
        }
    }
}

