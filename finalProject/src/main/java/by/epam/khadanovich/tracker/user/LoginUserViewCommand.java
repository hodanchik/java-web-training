package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = LOGIN_VIEW_CMD_NAME)
public class LoginUserViewCommand implements Command {
    private final static Logger LOG = Logger.getLogger(LoginUserCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        req.setAttribute("message", req.getAttribute("message"));
        req.setAttribute(VIEWNAME_REQ_PARAMETER, LOGIN_VIEW_CMD_NAME);
        RequestUtil.forward(req, resp, MAIN_LAYOUT);
    }
}
