package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.dao.BaseDao;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.UserDto;

import java.util.List;

public interface UserDao extends BaseDao<UserDto, Long> {

    UserDto loginUser(UserDto userDto) throws DaoException;

    long saveContact(UserDto userDto) throws DaoException;

    boolean checkLoginUnique(UserDto userDto) throws DaoException;

    boolean block(Long userId) throws DaoException;

    boolean unblock(Long userId) throws DaoException;

    UserDto changePassword(UserDto dto)throws DaoException;

    UserDto getInfoById(Long id) throws DaoException;

    List<UserDto> find(String criteria) throws DaoException;

}
