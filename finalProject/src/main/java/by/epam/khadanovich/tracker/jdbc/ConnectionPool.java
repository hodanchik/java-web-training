package by.epam.khadanovich.tracker.jdbc;

import java.sql.Connection;

public interface ConnectionPool {
    Connection getConnection();
    void closeAllConnection();
}
