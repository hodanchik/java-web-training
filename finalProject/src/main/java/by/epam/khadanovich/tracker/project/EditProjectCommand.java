package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = EDIT_PROJECT_CMD)
public class EditProjectCommand implements Command {
    private final static Logger LOG = Logger.getLogger(EditProjectCommand.class);
    private ProjectService projectService;
    private ProjectValidator projectValidator;


    public EditProjectCommand(ProjectService projectService, ProjectValidator projectValidator) {
        this.projectService = projectService;
        this.projectValidator = projectValidator;

    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        Long projectId = Long.parseLong(req.getParameter("projectId"));
        String projectName = req.getParameter("project.projectName");
        String projectDescription = req.getParameter("project.description");
        int projectStatusId = Integer.parseInt(req.getParameter("project.statusId"));
        ProjectDto projectDto = new ProjectDto(projectId, projectName, projectDescription, projectStatusId);
        ValidationResult validate = projectValidator.validate(req);
        if (validate.isValid()) {
            try {
                ProjectDto updateProject = projectService.update(projectDto);
                req.setAttribute("project", updateProject);
                String location = req.getContextPath() + "?commandName=" + ApplicationConstants.PROJECT_PAGE_NAME + "&projectId=" + projectId;
                RequestUtil.sendRedirect(resp, location);
            } catch (ServiceException e) {
                LOG.error("Edit project view error", e);
                throw new CommandException(e);
            }
        } else {
            Map<String, String> errorResult = validate.getResult();
            req.setAttribute("validationErrors", errorResult);
            req.setAttribute("project", projectDto);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_PROJECT_VIEW_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        }
    }
}