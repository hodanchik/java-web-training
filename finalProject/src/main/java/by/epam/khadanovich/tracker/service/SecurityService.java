package by.epam.khadanovich.tracker.service;


public interface SecurityService {

    boolean canAccessToProject(long userId, long projectId) throws ServiceException;

    boolean canAccessToUserPage(long currentUserId, long updateUserId) throws ServiceException;

}
