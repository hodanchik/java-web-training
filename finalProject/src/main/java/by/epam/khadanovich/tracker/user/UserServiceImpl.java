package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.service.TransactionSupport;
import by.epam.khadanovich.tracker.service.Transactional;
import by.epam.khadanovich.tracker.util.CryptPass;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Bean
@TransactionSupport
public class UserServiceImpl implements UserService {
    private final static Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    private UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    @Override
    public UserDto loginUser(UserDto userDto) throws ServiceException {
        try {
            UserDto foundUserDto = userDao.loginUser(userDto);
            if (foundUserDto.getLogin() != null && foundUserDto.getPassword().equals(CryptPass.cryptPass(userDto.getPassword())) &&
                    foundUserDto.getAccStatusId() == 1) {
                foundUserDto = userDao.getById(foundUserDto.getId());
                return foundUserDto;
            }
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
        return userDto;
    }

    @Transactional
    @Override
    public UserDto getById(Long id) throws ServiceException {
        try {
            return userDao.getById(id);
        } catch (DaoException e) {
            LOGGER.error("Failed to get by id user", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto getInfoById(Long id) throws ServiceException {
        try {
            return userDao.getInfoById(id);
        } catch (DaoException e) {
            LOGGER.error("Failed to get info by user id", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<UserDto> find(String criteria) throws ServiceException {
        try {
            return userDao.find(criteria);
        } catch (DaoException e) {
            LOGGER.error("Failed find user", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<UserDto> findAll() {
        try {
            return userDao.findAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to read users", e);
            return new ArrayList<>();
        }
    }

    @Override
    @Transactional
    public void delete(Long id) throws ServiceException {
        try {
            userDao.delete(id);
        } catch (DaoException e) {
            LOGGER.error("Failed to delete user", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean block(Long id) throws ServiceException {
        try {
            return userDao.block(id);
        } catch (DaoException e) {
            LOGGER.error("Failed to block user", e);
            throw new ServiceException(e);
        }

    }

    @Override
    public boolean unblock(Long id) throws ServiceException {
        try {
            return userDao.unblock(id);
        } catch (DaoException e) {
            LOGGER.error("Failed to unblock user", e);
            throw new ServiceException(e);
        }

    }

    @Override
    public boolean checkLoginUnique(UserDto dto) throws ServiceException {
        try {
            return userDao.checkLoginUnique(dto);
        } catch (DaoException e) {
            LOGGER.error("Failed check Login user Unique", e);
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public boolean save(UserDto dto) throws ServiceException {
        try {
            if (userDao.checkLoginUnique(dto)) {
                String password = dto.getPassword();
                String cryptPass = CryptPass.cryptPass(password);
                dto.setPassword(cryptPass);
                if (userDao.save(dto) > 0) {
                    return true;
                }
            } else {
                return false;
            }
        } catch (DaoException e) {
            LOGGER.error("Failed to save user", e);
            throw new ServiceException(e);
        }
        return false;
    }

    @Transactional
    @Override
    public UserDto update(UserDto dto) throws ServiceException {
        try {
            if (userDao.update(dto)) {
                return userDao.getById(dto.getId());
            } else {
                return dto;
            }
        } catch (DaoException e) {
            LOGGER.error("Failed to update user", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto changePassword(UserDto dto) throws ServiceException {
        try {
            return userDao.changePassword(dto);
        } catch (DaoException e) {
            LOGGER.error("Failed to change user password", e);
            throw new ServiceException(e);
        }
    }

}
