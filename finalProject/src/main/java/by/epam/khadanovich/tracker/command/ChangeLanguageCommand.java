package by.epam.khadanovich.tracker.command;

import by.epam.khadanovich.tracker.core.Bean;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.CHANGE_LANG_CMD_NAME;

@Bean(name = CHANGE_LANG_CMD_NAME)
public class ChangeLanguageCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ChangeLanguageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        String referer = req.getHeader("referer");
        RequestUtil.sendRedirect(resp, referer);
    }
}
