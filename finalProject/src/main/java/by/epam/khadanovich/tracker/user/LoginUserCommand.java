package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;


@Bean(name = LOGIN_CMD_NAME)
public class LoginUserCommand implements Command {
    private final static Logger LOG = Logger.getLogger(LoginUserCommand.class);

    private UserService userService;
    private LoginUserValidator loginUserValidator;

    public LoginUserCommand(UserService userService, LoginUserValidator loginUserValidator) {
        this.userService = userService;
        this.loginUserValidator = loginUserValidator;
    }

    public UserService getUserService() {
        return userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String login = req.getParameter("user.login");
            String password = req.getParameter("user.password");
            UserDto userDto = new UserDto(login, password);
            ValidationResult validate = loginUserValidator.validate(req);
            if (!validate.isValid()) {
                req.setAttribute("user", userDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, LOGIN_VIEW_CMD_NAME);
                req.setAttribute("message", "message.login.unsuccessfulLogin");
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
            UserDto loginUserDto = userService.loginUser(userDto);
            if (loginUserDto != null && loginUserDto.getId() > 0) {
                req.getSession().setAttribute("currentUser", loginUserDto);
                req.getSession().setAttribute("user.id", loginUserDto.getId());
                String location = req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_USER_PAGE_CMD_NAME;
                RequestUtil.sendRedirect(resp, location);
            } else {
                req.setAttribute("user", userDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, LOGIN_VIEW_CMD_NAME);
                req.setAttribute("message", "message.login.unsuccessfulLogin");
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Login user error ", e);
            new CommandException(e);
        }
    }
}

