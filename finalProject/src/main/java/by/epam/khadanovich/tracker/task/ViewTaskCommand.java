package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.comment.CommentService;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.PaginationUtil;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = TASK_PAGE_NAME)
public class ViewTaskCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewTaskCommand.class);
    private TaskService taskService;
    private CommentService commentService;
    private SecurityService securityService;

    public ViewTaskCommand(TaskService taskService, CommentService commentService, SecurityService securityService) {
        this.taskService = taskService;
        this.commentService = commentService;
        this.securityService = securityService;
    }


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long id = Long.valueOf(req.getParameter("taskId"));
            TaskDto taskById = taskService.getById(id);
            long projectId = taskById.getProjectId();
            Long userId = (Long) req.getSession().getAttribute("user.id");
            if (securityService.canAccessToProject(userId, projectId)) {
                long page = Long.parseLong(req.getParameter("page"));
                long count = commentService.findAllByTaskCount(id);
                long maxPage = PaginationUtil.calculateMaxPage(count);
                page = PaginationUtil.checkLastPage(maxPage, page);
                List<CommentDto> allCommentsByTask = commentService.findAllByTaskLimit(id, page);
                req.setAttribute("maxPage", maxPage);
                req.setAttribute("page", page);
                req.getSession().setAttribute("task.id", id);
                req.getSession().setAttribute("currentTask", taskById);
                req.setAttribute("allCommentByTask", allCommentsByTask);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, TASK_PAGE_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            } else {
                LOG.info("Command ViewTaskCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("View project page error", e);
            throw new CommandException(e);
        }
    }
}

