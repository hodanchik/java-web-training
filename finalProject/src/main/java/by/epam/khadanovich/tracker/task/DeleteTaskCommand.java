package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = DELETE_TASKS_CMD_NAME)
public class DeleteTaskCommand implements Command {
    private final static Logger LOG = Logger.getLogger(DeleteTaskCommand.class);
    private TaskService taskService;

    public DeleteTaskCommand(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long taskId = Long.parseLong(req.getParameter("taskId"));
            if(taskService.delete(taskId)) {
                String referer = req.getHeader("referer");
                RequestUtil.sendRedirect(resp, referer);
            }
            else{
                LOG.info("Command DeleteTaskCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_SERVER_ERROR);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Failed delete task", e);
            throw new CommandException("Failed delete task", e);
        }
    }
}
