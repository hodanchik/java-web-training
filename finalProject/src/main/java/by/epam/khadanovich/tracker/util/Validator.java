package by.epam.khadanovich.tracker.util;

import javax.servlet.http.HttpServletRequest;

public interface Validator {
    ValidationResult validate(HttpServletRequest req);
}
