package by.epam.khadanovich.tracker.service;


import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.DataSource;
import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;


@Bean
public class TransactionManagerImpl implements TransactionManager {
    private final static Logger LOGGER = Logger.getLogger(TransactionManagerImpl.class);
    private DataSource dataSource;
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    public TransactionManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void beginTransaction() throws SQLException {
        if (localConnection.get() == null) {
            Connection connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            localConnection.set(connection);
        } else {
            LOGGER.warn("Transaction already started");
        }
    }

    @Override
    public void commitTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.commit();
            connection.setAutoCommit(true);
            connection.close();
        } else {
            LOGGER.error("Connection is null");
        }
        localConnection.remove();
    }

    @Override
    public void rollbackTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.rollback();
            connection.setAutoCommit(true);
            connection.close();
        }else {
            LOGGER.error("Connection is null");
        }
        localConnection.remove();
    }

    @Override
    public Connection getConnection() {
        if (localConnection.get() != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals("close")) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        } else {
            return null;
        }
    }
}
