package by.epam.khadanovich.tracker.core;

public interface BeanRegistry {

    <T> void registerBean(T service);

    <T> void registerBean(Class<T> beanClass);

    <T> T getBean(Class<T> beanClass);

    <T> T getBean(String name);

    <T> boolean removeBean(T bean);

    void destroy();
}



