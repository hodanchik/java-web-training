package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = EDIT_TASK_VIEW_NAME)
public class EditTaskViewCommand implements Command {
    private final static Logger LOG = Logger.getLogger(EditTaskViewCommand.class);
    private TaskService taskService;
    private SecurityService securityService;

    public EditTaskViewCommand(TaskService taskService, SecurityService securityService) {
        this.taskService = taskService;
        this.securityService = securityService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long id = Long.parseLong(req.getParameter("taskId"));
            TaskDto taskById = taskService.getById(id);
            long projectId = taskById.getProjectId();
            Long userId = (Long) req.getSession().getAttribute("user.id");
            if (securityService.canAccessToProject(userId, projectId)) {
                req.setAttribute("task", taskById);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_TASK_VIEW_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            } else {
                LOG.info("Command EditTaskViewCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException  e){
            LOG.error("View edit task page error", e);
            throw new CommandException(e);
        }
    }
}
