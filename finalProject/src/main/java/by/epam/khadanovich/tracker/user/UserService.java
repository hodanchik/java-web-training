package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;

import java.util.List;

public interface UserService {

    UserDto getById(Long id) throws ServiceException;

    UserDto getInfoById(Long id) throws ServiceException ;

    List<UserDto> find(String criteria)throws ServiceException ;

    List<UserDto> findAll() throws ServiceException ;

    void delete(Long id) throws ServiceException;

    boolean block(Long id) throws ServiceException;

    boolean unblock(Long id) throws ServiceException;

    boolean checkLoginUnique(UserDto dto) throws ServiceException;

    boolean save(UserDto dto) throws ServiceException;

    UserDto update(UserDto dto) throws ServiceException;

    UserDto changePassword(UserDto dto) throws ServiceException;

    UserDto loginUser(UserDto userDto) throws ServiceException;
}
