package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;


@Bean(name = VIEW_OWN__PROJECT_LIST_CMD_NAME)
public class ViewProjectListCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewProjectListCommand.class);
    ProjectService projectService;

    public ViewProjectListCommand(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
        try {
            long currentUserId = currentUser.getId();
            List<ProjectDto> ownProjectList = projectService.findOwnProject(currentUserId);
            req.setAttribute("ownProjectList", ownProjectList);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_OWN__PROJECT_LIST_CMD_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("VIEW_PROJECT_LIST by.epam.khadanovich.tracker.command exception", e);
            throw new CommandException(e);
        }
    }
}