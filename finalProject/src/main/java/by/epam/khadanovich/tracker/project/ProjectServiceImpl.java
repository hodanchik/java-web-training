package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.comment.CommentDao;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.service.TransactionSupport;
import by.epam.khadanovich.tracker.service.Transactional;
import by.epam.khadanovich.tracker.task.TaskDao;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@Bean
@TransactionSupport
public class ProjectServiceImpl implements ProjectService {
    private final static Logger LOG = Logger.getLogger(ProjectServiceImpl.class);
    private ProjectDao projectDao;
    private TaskDao taskDao;
    private CommentDao commentDao;

    public ProjectServiceImpl(ProjectDao projectDao, TaskDao taskDao, CommentDao commentDao) {
        this.projectDao = projectDao;
        this.taskDao = taskDao;
        this.commentDao = commentDao;
    }

    @Override
    public ProjectDto getById(Long id) throws ServiceException {
        try {
            return projectDao.getById(id);
        } catch (DaoException e) {
            LOG.error("Failed find project by id", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<ProjectDto> find(Predicate<ProjectDto> criteria) {
        return null;
    }

    @Override
    public List<ProjectDto> findOwnProject(long id) {
        try {
            return projectDao.findOwnProject(id);
        } catch (DaoException e) {
            LOG.error("Failed find own project", e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<ProjectDto> findAll() {
        try {
            return projectDao.findAll();
        } catch (DaoException e) {
            LOG.error("Failed find all project", e);
            return new ArrayList<>();
        }
    }

    @Transactional
    @Override
    public void delete(Long projectId) throws ServiceException {
        try {
            List<TaskDto> allTasksOfProject = taskDao.getByProject(projectId);
            for (TaskDto task : allTasksOfProject) {
                List<CommentDto> allByTask = commentDao.findAllByTask(task.getId());
                for (CommentDto comment : allByTask) {
                    commentDao.delete(comment.getId());
                }
                taskDao.delete(task.getId());
            }
            projectDao.delete(projectId);

        } catch (DaoException e) {
            LOG.error("Failed to delete project", e);
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public boolean save(ProjectDto dto) throws ServiceException {

        try {
            return projectDao.save(dto) > 0;
        } catch (DaoException e) {
            LOG.error("Failed save project", e);
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public ProjectDto update(ProjectDto dto) throws ServiceException {
        try {
            if (projectDao.update(dto)) {
                return projectDao.getById(dto.getId());
            }
        } catch (DaoException e) {
            LOG.error("Failed update project", e);
            throw new ServiceException(e);
        }
        return dto;
    }
}
