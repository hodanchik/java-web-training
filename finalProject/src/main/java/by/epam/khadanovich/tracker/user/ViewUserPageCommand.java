package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_USER_PAGE_CMD_NAME)
public class ViewUserPageCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewUserPageCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
        if (currentUser.getRole_id() == 1) {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, ADMIN_PAGE_NAME);
        } else {
            req.setAttribute(VIEWNAME_REQ_PARAMETER, PERSONAL_PAGE_NAME);
        }
        RequestUtil.forward(req, resp, MAIN_LAYOUT);

    }
}
