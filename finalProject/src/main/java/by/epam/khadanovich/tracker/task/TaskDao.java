package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.dao.BaseDao;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.TaskDto;

import java.util.List;

public interface TaskDao extends BaseDao<TaskDto, Long> {
    List<TaskDto> getByProject(long id) throws DaoException;
    List<TaskDto> findOwnTasks(long id) throws DaoException;
}
