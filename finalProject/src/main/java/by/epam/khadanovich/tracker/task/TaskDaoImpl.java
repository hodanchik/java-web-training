package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.ConnectionManager;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.TaskDto;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Bean
public class TaskDaoImpl implements TaskDao {
    private final static Logger LOG = Logger.getLogger(TaskDaoImpl.class);
    private static final String FIND_ALL_TASKS_OF_PROJECT = "SELECT task.id, task.task_name, task.creator_id, " +
            "task.executor_id, task.status_id, task.date_deadline, user_account.login as executor_login, " +
            "task.project_id, status_state.status_name, project.project_name from task LEFT JOIN user_account " +
            "on task.executor_id = user_account.id LEFT JOIN status_state on task.status_id = status_state.id " +
            "LEFT JOIN project on task.project_id = project.id where project_id = ?";
    private static final String FIND_ALL_TASKS = "SELECT task.id, task.task_name, task.creator_id, task.executor_id, " +
            "task.status_id, task.date_deadline, user_account.login as executor_login, task.project_id, " +
            "status_state.status_name, project.project_name from task LEFT JOIN user_account " +
            "on task.executor_id = user_account.id LEFT JOIN status_state on task.status_id = status_state.id " +
            "LEFT JOIN project on task.project_id = project.id";
    private static final String CREATE_TASK = "INSERT into task(task_name, project_id, creator_id, executor_id, " +
            "date_deadline) VALUES (?,?,?,?,?)";
    private static final String FIND_OWN_TASKS = "SELECT task.id, task.task_name, task.creator_id, user_account.login " +
            "as executor_login, task.executor_id, task.status_id, task.date_deadline, task.project_id, " +
            "status_state.status_name, project.project_name from task LEFT JOIN status_state " +
            "on task.status_id = status_state.id LEFT JOIN user_account on task.executor_id = user_account.id " +
            "LEFT JOIN project on task.project_id = project.id where task.executor_id =?";
    private static final String GET_BY_ID = "SELECT task.id, task.task_name, task.creator_id, task.executor_id, " +
            "user_account.login as executor_login, task.status_id, task.date_deadline, task.project_id, " +
            "status_state.status_name, project.project_name from task LEFT JOIN status_state " +
            "on task.status_id = status_state.id LEFT JOIN user_account on task.executor_id = user_account.id " +
            "LEFT JOIN project on task.project_id = project.id where task.id =?";
    private static final String UPDATE_TASK = "UPDATE task SET task_name=?, executor_id=?, status_id=?, " +
            "date_deadline=? WHERE id=?";
    private static final String DELETE_TASK = "DELETE FROM task WHERE id=?";

    private ConnectionManager connectionManager;

    public TaskDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public List<TaskDto> getByProject(long projectId) throws DaoException {
        List<TaskDto> tasksOfProject = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_TASKS_OF_PROJECT)) {
            int i = 0;
            preparedStatement.setLong(++i, projectId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                tasksOfProject.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            LOG.error("Find all tasks of project error", e);
            throw new DaoException("Find all tasks of project error", e);
        }
        return tasksOfProject;
    }

    @Override
    public List<TaskDto> findOwnTasks(long userId) throws DaoException {
        List<TaskDto> ownTasks = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_OWN_TASKS)) {
            int i = 0;
            preparedStatement.setLong(++i, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ownTasks.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            LOG.error("Find all tasks of project error", e);
            throw new DaoException("Find all tasks of project error", e);
        }
        return ownTasks;
    }

    @Override
    public Long save(TaskDto taskDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(CREATE_TASK, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            preparedStatement.setString(++i, taskDto.getTaskName());
            preparedStatement.setLong(++i, taskDto.getProjectId());
            preparedStatement.setLong(++i, taskDto.getCreatorId());
            preparedStatement.setLong(++i, taskDto.getExecutorId());
            Timestamp deadlineTimestamp = new Timestamp(taskDto.getDateDeadline().getTime());
            preparedStatement.setTimestamp(++i, deadlineTimestamp);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                taskDto.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            LOG.error("Save tasks of project error", e);
            throw new DaoException("Save tasks of project error", e);
        }
        return taskDto.getId();
    }

    @Override
    public boolean update(TaskDto taskDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_TASK)) {
            int i = 0;
            preparedStatement.setString(++i, taskDto.getTaskName());
            preparedStatement.setLong(++i, taskDto.getExecutorId());
            preparedStatement.setLong(++i, taskDto.getStatusId());
            Timestamp deadlineTimestamp = new Timestamp(taskDto.getDateDeadline().getTime());
            preparedStatement.setTimestamp(++i, deadlineTimestamp);
            preparedStatement.setLong(++i, taskDto.getId());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOG.error("Update tasks of project error", e);
            throw new DaoException("Update tasks of project error", e);
        }
    }

    @Override
    public boolean delete(Long taskId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_TASK)) {
            int i = 0;
            preparedStatement.setLong(++i, taskId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DaoException("Request or table failed delete task", e);
        }
    }

    @Override
    public TaskDto getById(Long taskId) throws DaoException {
        TaskDto taskById = new TaskDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(GET_BY_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, taskId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                taskById = parseResultSet(resultSet);
            }
            return taskById;
        } catch (SQLException e) {
            LOG.error("Get by id task error", e);
            throw new DaoException("Get by id task error", e);
        }
    }

    @Override
    public List<TaskDto> findAll() throws DaoException {
        List<TaskDto> allTasks = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_TASKS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                allTasks.add(parseResultSet(resultSet));
            }
        } catch (SQLException e) {
            LOG.error("Get all task error", e);
            throw new DaoException("Get all task error", e);
        }
        return allTasks;
    }


    private TaskDto parseResultSet(ResultSet resultSet) throws DaoException {
        try {
            long taskId = resultSet.getLong("id");
            String taskName = resultSet.getString("task_name");
            long projectId = resultSet.getLong("project_id");
            String projectName = resultSet.getString("project_name");
            long creatorId = resultSet.getLong("creator_id");
            long executorId = resultSet.getLong("executor_id");
            String executorLogin = resultSet.getString("executor_login");
            int statusId = resultSet.getInt("status_id");
            String statusName = resultSet.getString("status_name");
            Timestamp dateDeadline = resultSet.getTimestamp("date_deadline");
            Date deadlineTime = new Date(dateDeadline.getTime());
            return new TaskDto(taskId, taskName, projectId, projectName, creatorId, executorId,
                    executorLogin, statusId, statusName, deadlineTime);
        } catch (SQLException e) {
            LOG.error("parse result set error", e);
            throw new DaoException("parse result set error", e);
        }
    }
}