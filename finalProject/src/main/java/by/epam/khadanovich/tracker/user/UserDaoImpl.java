package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.ConnectionManager;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.UserDto;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Bean
public class UserDaoImpl implements UserDao {
    private final static Logger LOG = Logger.getLogger(UserDaoImpl.class);
    private static final String SELECT_BY_ID = "SELECT user_account.id, user_account.login, user_account.password," +
            " user_account.contact_id, contact.first_name, contact.last_name, contact.email, contact.phone, " +
            "user_role.role_name, user_role.id as id_role, user_skill_level.skill_level_name, " +
            "user_account.account_status as id_status_acc, account_status.account_status_name, user_skill_level.id " +
            "as id_skill FROM user_account  LEFT JOIN contact ON user_account.contact_id = contact.id " +
            "LEFT JOIN user_role ON user_account.role_id = user_role.id LEFT JOIN account_status " +
            "ON user_account.account_status = account_status.id LEFT JOIN user_skill_level " +
            "ON user_account.skill_id = user_skill_level.id WHERE user_account.id =?";
    private static final String GET_INFO_BY_ID = "SELECT user_account.id, user_account.login, user_account.contact_id, " +
            "contact.first_name, contact.last_name, contact.email, contact.phone, user_role.role_name, user_role.id " +
            "as id_role, user_skill_level.skill_level_name, user_account.account_status as id_status_acc, " +
            "account_status.account_status_name, user_skill_level.id as id_skill FROM user_account LEFT JOIN contact " +
            "ON user_account.contact_id = contact.id LEFT JOIN account_status ON user_account.account_status = account_status.id " +
            "LEFT JOIN user_role ON user_account.role_id = user_role.id LEFT JOIN user_skill_level " +
            "ON user_account.skill_id = user_skill_level.id WHERE user_account.id =?";
    private static final String FIND_BY_LOGIN = "SELECT id FROM user_account WHERE login=?";
    private static final String REGISTER_USER = "INSERT into user_account(login, password, role_id, skill_id, contact_id)" +
            " VALUES (?, ?, ?, ?, ?)";
    private static final String REGISTER_CONTACT = "INSERT into contact(first_name, last_name, email, phone) " +
            "VALUES (?, ?, ?, ?)";
    private static final String UPDATE_USER = "UPDATE user_account LEFT JOIN contact ON user_account.contact_id = contact.id " +
            "SET contact.first_name=?, contact.last_name=?, contact.email=?,  contact.phone=?, user_account.role_id = ?, " +
            "user_account.skill_id = ? WHERE user_account.id=?";
    private static final String CHANGE_USER_PASSWORD = "UPDATE user_account SET user_account.password = ? WHERE user_account.id=?";
    private static final String SELECT_ALL_USERS = "SELECT user_account.id, user_account.login, " +
            "user_account.account_status as id_status_acc, account_status.account_status_name, user_account.password, " +
            " user_account.contact_id, contact.first_name, contact.last_name, contact.email, contact.phone, " +
            "user_role.role_name, user_role.id as id_role, user_skill_level.skill_level_name, " +
            "user_skill_level.id as id_skill FROM user_account LEFT JOIN account_status ON user_account.account_status" +
            " = account_status.id LEFT JOIN contact ON user_account.contact_id = contact.id LEFT JOIN user_role " +
            "ON user_account.role_id = user_role.id LEFT JOIN user_skill_level ON user_account.skill_id = user_skill_level.id";
    private static final String LOGIN_USER = "SELECT user_account.id, user_account.login, user_account.password, " +
            "user_account.account_status as id_status_acc FROM user_account WHERE user_account.login=?";
    private static final String DELETE_USER = "DELETE FROM user_account WHERE id=?";
    private static final String BLOCK_USER = "UPDATE user_account SET account_status=2 WHERE id=?";
    private static final String UNBLOCK_USER = "UPDATE user_account SET account_status=1 WHERE id=?";
    private static final String DELETE_USER_CONTACT = "DELETE FROM contact WHERE id=?";
    private static final String FIND_USER = "SELECT user_account.id, user_account.login, user_account.contact_id, " +
            "contact.first_name, contact.last_name, contact.email, contact.phone, user_role.role_name, " +
            "user_role.id as id_role, user_skill_level.skill_level_name, user_account.account_status as id_status_acc," +
            " account_status.account_status_name, user_skill_level.id as id_skill FROM user_account LEFT JOIN contact" +
            " ON user_account.contact_id = contact.id LEFT JOIN account_status " +
            "ON user_account.account_status = account_status.id LEFT JOIN user_role ON user_account.role_id = user_role.id " +
            "LEFT JOIN user_skill_level ON user_account.skill_id = user_skill_level.id " +
            "WHERE (user_account.account_status = 1)  AND (user_account.login LIKE ? OR contact.first_name " +
            "LIKE ? OR contact.last_name LIKE ?)";
    private ConnectionManager connectionManager;

    public UserDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }


    @Override
    public UserDto loginUser(UserDto userDto) throws DaoException {
        UserDto userFoundDto = new UserDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(LOGIN_USER)) {
            int i = 0;
            preparedStatement.setString(++i, userDto.getLogin());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                userFoundDto.setId(resultSet.getLong("id"));
                userFoundDto.setLogin(resultSet.getString("login"));
                userFoundDto.setPassword(resultSet.getString("password"));
                userFoundDto.setAccStatusId(resultSet.getInt("id_status_acc"));
            }
        } catch (SQLException e) {
            throw new DaoException("Request or table failed ", e);
        }
        return userFoundDto;
    }

    @Override
    public List<UserDto> find(String criteria) throws DaoException {
        String modifCriteria = "%".concat(criteria).concat("%");
        List<UserDto> userInfo = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_USER)) {
            int i = 0;
            preparedStatement.setString(++i, modifCriteria);
            preparedStatement.setString(++i, modifCriteria);
            preparedStatement.setString(++i, modifCriteria);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userInfo.add(parseResultSetInfo(resultSet));
            }
        } catch (SQLException e) {
            throw new DaoException("Request or table find ", e);
        }
        return userInfo;

    }

    @Override
    public List<UserDto> findAll() throws DaoException {
        List<UserDto> userList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserDto userDto = parseResultSet(resultSet);
                userList.add(userDto);
            }
        } catch (SQLException e) {
            throw new DaoException("Request or table failed ", e);
        }
        return userList;
    }

    @Override
    public Long save(UserDto userDto) throws DaoException {
        long contactId = saveContact(userDto);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(REGISTER_USER, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            preparedStatement.setString(++i, userDto.getLogin());
            preparedStatement.setString(++i, userDto.getPassword());
            preparedStatement.setInt(++i, userDto.getRole_id());
            preparedStatement.setInt(++i, userDto.getSkillLevel_id());
            preparedStatement.setLong(++i, contactId);
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                userDto.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            throw new DaoException("Save failed ", e);
        }
        return userDto.getId();
    }

    @Override
    public boolean update(UserDto userDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_USER)) {
            int i = 0;
            preparedStatement.setString(++i, userDto.getFirstName());
            preparedStatement.setString(++i, userDto.getLastName());
            preparedStatement.setString(++i, userDto.getEmail());
            preparedStatement.setString(++i, userDto.getPhone());
            preparedStatement.setInt(++i, userDto.getRole_id());
            preparedStatement.setInt(++i, userDto.getSkillLevel_id());
            preparedStatement.setLong(++i, userDto.getId());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DaoException("Request or table failed update user", e);
        }
    }


    @Override
    public boolean delete(Long userId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_USER)) {
            int i = 0;
            preparedStatement.setLong(++i, userId);
            preparedStatement.executeUpdate();
            return deleteContact(userId);
        } catch (SQLException e) {
            LOG.error("Delete user error", e);
            throw new DaoException("Request or table failed delete user", e);
        }
    }

    @Override
    public boolean block(Long userId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(BLOCK_USER)) {
            int i = 0;
            preparedStatement.setLong(++i, userId);
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOG.error("Block user error", e);
            throw new DaoException("Request or table failed change block user", e);
        }
    }

    @Override
    public boolean unblock(Long userId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UNBLOCK_USER)) {
            int i = 0;
            preparedStatement.setLong(++i, userId);
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOG.error("Unblock user error", e);
            throw new DaoException("Request or table failed change unblock user", e);
        }
    }

    @Override
    public UserDto changePassword(UserDto userDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(CHANGE_USER_PASSWORD)) {
            int i = 0;
            preparedStatement.setString(++i, userDto.getPassword());
            preparedStatement.setLong(++i, userDto.getId());
            preparedStatement.executeUpdate();
            return getById(userDto.getId());
        } catch (SQLException e) {
            LOG.error("Change password error", e);
            throw new DaoException("Request or table failed change user password", e);
        }
    }

    @Override
    public UserDto getInfoById(Long id) throws DaoException {
        UserDto userInfoById = new UserDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(GET_INFO_BY_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                userInfoById = parseResultSetInfo(resultSet);
            }
        } catch (SQLException e) {
            LOG.error("Get info by id error", e);
            throw new DaoException("Request or table failed get user by id ", e);
        }
        return userInfoById;
    }

    private boolean deleteContact(Long userId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_USER_CONTACT)) {
            int i = 0;
            preparedStatement.setLong(++i, userId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOG.error("Request or table failed delete contact user", e);
            throw new DaoException("Request or table failed delete contact user", e);
        }
    }


    @Override
    public UserDto getById(Long id) throws DaoException {
        UserDto userFoundDto = new UserDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                userFoundDto = parseResultSet(resultSet);
            }
        } catch (SQLException e) {
            LOG.error("Request or table failed get user by id", e);
            throw new DaoException("Request or table failed get user by id ", e);
        }
        return userFoundDto;
    }


    @Override
    public boolean checkLoginUnique(UserDto userDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_LOGIN)) {
            int i = 0;
            preparedStatement.setString(++i, userDto.getLogin());
            ResultSet resultSet = preparedStatement.executeQuery();
            return !resultSet.next();
        } catch (SQLException e) {
            LOG.error("Request or table failed check Login unique", e);
            throw new DaoException("Request or table failed check Login unique", e);
        }
    }

    @Override
    public long saveContact(UserDto userDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(REGISTER_CONTACT, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            preparedStatement.setString(++i, userDto.getFirstName());
            preparedStatement.setString(++i, userDto.getLastName());
            preparedStatement.setString(++i, userDto.getEmail());
            preparedStatement.setString(++i, userDto.getPhone());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            while (generatedKeys.next()) {
                userDto.setContactId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            throw new DaoException("Save Contact failed ", e);
        }
        return userDto.getContactId();
    }

    private UserDto parseResultSet(ResultSet resultSet) throws DaoException {
        try {
            long userId = resultSet.getLong("id");
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            long contactId = resultSet.getLong("contact_id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String phone = resultSet.getString("phone");
            String roleName = resultSet.getString("role_name");
            int roleId = resultSet.getInt("id_role");
            String skillName = resultSet.getString("skill_level_name");
            int skillId = resultSet.getInt("id_skill");
            int accStatusId = resultSet.getInt("id_status_acc");
            String accStatusName = resultSet.getString("account_status_name");
            return new UserDto(userId, login, password, firstName, lastName, email, phone, roleName, roleId, skillName,
                    skillId, contactId, accStatusId, accStatusName);
        } catch (SQLException e) {
            LOG.error("parse result Set user error", e);
            throw new DaoException("parse result Set user error ", e);
        }
    }

    private UserDto parseResultSetInfo(ResultSet resultSet) throws DaoException {
        try {
            long userId = resultSet.getLong("id");
            String login = resultSet.getString("login");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String phone = resultSet.getString("phone");
            String roleName = resultSet.getString("role_name");
            int roleId = resultSet.getInt("id_role");
            String skillName = resultSet.getString("skill_level_name");
            int skillId = resultSet.getInt("id_skill");
            String accStatusName = resultSet.getString("account_status_name");
            return new UserDto(userId, login, firstName, lastName, email, phone, roleName, roleId, skillName,
                    skillId, accStatusName);
        } catch (SQLException e) {
            LOG.error("parse result Set user info error", e);
            throw new DaoException("parse result Set user info error ", e);
        }
    }
}

