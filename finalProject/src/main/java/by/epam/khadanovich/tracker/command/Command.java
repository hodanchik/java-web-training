package by.epam.khadanovich.tracker.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@FunctionalInterface
public interface Command {

    void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;
}