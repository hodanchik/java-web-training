package by.epam.khadanovich.tracker.dto;


import java.util.Date;

public class TaskDto {
    private long id;
    private String taskName;
    private long projectId;
    private String projectName;
    private long creatorId;
    private String executorLogin;
    private long executorId;
    private int statusId;
    private String statusName;
    private Date dateDeadline;


    public TaskDto() {
    }

    public TaskDto(String taskName, long projectId, long creatorId, Date dateDeadline) {
        this.taskName = taskName;
        this.projectId = projectId;
        this.creatorId = creatorId;
        this.dateDeadline = dateDeadline;
    }

    public TaskDto(long id, String taskName, long projectId, String projectName, long creatorId,
                   long executorId, int statusId, String statusName, Date dateDeadline) {
        this.id = id;
        this.taskName = taskName;
        this.projectId = projectId;
        this.projectName = projectName;
        this.creatorId = creatorId;
        this.executorId = executorId;
        this.statusId = statusId;
        this.statusName = statusName;
        this.dateDeadline = dateDeadline;
    }

    public TaskDto(long id, int statusId) {
        this.id = id;
        this.statusId = statusId;
    }

    public TaskDto(long id, String taskName, long projectId, String projectName, long creatorId, long executorId,
                   String executorLogin, int statusId, String statusName, Date deadlineTime) {
        this.id = id;
        this.taskName = taskName;
        this.projectId = projectId;
        this.projectName = projectName;
        this.creatorId = creatorId;
        this.executorId = executorId;
        this.executorLogin = executorLogin;
        this.statusId = statusId;
        this.statusName = statusName;
        this.dateDeadline = deadlineTime;



    }

    public TaskDto(Long id, String taskName, long executorId, int statusId, Date deadlineTime) {
        this.id = id;
        this.taskName = taskName;
        this.executorId = executorId;
        this.statusId = statusId;
        this.dateDeadline = deadlineTime;
    }

    public String getExecutorLogin() {
        return executorLogin;
    }

    public void setExecutorLogin(String executorLogin) {
        this.executorLogin = executorLogin;
    }

    public TaskDto(String taskName, Long projectId, Long creatorId, Long executorId, Date deadlineTime) {
        this.taskName = taskName;
        this.projectId = projectId;
        this.creatorId = creatorId;
        this.executorId = executorId;
        this.dateDeadline = deadlineTime;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public long getExecutorId() {
        return executorId;
    }

    public void setExecutorId(long executorId) {
        this.executorId = executorId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Date getDateDeadline() {
        return dateDeadline;
    }

    public void setDateDeadline(Date dateDeadline) {
        this.dateDeadline = dateDeadline;
    }

    @Override
    public String toString() {
        return "TaskDto{" +
                "id=" + id +
                ", taskName='" + taskName + '\'' +
                ", projectId=" + projectId +
                ", creatorId=" + creatorId +
                ", executorId=" + executorId +
                ", statusId=" + statusId +
                ", statusName='" + statusName + '\'' +
                ", dateDeadline=" + dateDeadline +
                '}';
    }
}
