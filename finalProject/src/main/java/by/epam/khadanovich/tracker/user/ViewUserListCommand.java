package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationServlet;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_ALL_USERS_CMD_NAME)
public class ViewUserListCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ApplicationServlet.class);
    private UserService userService;

    public ViewUserListCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            List<UserDto> allUsersList = userService.findAll();
            req.setAttribute("allUsersList", allUsersList);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ALL_USERS_CMD_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("View User List Command  exception", e);
            throw new CommandException(e);
        }
    }
}

