package by.epam.khadanovich.tracker.entity;

import java.util.Date;
import java.util.Objects;

public class TaskEntity {
    private long id;
    private String name;
    private long projectId;
    private long creatorId;
    private long executorId;
    private StatusEntity status;
    private Date deadline;

    public TaskEntity(long id, String name, long projectId, long creatorId, long executorId, StatusEntity status, Date deadline) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
        this.creatorId = creatorId;
        this.executorId = executorId;
        this.status = status;
        this.deadline = deadline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskEntity)) return false;
        TaskEntity that = (TaskEntity) o;
        return getId() == that.getId() &&
                getProjectId() == that.getProjectId() &&
                getCreatorId() == that.getCreatorId() &&
                getExecutorId() == that.getExecutorId() &&
                getName().equals(that.getName()) &&
                getStatus().equals(that.getStatus()) &&
                getDeadline().equals(that.getDeadline());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getProjectId(), getCreatorId(), getExecutorId(), getStatus(), getDeadline());
    }

    public TaskEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public long getExecutorId() {
        return executorId;
    }

    public void setExecutorId(long executorId) {
        this.executorId = executorId;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public void setStatus(StatusEntity status) {
        this.status = status;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
}
