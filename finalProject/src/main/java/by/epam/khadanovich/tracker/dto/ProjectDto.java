package by.epam.khadanovich.tracker.dto;

public class ProjectDto {

    private long id;
    private long creatorId;
    private String projectName;
    private String statusName;
    private String description;
    private int statusId;

    public ProjectDto() {
    }

    public ProjectDto(long creatorId, String projectName, String description) {
        this.creatorId = creatorId;
        this.projectName = projectName;
        this.description = description;
    }

    public ProjectDto(long projectId, long creatorId, String projectName, String description, String statusName, int statusId) {
        this.id = projectId;
        this.creatorId = creatorId;
        this.projectName = projectName;
        this.description = description;
        this.statusName = statusName;
        this.statusId = statusId;
    }

    public ProjectDto(Long projectId, String projectName, String projectDescription, int projectStatusId) {
        this.id = projectId;
        this.projectName = projectName;
        this.description = projectDescription;
        this.statusId = projectStatusId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
