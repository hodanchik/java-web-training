package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.service.ServiceException;

import java.util.List;
import java.util.function.Predicate;

public interface CommentService {
    CommentDto getById(Long id)  throws ServiceException;

    List<CommentDto> find(Predicate<CommentDto> criteria)  throws ServiceException;

    List<CommentDto> findAllByTask(long id)  throws ServiceException;

    long findAllCount()  throws ServiceException;

    long findAllByTaskCount(long taskId)  throws ServiceException;

    List<CommentDto> findAll()  throws ServiceException;

    List<CommentDto> findAllLimit(long page)  throws ServiceException;

    List<CommentDto> findAllByTaskLimit(long taskId, long page)  throws ServiceException;

    void delete(Long id)  throws ServiceException;

    boolean save(CommentDto dto)  throws ServiceException;

    CommentDto update(CommentDto dto)  throws ServiceException;
}
