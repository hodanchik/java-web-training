package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.comment.CommentDao;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.service.TransactionSupport;
import by.epam.khadanovich.tracker.service.Transactional;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;


@Bean
@TransactionSupport
public class TaskServiceImpl implements TaskService {
    private final static Logger LOG = Logger.getLogger(TaskServiceImpl.class);

    private TaskDao taskDao;
    private CommentDao commentDao;

    public TaskServiceImpl(TaskDao taskDao, CommentDao commentDao) {
        this.taskDao = taskDao;
        this.commentDao = commentDao;
    }

    @Override
    public TaskDto getById(Long id) throws ServiceException {
        try {
            return taskDao.getById(id);
        } catch (DaoException e) {
            LOG.error("Failed get task by id ", e);
            throw new ServiceException("Failed get task by id ", e);
        }
    }

    @Override
    public List<TaskDto> find(Predicate<TaskDto> criteria) throws ServiceException {
        return null;
    }

    @Override
    public List<TaskDto> findAllTasksOfProject(long id) throws ServiceException {
        try {
            return taskDao.getByProject(id);
        } catch (DaoException e) {
            LOG.error("Failed find all tasks of project", e);
            throw new ServiceException("Failed find all tasks of project", e);
        }
    }

    @Override
    public List<TaskDto> findOwnTasks(long id) throws ServiceException {
        try {
            return taskDao.findOwnTasks(id);
        } catch (DaoException e) {
            LOG.error("Failed find own tasks", e);
            throw new ServiceException("Failed find own tasks", e);
        }
    }

    @Override
    public Map<String, List<TaskDto>> findAllTasksOfProjectSortedByStatus(long id) throws ServiceException {
        try {
            List<TaskDto> allTasksOfProject = taskDao.getByProject(id);

            Map<Integer, List<TaskDto>> sortTaskByStatus = new HashMap<>();

            for (TaskDto taskDto : allTasksOfProject) {
                sortTaskByStatus.computeIfAbsent(taskDto.getStatusId(), integer -> new LinkedList<>()).add(taskDto);
            }
            Map<String, List<TaskDto>> sortTaskByStatusString = new HashMap<>();
            sortTaskByStatusString.put("1", sortTaskByStatus.get(1));
            sortTaskByStatusString.put("2", sortTaskByStatus.get(2));
            sortTaskByStatusString.put("3", sortTaskByStatus.get(3));
            sortTaskByStatusString.put("4", sortTaskByStatus.get(4));

            return sortTaskByStatusString;

        } catch (DaoException e) {
            LOG.error("Failed find all tasks of project", e);
            throw new ServiceException("Failed find all tasks of project", e);
        }
    }


    @Override
    public List<TaskDto> findAll() throws ServiceException {
        try {
            return taskDao.findAll();
        } catch (DaoException e) {
            LOG.error("Failed find all tasks", e);
            throw new ServiceException("Failed find all tasks ", e);
        }
    }

    @Transactional
    @Override
    public boolean delete(Long id) throws ServiceException {
        try {
            List<CommentDto> allCommentByTask = commentDao.findAllByTask(id);
            for (CommentDto comment : allCommentByTask) {
                commentDao.delete(comment.getId());
            }
            return taskDao.delete(id);
        } catch (DaoException e) {
            LOG.error("Failed delete task", e);
            throw new ServiceException("Failed delete task", e);
        }
    }

    @Override
    public boolean save(TaskDto dto) throws ServiceException {
        try {
            return (taskDao.save(dto) > 0);
        } catch (DaoException e) {
            LOG.error("Failed save tasks", e);
            throw new ServiceException("Failed save tasks", e);
        }
    }

    @Transactional
    @Override
    public TaskDto update(TaskDto dto) throws ServiceException {
        try {
            if (taskDao.update(dto)) {
                return taskDao.getById(dto.getId());
            }
        } catch (DaoException e) {
            LOG.error("Failed update task", e);
            throw new ServiceException("Failed update task", e);
        }
        return dto;
    }

    @Transactional
    @Override
    public TaskDto changeStatus(TaskDto dto) throws ServiceException {
        try {
            TaskDto taskById = taskDao.getById(dto.getId());
            taskById.setStatusId(dto.getStatusId());
            if (taskDao.update(taskById)) {
                return taskDao.getById(dto.getId());
            }
        } catch (DaoException e) {
            LOG.error("Failed change status", e);
            throw new ServiceException("Failed change status", e);
        }
        return dto;
    }
}
