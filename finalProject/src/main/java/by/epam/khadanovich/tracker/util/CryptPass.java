package by.epam.khadanovich.tracker.util;
import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptPass {
    private final static Logger LOG = Logger.getLogger(CryptPass.class);
    private static MessageDigest md;

    public static String cryptPass(String pass){
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<digested.length;i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            LOG.error("Crypt password exception", e);
        }
        return null;


    }
}