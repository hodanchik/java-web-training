package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_ALL_PROJECTS_CMD_NAME)
public class ViewAllProjectsListCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewAllProjectsListCommand.class);
    private ProjectService projectService;

    public ViewAllProjectsListCommand(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            List<ProjectDto> allProjectsList = projectService.findAll();
            req.setAttribute("allProjectsList", allProjectsList);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ALL_PROJECTS_CMD_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("View all projects by.epam.khadanovich.tracker.command exception", e);
            throw new CommandException(e);
        }
    }
}