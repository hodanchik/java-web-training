package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.ConnectionManager;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Bean
public class ProjectDaoImpl implements ProjectDao {
    private final static Logger LOG = Logger.getLogger(ProjectDaoImpl.class);
    private static final String CREATE_PROJECT = "INSERT into project(creator_id, project_name, description_project) " +
            "VALUES (?, ?, ?)";
    private static final String FIND_ALL_PROJECT_BY_CREATOR_ID = "SELECT project.id, project.creator_id, " +
            "project.project_name, project.description_project, status_state.status_name, status_state.id as status_id"+
             " FROM project LEFT JOIN status_state On project.status_id = status_state.id where project.creator_id = ?";
    private static final String SELECT_ALL_PROJECT = "SELECT project.id,  project.creator_id, project.project_name," +
            "project.description_project, status_state.status_name, status_state.id as status_id" +
            " FROM project LEFT JOIN status_state On project.status_id = status_state.id";
    private static final String SELECT_BY_ID = "SELECT project.id,  project.creator_id,project.project_name, " +
            "project.description_project, status_state.status_name, status_state.id as status_id" +
            " FROM project LEFT JOIN status_state On project.status_id = status_state.id where project.id = ?";
    private static final String UPDATE_PROJECT = "UPDATE project SET project_name=?, status_id=?, description_project=? " +
            "WHERE id=?";
    private static final String  DELETE_PROJECT = "DELETE FROM project WHERE id=?";

    private ConnectionManager connectionManager;

    public ProjectDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Long save(ProjectDto projectDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(CREATE_PROJECT, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            preparedStatement.setLong(++i, projectDto.getCreatorId());
            preparedStatement.setString(++i, projectDto.getProjectName());
            preparedStatement.setString(++i, projectDto.getDescription());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                projectDto.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            LOG.error("Save project error", e);
            throw new DaoException("Save project failed ", e);
        }
        return projectDto.getId();
    }

    @Override
    public boolean update(ProjectDto projectDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(UPDATE_PROJECT)) {
            LOG.info(projectDto.getDescription());
            int i = 0;
            preparedStatement.setString(++i, projectDto.getProjectName());
            preparedStatement.setInt(++i, projectDto.getStatusId());
            preparedStatement.setString(++i, projectDto.getDescription());
            preparedStatement.setLong(++i, projectDto.getId());
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            LOG.error("Update project error", e);
            throw new DaoException("Update project failed ", e);
        }
    }
    @Override
    public boolean delete(Long projectId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_PROJECT)) {
            int i = 0;
            preparedStatement.setLong(++i, projectId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DaoException("Request or table failed delete project", e);
        }
    }

    @Override
    public ProjectDto getById(Long id) throws DaoException {
        ProjectDto projectById = new ProjectDto();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                projectById = parseResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Request or table failed get user by id ", e);
        }
        return projectById;
    }
    @Override
    public List findAll() throws DaoException {
        List<ProjectDto> allProjectList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SELECT_ALL_PROJECT)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ProjectDto projectDto = parseResultSet(resultSet);
                allProjectList.add(projectDto);
            }
        } catch (SQLException e) {
            LOG.error("Find all project error", e);
            throw new DaoException("Find all project error", e);
        }
        return allProjectList;
    }
    @Override
    public List<ProjectDto> findOwnProject(long creatorId) throws DaoException {
        List<ProjectDto> ownProjectList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_PROJECT_BY_CREATOR_ID)) {
            int i = 0;
            preparedStatement.setLong(++i, creatorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                ProjectDto projectDto = parseResultSet(resultSet);
                ownProjectList.add(projectDto);
            }
        } catch (SQLException e) {
            LOG.error("Find all own project error", e);
            throw new DaoException("Find all own project error", e);
        }
        return ownProjectList;
    }
    private ProjectDto parseResultSet(ResultSet resultSet) throws DaoException {
        try {
            long projectId = resultSet.getLong("id");
            long creatorId = resultSet.getLong("creator_id");
            String projectName = resultSet.getString("project_name");
            String description = resultSet.getString("description_project");
            String statusName = resultSet.getString("status_name");
            int statusId = resultSet.getInt("status_id");
            return new ProjectDto(projectId, creatorId, projectName, description, statusName, statusId);
        } catch (SQLException e) {
            throw new DaoException("parse Result Set project failed ", e);
        }
    }
}