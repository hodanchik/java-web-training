package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = REGISTER_SAVE_CMD_NAME)
public class RegistrationUserCommand implements Command {
    private final static Logger LOG = Logger.getLogger(RegistrationUserCommand.class);
    private UserService userService;
    private RegistrationUserValidator registrationUserValidator;

    public RegistrationUserCommand(UserService userService, RegistrationUserValidator registrationUserValidator) {
        this.userService = userService;
        this.registrationUserValidator = registrationUserValidator;
    }

    public UserService getUserService() {
        return userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String login = req.getParameter("user.login");
            String password = req.getParameter("user.password");
            String firstName = req.getParameter("user.firstName");
            String lastName = req.getParameter("user.lastName");
            String email = req.getParameter("user.email");
            String phone = req.getParameter("user.phone");
            int role_id = Integer.parseInt(req.getParameter("user.roleId"));
            int skillLevel_id = Integer.parseInt(req.getParameter("user.skillLevelId"));
            UserDto userDto = new UserDto(login, password, firstName, lastName, email, phone,
                    role_id, skillLevel_id);
            ValidationResult validate = registrationUserValidator.validate(req);
            if (validate.isValid()) {
                if (userService.checkLoginUnique(userDto)) {
                    if (userService.save(userDto)) {
                        String location = req.getContextPath() + "?commandName=" + ApplicationConstants.LOGIN_VIEW_CMD_NAME;
                        RequestUtil.sendRedirect(resp, location);
                    } else {
                        req.setAttribute("message", "message.registration.unsuccessfulRegistration");
                        req.setAttribute("user", userDto);
                        req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);
                        RequestUtil.forward(req, resp, MAIN_LAYOUT);
                    }
                } else {
                    req.setAttribute("message", "message.registration.notuniqueLogin");
                    req.setAttribute("user", userDto);
                    req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);
                    RequestUtil.forward(req, resp, MAIN_LAYOUT);
                }
            } else {
                Map<String, String> errorResult = validate.getResult();
                req.setAttribute("message", "message.registration.unsuccessfulRegistration");
                req.setAttribute("validationErrors", errorResult);
                req.setAttribute("user", userDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Registration user error ", e);
            new CommandException(e);
        }
    }
}