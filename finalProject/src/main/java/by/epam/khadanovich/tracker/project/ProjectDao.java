package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.dao.BaseDao;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.ProjectDto;

import java.util.List;

public interface ProjectDao extends BaseDao<ProjectDto, Long> {

     List<ProjectDto> findOwnProject(long id) throws DaoException;
}
