package by.epam.khadanovich.tracker.dto;

import java.util.Objects;

public class UserDto {

    private long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String roleName;
    private int role_id;
    private String skillLevelName;
    private int skillLevel_id;
    private long contactId;
    private int accStatusId;
    private String accStatusName;


    public UserDto(String login, String password) {
        this.login = login;
        this.password = password;
    }

//    public UserDto(long id, String login, String password, String firstName, String lastName, String email,
//                   String phone, String roleName, int role_id, String skillLevelName, int skillLevel_id, long contactId) {
//        this.id = id;
//        this.login = login;
//        this.password = password;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phone = phone;
//        this.roleName = roleName;
//        this.role_id = role_id;
//        this.skillLevelName = skillLevelName;
//        this.skillLevel_id = skillLevel_id;
//        this.contactId = contactId;
//    }

    public UserDto(long userId, String login, String firstName, String lastName, String email,
                   String phone, String roleName, int roleId, String skillName, int skillId, String accStatusName) {
        this.id = userId;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.roleName = roleName;
        this.role_id = roleId;
        this.skillLevelName = skillName;
        this.skillLevel_id = skillId;
        this.accStatusName = accStatusName;

    }

//    public UserDto(long id, String login, String password, String firstName, String lastName,
//                   String email, String phone, String roleName, int role_id, String skillLevelName, int skillLevel_id) {
//        this.id = id;
//        this.login = login;
//        this.password = password;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phone = phone;
//        this.roleName = roleName;
//        this.role_id = role_id;
//        this.skillLevelName = skillLevelName;
//        this.skillLevel_id = skillLevel_id;
//    }
//
//    public UserDto(long id, String login, String password, String firstName, String lastName, String email,
//                   String phone, String roleName, String skillLevelName) {
//        this.id = id;
//        this.login = login;
//        this.password = password;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phone = phone;
//        this.roleName = roleName;
//        this.skillLevelName = skillLevelName;
//    }
//
//    public UserDto(Long userId, String login, String password, String firstName, String lastName, String email,
//                   String phone, int role_id, int skillLevel_id) {
//        this.id = userId;
//        this.login = login;
//        this.password = password;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phone = phone;
//        this.role_id = role_id;
//        this.skillLevel_id = skillLevel_id;
//    }
//
    public UserDto(Long userId, String cryptPass) {
        this.id = userId;
        this.password = cryptPass;
    }
//
//    public UserDto(long userId, String login, String password, String firstName, String lastName, String email,
//                   String phone, String roleName, int roleId, String skillName, int skillId, long contactId,
//                   int accStatusId, String accStatusName) {
//        this.id = userId;
//        this.login = login;
//        this.password = password;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phone = phone;
//        this.roleName = roleName;
//        this.role_id = roleId;
//        this.skillLevelName = skillName;
//        this.skillLevel_id = skillId;
//        this.contactId = contactId;
//        this.accStatusId = accStatusId;
//        this.accStatusName = accStatusName;
//    }
//
//
//    public UserDto(long userId, String login, long contactId, String firstName, String lastName, String email,
//                   String phone, int roleId, String roleName, int skillId, String skillName, int accStatusId) {
//        this.id = userId;
//        this.login = login;
//        this.firstName = firstName;
//        this.lastName = lastName;
//        this.email = email;
//        this.phone = phone;
//        this.roleName = roleName;
//        this.role_id = roleId;
//        this.skillLevelName = skillName;
//        this.skillLevel_id = skillId;
//        this.contactId = contactId;
//        this.accStatusId = accStatusId;
//    }

    public UserDto(long userId, String login, String password, String firstName, String lastName, String email,
                   String phone, String roleName, int roleId, String skillName, int skillId, long contactId,
                   int accStatusId, String accStatusName) {
        this.id = userId;
        this.login = login;
        this.password=password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.roleName = roleName;
        this.role_id = roleId;
        this.skillLevelName = skillName;
        this.skillLevel_id = skillId;
        this.contactId = contactId;
        this.accStatusId = accStatusId;
        this.accStatusName = accStatusName;




    }


    public UserDto(String login, String password, String firstName, String lastName, String email,
                   String phone, int role_id, int skillLevel_id) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.role_id = role_id;
        this.skillLevel_id = skillLevel_id;
    }

    public UserDto(Long userId, String login, String firstName, String lastName,
                   String email, String phone, int role_id, int skillLevel_id) {
        this.id = userId;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.role_id = role_id;
        this.skillLevel_id = skillLevel_id;
    }

    public int getAccStatusId() {
        return accStatusId;
    }

    public void setAccStatusId(int accStatusId) {
        this.accStatusId = accStatusId;
    }

    public String getAccStatusName() {
        return accStatusName;
    }

    public void setAccStatusName(String accStatusName) {
        this.accStatusName = accStatusName;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    public long getContactId() {
        return contactId;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public int getSkillLevel_id() {
        return skillLevel_id;
    }

    public void setSkillLevel_id(int skillLevel_id) {
        this.skillLevel_id = skillLevel_id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getSkillLevelName() {
        return skillLevelName;
    }

    public void setSkillLevelName(String skillLevelName) {
        this.skillLevelName = skillLevelName;
    }

    public UserDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDto)) return false;
        UserDto userDto = (UserDto) o;
        return getId() == userDto.getId() &&
                getRole_id() == userDto.getRole_id() &&
                getSkillLevel_id() == userDto.getSkillLevel_id() &&
                getContactId() == userDto.getContactId() &&
                getAccStatusId() == userDto.getAccStatusId() &&
                Objects.equals(getLogin(), userDto.getLogin()) &&
                Objects.equals(getPassword(), userDto.getPassword()) &&
                Objects.equals(getFirstName(), userDto.getFirstName()) &&
                Objects.equals(getLastName(), userDto.getLastName()) &&
                Objects.equals(getEmail(), userDto.getEmail()) &&
                Objects.equals(getPhone(), userDto.getPhone()) &&
                Objects.equals(getRoleName(), userDto.getRoleName()) &&
                Objects.equals(getSkillLevelName(), userDto.getSkillLevelName()) &&
                Objects.equals(getAccStatusName(), userDto.getAccStatusName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLogin(), getPassword(), getFirstName(), getLastName(), getEmail(), getPhone(), getRoleName(), getRole_id(), getSkillLevelName(), getSkillLevel_id(), getContactId(), getAccStatusId(), getAccStatusName());
    }
}
