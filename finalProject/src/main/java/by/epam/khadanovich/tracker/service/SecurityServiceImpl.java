package by.epam.khadanovich.tracker.service;


import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.project.ProjectDao;
import by.epam.khadanovich.tracker.task.TaskDao;
import by.epam.khadanovich.tracker.user.UserDao;
import org.apache.log4j.Logger;

import java.util.List;

@Bean
@TransactionSupport
public class SecurityServiceImpl implements SecurityService {
    private final static Logger LOG = Logger.getLogger(SecurityServiceImpl.class);
    private ProjectDao projectDao;
    private TaskDao taskDao;
    private UserDao userDao;

    public SecurityServiceImpl(ProjectDao projectDao, TaskDao taskDao, UserDao userDao) {
        this.projectDao = projectDao;
        this.taskDao = taskDao;
        this.userDao = userDao;
    }

    @Override
    public boolean canAccessToProject(long userId, long projectId) throws ServiceException {
        try {
            UserDto userById = userDao.getInfoById(userId);
            if (userById.getRole_id() == 1) {
                return true;
            }
            ProjectDto projectById = projectDao.getById(projectId);
            if (userId == projectById.getCreatorId()) {
                return true;
            } else {
                List<TaskDto> tasksByProject = taskDao.getByProject(projectId);
                long count = tasksByProject.stream().filter(taskDto -> taskDto.getExecutorId() == userId).count();
                if (count != 0) {
                    return true;
                }
            }
        } catch (DaoException e) {
            LOG.error("Can access to project error", e);
            throw new ServiceException("Can access to project error", e);
        }
        return false;
    }

    @Override
    public boolean canAccessToUserPage(long currentUserId, long updateUserId) throws ServiceException {
        try {
            UserDto userById = userDao.getInfoById(currentUserId);
            if (userById.getRole_id() == 1) {
                return true;
            } else if (currentUserId == updateUserId) {
                return true;
            } else {
                return false;
            }
        } catch (DaoException e) {
            LOG.error("Can access to user page error", e);
            throw new ServiceException("Can access to user page error", e);
        }
    }
}
