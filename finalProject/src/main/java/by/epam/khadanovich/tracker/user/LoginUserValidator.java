package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.util.ValidationResult;
import by.epam.khadanovich.tracker.util.Validator;
import by.epam.khadanovich.tracker.util.ValidatorUtil;

import javax.servlet.http.HttpServletRequest;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.USER_LOGIN_VALIDATOR;

@Bean(name = USER_LOGIN_VALIDATOR)
public class LoginUserValidator implements Validator {
    private static final String LOGIN_REGEX = "^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$";
    private static final String PASSWORD_REGEX = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();
        validateLogin(validationResult, req.getParameter("user.login"));
        validatePassword(validationResult, req.getParameter("user.password"));
        return validationResult;
    }

    private void validateLogin(ValidationResult validationResult, String loginString) {
        if (ValidatorUtil.isStringEmpty(loginString)) {
            validationResult.addResult("login", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(loginString, LOGIN_REGEX)) {
            validationResult.addResult("login", "error.registration.field.login");
        }
    }

    private void validatePassword(ValidationResult validationResult, String passwordString) {
        if (ValidatorUtil.isStringEmpty(passwordString)) {
            validationResult.addResult("password", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(passwordString, PASSWORD_REGEX)) {
            validationResult.addResult("password", "error.registration.field.password");
        }
    }
}
