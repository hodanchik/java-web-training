package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.dao.BaseDao;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.CommentDto;

import java.util.List;

public interface CommentDao extends BaseDao<CommentDto, Long> {
    List<CommentDto> findAllByTask(long id) throws DaoException;
    List<CommentDto> findAllLimit(long firstRow) throws DaoException;
    long findAllCount() throws DaoException;
    List<CommentDto> findAllByTaskLimit(long taskId, long firstRow) throws DaoException;
    long findAllByTaskCount(long taskId) throws DaoException;
}
