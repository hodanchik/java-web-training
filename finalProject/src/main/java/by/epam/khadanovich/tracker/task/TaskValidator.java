package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.util.ValidationResult;
import by.epam.khadanovich.tracker.util.Validator;
import by.epam.khadanovich.tracker.util.ValidatorUtil;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.TASK_VALIDATOR;

@Bean(name = TASK_VALIDATOR)
public class TaskValidator implements Validator {
    private static final String TASK_NAME_REGEX = ".{1,45}";

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();

        validateName(validationResult, req.getParameter("task.taskName"));
        validateDate(validationResult, req.getParameter("task.dateDeadline"));

        return validationResult;
    }

    private void validateName(ValidationResult validationResult, String nameString) {
        if (ValidatorUtil.isStringEmpty(nameString)) {
            validationResult.addResult("taskName", "error.createProject.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(nameString, TASK_NAME_REGEX)) {
            validationResult.addResult("taskName", "error.createProject.field.projectName");
        }
    }

    private void validateDate(ValidationResult validationResult, String deadlineTimeStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date deadlineTime;
        try {
            deadlineTime = dateFormat.parse(deadlineTimeStr);
            Date today = new Date();
            Date dateMax = new SimpleDateFormat("yyyy-MM-dd").parse("2119-12-12");
            if (deadlineTime.before(today) || deadlineTime == null) {
                validationResult.addResult("dateDeadline", "error.createTask.field.dateDeadline.earlyDate");
            } else if (deadlineTime.after(dateMax)) {
                validationResult.addResult("dateDeadline", "error.createTask.field.dateDeadline.lateDate");
            }
        } catch (ParseException e) {
            validationResult.addResult("dateDeadline", "error.createTask.field.dateDeadline");
        }
    }
}