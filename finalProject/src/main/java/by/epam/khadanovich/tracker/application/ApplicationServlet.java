package by.epam.khadanovich.tracker.application;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;


@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "app")
public class ApplicationServlet extends HttpServlet {

    private static final long serialVersionUID = -898419077104540041L;
    private final static Logger LOG = Logger.getLogger(ApplicationServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  {
        String commandName = req.getParameter("commandName");
        Command command = ApplicationContext.getInstance().getBean(commandName);
        try {
            if (command != null) {
                command.execute(req, resp);
            } else {
                req.getRequestDispatcher(MAIN_LAYOUT).forward(req, resp);
            }
        } catch (CommandException e) {
            LOG.error("Servlet exception from Command", e);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_SERVER_ERROR);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (Exception e) {
            LOG.error("Servlet exception", e);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_BAD_REQUEST);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp){
        doGet(req, resp);
    }
}
