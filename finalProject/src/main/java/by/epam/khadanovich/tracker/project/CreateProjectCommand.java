package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = CREATE_PROJECT_CMD_NAME)
public class CreateProjectCommand implements Command {
    private final static Logger LOG = Logger.getLogger(CreateProjectCommand.class);
    private ProjectService projectService;
    private ProjectValidator projectValidator;

    public CreateProjectCommand(ProjectService projectService, ProjectValidator projectValidator) {
        this.projectService = projectService;
        this.projectValidator = projectValidator;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        UserDto creator = (UserDto) req.getSession().getAttribute("currentUser");
        long creatorId = creator.getId();
        String projectName = req.getParameter("project.projectName");
        String projectDescription = req.getParameter("project.description");
        ProjectDto projectDto = new ProjectDto(creatorId, projectName, projectDescription);
        ValidationResult validate = projectValidator.validate(req);
        if (validate.isValid()) {
            try {
                if (projectService.save(projectDto)) {
                    String location = req.getContextPath() + "?commandName=" +
                            ApplicationConstants.VIEW_OWN__PROJECT_LIST_CMD_NAME;
                    RequestUtil.sendRedirect(resp, location);
                }
            } catch (ServiceException e) {
                LOG.error("Failed to create project", e);
                throw new CommandException("Failed to create project", e);
            }
        } else {
            Map<String, String> errorResult = validate.getResult();
            req.setAttribute("validationErrors", errorResult);
            req.setAttribute("project", projectDto);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, CREATE_PROJECT_VIEW_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        }
    }
}