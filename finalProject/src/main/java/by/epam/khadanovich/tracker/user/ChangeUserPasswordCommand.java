package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.CryptPass;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = CHANGE_USER_PASSWORD_COMMAND)
public class ChangeUserPasswordCommand implements Command {

    private final static Logger LOG = Logger.getLogger(ChangeUserPasswordCommand.class);

    private UserService userService;
    private ChangePasswordValidator passwordValidator;

    public ChangeUserPasswordCommand(UserService userService, ChangePasswordValidator passwordValidator) {
        this.userService = userService;
        this.passwordValidator = passwordValidator;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long userId = Long.parseLong(req.getParameter("user.id"));
            String password = req.getParameter("user.password");
            String cryptPass = CryptPass.cryptPass(password);
            UserDto editUserDto = new UserDto(userId, cryptPass);
            ValidationResult validate = passwordValidator.validate(req);
            if (validate.isValid()) {
                UserDto userChangePasswordDto = userService.changePassword(editUserDto);
                UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
                String location;
                if (currentUser.getRoleName().equals("admin")) {
                    location = req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_ALL_USERS_CMD_NAME;
                } else {
                    req.getSession().setAttribute("currentUser", userChangePasswordDto);
                    req.setAttribute(VIEWNAME_REQ_PARAMETER, PERSONAL_PAGE_NAME);
                    location = req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_USER_PAGE_CMD_NAME;
                }
                RequestUtil.sendRedirect(resp, location);
            } else {
                Map<String, String> errorResult = validate.getResult();
                req.setAttribute("validationErrors", errorResult);
                req.setAttribute("user", editUserDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_USER_VIEW);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Change password user error ", e);
            throw new CommandException("Change password user error ", e);
        }
    }
}
