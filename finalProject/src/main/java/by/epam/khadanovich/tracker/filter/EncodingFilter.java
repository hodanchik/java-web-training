package by.epam.khadanovich.tracker.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(servletNames = {"app"}, filterName = "encoding_filter")
public class EncodingFilter implements Filter {
    private final static Logger LOG = Logger.getLogger(EncodingFilter.class);
    private static final String ENCODING = "UTF-8";
    @Override
    public void init(FilterConfig filterConfig)  {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        LOG.info("EncodingFilter");
        req.setCharacterEncoding(ENCODING);
        resp.setCharacterEncoding(ENCODING);
        resp.setContentType("text/html; charset=UTF-8");
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {

    }
}
