package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public interface TaskService {
    TaskDto getById(Long id) throws ServiceException;

    List<TaskDto> find(Predicate<TaskDto> criteria) throws ServiceException;

    List<TaskDto> findAllTasksOfProject(long id) throws ServiceException;

    List<TaskDto> findOwnTasks(long id) throws ServiceException;

    List<TaskDto> findAll() throws ServiceException;

    boolean delete(Long id) throws ServiceException;

    boolean save(TaskDto dto) throws ServiceException;

    TaskDto update(TaskDto dto) throws ServiceException;

    TaskDto changeStatus(TaskDto dto ) throws ServiceException;

    Map<String, List<TaskDto>> findAllTasksOfProjectSortedByStatus(long id) throws ServiceException;
}
