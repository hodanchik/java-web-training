package by.epam.khadanovich.tracker.entity;

import java.util.Objects;

public class UserEntity {
    private long id;
    private String login;
    private String password;
    private RoleEntity role;
    private SkillsLevelEntity skillsLevel;
    private long contactId;

    public UserEntity() {
    }


    public UserEntity(long id, String login, String password) {
        this.id = id;
        this.login = login;
        this.password = password;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleEntity getRole() {
        return role;
    }

    public void setRole(RoleEntity role) {
        this.role = role;
    }

    public SkillsLevelEntity getSkillsLevel() {
        return skillsLevel;
    }

    public void setSkillsLevel(SkillsLevelEntity skillsLevel) {
        this.skillsLevel = skillsLevel;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserEntity)) return false;
        UserEntity that = (UserEntity) o;
        return getId() == that.getId() &&
                getContactId() == that.getContactId() &&
                getLogin().equals(that.getLogin()) &&
                getPassword().equals(that.getPassword()) &&
                getRole() == that.getRole() &&
                getSkillsLevel() == that.getSkillsLevel();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLogin(), getPassword(), getRole(), getSkillsLevel(), getContactId());
    }
}
