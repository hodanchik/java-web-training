package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = UNBLOCK_USER_COMMAND)
public class UnblockUserCommand implements Command {
    private final static Logger LOG = Logger.getLogger(UnblockUserCommand.class);
    private UserService userService;

    public UnblockUserCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long userId = Long.parseLong(req.getParameter("user.id"));
            if(userService.unblock(userId)) {
                String location = req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_ALL_USERS_CMD_NAME;
                RequestUtil.sendRedirect(resp, location);
            }
            else{
                LOG.info("Command UnblockUserCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_SERVER_ERROR);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
       } catch ( ServiceException e) {
            LOG.error("Failed unblock user", e);
            throw new CommandException("Failed unblock user", e);
        }
    }
}
