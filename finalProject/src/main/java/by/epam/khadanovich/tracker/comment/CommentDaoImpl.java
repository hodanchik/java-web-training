package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.ConnectionManager;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.CommentDto;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.ROW_COUNT;

@Bean
public class CommentDaoImpl implements CommentDao {
    private final static Logger LOG = Logger.getLogger(CommentDaoImpl.class);
    private static final String CREATE_COMMENT = "INSERT into comment(task_id, creator_id, text) VALUES (?,?,?)";
    private static final String FIND_ALL_COMMENTS_BY_TASK = "SELECT comment.id, comment.task_id, " +
            "comment.create_date, comment.text, comment.creator_id, user_account.login, task.task_name " +
            "FROM comment LEFT JOIN user_account ON comment.creator_id = user_account.id LEFT JOIN task ON " +
            "comment.task_id=task.id WHERE comment.task_id =?";
    private static final String FIND_ALL_COMMENTS_BY_TASK_LIMIT = "SELECT comment.id, comment.task_id, " +
            "comment.create_date, comment.text, comment.creator_id, user_account.login, task.task_name FROM comment " +
            "LEFT JOIN user_account ON comment.creator_id = user_account.id LEFT JOIN task ON comment.task_id=task.id" +
            " WHERE comment.task_id =? ORDER BY id LIMIT %d OFFSET %d";
    private static final String DELETE_COMMENT = "DELETE FROM comment WHERE id=?";
    private static final String FIND_ALL_COMMENTS = "SELECT comment.id, comment.task_id, comment.create_date," +
            " comment.text, comment.creator_id, user_account.login, task.task_name FROM comment LEFT JOIN user_account " +
            "ON comment.creator_id = user_account.id LEFT JOIN task ON comment.task_id=task.id";
    private static final String FIND_ALL_COMMENTS_LIMIT = "SELECT comment.id, comment.task_id, comment.create_date," +
            " comment.text, comment.creator_id, user_account.login, task.task_name FROM comment LEFT JOIN user_account " +
            "ON comment.creator_id = user_account.id LEFT JOIN task ON comment.task_id=task.id ORDER BY id LIMIT %d OFFSET %d";
    private static final String FIND_ALL_COMMENTS_COUNT = "SELECT COUNT(*) FROM comment";
    private static final String FIND_ALL_COMMENTS_BY_TASK_COUNT = "SELECT COUNT(*) FROM comment WHERE comment.task_id =?";

    private ConnectionManager connectionManager;

    public CommentDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }


    @Override
    public List<CommentDto> findAllByTask(long taskId) throws DaoException {
        List<CommentDto> commentsByTask = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_COMMENTS_BY_TASK)) {
            int i = 0;
            preparedStatement.setLong(++i, taskId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CommentDto comment = parseResultSet(resultSet);
                commentsByTask.add(comment);
            }
        } catch (SQLException e) {
            LOG.error("Find all comments of task error", e);
            throw new DaoException("Find all comments of task error", e);
        }
        return commentsByTask;
    }
    @Override
    public Long save(CommentDto commentDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(CREATE_COMMENT, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            preparedStatement.setLong(++i, commentDto.getTaskId());
            preparedStatement.setLong(++i, commentDto.getCreatorId());
            preparedStatement.setString(++i, commentDto.getText());
            preparedStatement.executeUpdate();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                commentDto.setId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            LOG.error("Create comment error", e);
            throw new DaoException("Create comment error", e);
        }
        return commentDto.getId();
    }

    @Override
    public boolean update(CommentDto commentDto) throws DaoException {
        return false;
    }

    @Override
    public boolean delete(Long commentId) throws DaoException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(DELETE_COMMENT)) {
            int i = 0;
            preparedStatement.setLong(++i, commentId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new DaoException("Request or table failed delete comment", e);
        }
    }

    @Override
    public CommentDto getById(Long id) {
        return new CommentDto();
    }

    @Override
    public List<CommentDto> findAllLimit(long firstRow) throws DaoException {
        String sql = String.format(FIND_ALL_COMMENTS_LIMIT, ROW_COUNT, firstRow);
        List<CommentDto> allCommentsList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(sql)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CommentDto comment = parseResultSet(resultSet);
                allCommentsList.add(comment);
            }
        } catch (SQLException e) {
            LOG.error("Find all comments limit error", e);
            throw new DaoException("Find all comments limit error", e);
        }
        return allCommentsList;
    }

    @Override
    public long findAllCount() throws DaoException {
        long commentCount = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_COMMENTS_COUNT)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                commentCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOG.error("Find all comments count", e);
            throw new DaoException("Find all comments count", e);
        }
        return commentCount;
    }

    @Override
    public List<CommentDto> findAllByTaskLimit(long taskId, long firstRow) throws DaoException {
        String sql = String.format(FIND_ALL_COMMENTS_BY_TASK_LIMIT, ROW_COUNT, firstRow);
        List<CommentDto> commentsByTask = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(sql)) {
            int i = 0;
            preparedStatement.setLong(++i, taskId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CommentDto comment = parseResultSet(resultSet);
                commentsByTask.add(comment);
            }
        } catch (SQLException e) {
            LOG.error("Find all comments by task limit error", e);
            throw new DaoException("Find all comments by task limit error", e);
        }
        return commentsByTask;
    }

    @Override
    public long findAllByTaskCount(long taskId) throws DaoException {
        long commentCount = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_COMMENTS_BY_TASK_COUNT)) {
            int i = 0;
            preparedStatement.setLong(++i, taskId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                commentCount = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            LOG.error("Find all comments by task count", e);
            throw new DaoException("Find all comments by task count", e);
        }
        return commentCount;
    }

    @Override
    public List<CommentDto> findAll() throws DaoException {
        List<CommentDto> allCommentsList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(FIND_ALL_COMMENTS)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                CommentDto comment = parseResultSet(resultSet);
                allCommentsList.add(comment);
            }
        } catch (SQLException e) {
            LOG.error("Find all comments error", e);
            throw new DaoException("Find all comments error", e);
        }
        return allCommentsList;
    }

    private CommentDto parseResultSet(ResultSet resultSet) throws DaoException {
        try {
            long commentId = resultSet.getLong("id");
            Timestamp createDate = resultSet.getTimestamp("create_date");
            long taskId = resultSet.getLong("task_id");
            String text = resultSet.getString("text");
            long creatorId = resultSet.getLong("creator_id");
            String creatorLogin = resultSet.getString("login");
            String taskName = resultSet.getString("task_name");
            return new CommentDto(commentId, taskId, taskName, creatorId, creatorLogin, createDate, text);
        } catch (SQLException e) {
            LOG.error("parse Result Set comment failed", e);
            throw new DaoException("parse Result Set comment failed ", e);
        }
    }
}