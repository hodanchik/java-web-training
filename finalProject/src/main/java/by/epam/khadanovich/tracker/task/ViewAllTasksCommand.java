package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_ALL_TASKS_CMD_NAME)
public class ViewAllTasksCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewAllTasksCommand.class);
    private TaskService taskService;

    public ViewAllTasksCommand(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            List<TaskDto> allTasks = taskService.findAll();
            req.setAttribute("allTasksList", allTasks);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ALL_TASKS_CMD_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("View all tasks command exception", e);
            throw new CommandException(e);
        }
    }
}
