package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.util.ValidationResult;
import by.epam.khadanovich.tracker.util.Validator;
import by.epam.khadanovich.tracker.util.ValidatorUtil;

import javax.servlet.http.HttpServletRequest;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.USER_PASSWORD_VALIDATOR;

@Bean(name = USER_PASSWORD_VALIDATOR)
public class ChangePasswordValidator implements Validator {
    private static final String PASSWORD_REGEX = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();
        String passwordString = req.getParameter("user.password");
        if (ValidatorUtil.isStringEmpty(passwordString)) {
            validationResult.addResult("password", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(passwordString, PASSWORD_REGEX)) {
            validationResult.addResult("password", "error.registration.field.password");
        }
        return validationResult;
    }
}
