package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = EDIT_USER_COMMAND)
public class EditUserCommand implements Command {
    private final static Logger LOG = Logger.getLogger(EditUserCommand.class);
    private EditUserValidator editUserValidator;
    private UserService userService;

    public EditUserCommand(EditUserValidator editUserValidator, UserService userService) {
        this.editUserValidator = editUserValidator;
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {
        try {
            Long userId = Long.parseLong(req.getParameter("user.id"));
            String login = req.getParameter("user.login");
            String firstName = req.getParameter("user.firstName");
            String lastName = req.getParameter("user.lastName");
            String email = req.getParameter("user.email");
            String phone = req.getParameter("user.phone");
            int role_id = Integer.parseInt(req.getParameter("user.roleId"));
            int skillLevel_id = Integer.parseInt(req.getParameter("user.skillLevelId"));
            UserDto editUserDto = new UserDto(userId, login, firstName, lastName, email, phone,
                    role_id, skillLevel_id);
            ValidationResult validate = editUserValidator.validate(req);
            if (validate.isValid()) {
                UserDto updateUser = userService.update(editUserDto);
                UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
                String location;
                if (currentUser.getRoleName().equals("admin")) {
                    location= req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_ALL_USERS_CMD_NAME;
                    RequestUtil.sendRedirect(resp, location);
                } else {
                    req.getSession().setAttribute("currentUser", updateUser);
                    req.setAttribute(VIEWNAME_REQ_PARAMETER, PERSONAL_PAGE_NAME);
                    location= req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_USER_PAGE_CMD_NAME;
                    RequestUtil.sendRedirect(resp, location);
                }
            } else {
                Map<String, String> errorResult = validate.getResult();
                req.setAttribute("validationErrors", errorResult);
                req.setAttribute("user", editUserDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_USER_VIEW);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch ( ServiceException e) {
            LOG.error("Edit user error ", e);
            new CommandException(e);
        }
    }
}