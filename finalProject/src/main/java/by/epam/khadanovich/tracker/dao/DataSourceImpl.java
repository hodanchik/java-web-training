package by.epam.khadanovich.tracker.dao;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.jdbc.ConnectionPool;
import by.epam.khadanovich.tracker.jdbc.ConnectionPoolImpl;

import java.sql.Connection;

@Bean
public class DataSourceImpl implements DataSource {

    private final ConnectionPool connectionPool;

    public DataSourceImpl() {
        connectionPool =  ConnectionPoolImpl.getInstance();
    }

    @Override
    public Connection getConnection() {
        return connectionPool.getConnection();
    }

    @Override
    public void close() {
        connectionPool.closeAllConnection();
    }
}
