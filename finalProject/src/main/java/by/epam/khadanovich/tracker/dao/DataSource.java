package by.epam.khadanovich.tracker.dao;

import java.sql.Connection;
public interface DataSource {

    Connection getConnection() ;

    void close();
}
