package by.epam.khadanovich.tracker.core;

public class NotUniqueBeanException extends BeanRegistrationException {
    public NotUniqueBeanException(String message) {
        super(message);
    }

}
