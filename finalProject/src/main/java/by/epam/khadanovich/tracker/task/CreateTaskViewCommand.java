package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = CREATE_TASK_VIEW_NAME)
public class CreateTaskViewCommand implements Command {
    private final static Logger LOG = Logger.getLogger(CreateTaskViewCommand.class);
    private SecurityService securityService;

    public CreateTaskViewCommand(SecurityService securityService) {
        this.securityService = securityService;
    }


    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        long projectId = Long.parseLong(req.getParameter("projectId"));
        Long userId = (Long) req.getSession().getAttribute("user.id");
        try {
            if (securityService.canAccessToProject(userId, projectId)) {
                req.getSession().setAttribute("projectId", projectId);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, CREATE_TASK_VIEW_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            } else {
                LOG.info("Command Create Task View Command wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Command Create Task View Command error", e);
            throw new CommandException(e);
        }
    }
}
