package by.epam.khadanovich.tracker.dao;

import java.util.List;

public interface BaseDao<ENTITY, KEY> {

    KEY save(ENTITY entity) throws DaoException;

    boolean update(ENTITY entity) throws DaoException;

    boolean delete(KEY id) throws DaoException;

    ENTITY getById(KEY id) throws DaoException;

    List<ENTITY> findAll() throws DaoException;
}



