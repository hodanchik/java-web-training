package by.epam.khadanovich.tracker.core;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.lang.reflect.Proxy;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class BeanRegistryImpl implements BeanRegistry {
    private FactoryBean factoryBean = new FactoryBean();
    private Set<RegistryInfo> beanRegistry = new HashSet<>();

    public BeanRegistryImpl() {
    }

    @Override
    public <T> void registerBean(T bean) {
        RegistryInfo info = generateRegistryInfo(bean.getClass());
        info.setConcreteBean(bean);
        addRegistryInfo(info);
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {
        RegistryInfo info = generateRegistryInfo(beanClass);
        final Supplier<Object> factory = createFactory(info);
        info.setFactory(factory);
        addRegistryInfo(info);
    }

    private void addRegistryInfo(RegistryInfo info) {
        beanRegistry.stream()
                .filter(registryInfo -> registryInfo.getName().equals(info.getName()))
                .findFirst()
                .ifPresent(registryInfo -> {
                    throw new NotUniqueBeanException("Bean with name " + registryInfo.getName() + " already registered");
                });
        beanRegistry.add(info);
    }

    private Supplier<Object> createFactory(RegistryInfo info) {
        Class<?> clazz = info.getClazz();
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        if (constructors.length > 1) {
            throw new BeanInstantiationException(clazz.getSimpleName() + " Have more than 1 constructor is present for class");
        }

        return () -> {
            Constructor<?> constructor = constructors[0];
            if (constructor.getParameterCount() > 0) {
                Parameter[] parameters = constructor.getParameters();
                Object[] args = new Object[parameters.length];
                for (int i = 0; i < parameters.length; i++) {
                    Class<?> type = parameters[i].getType();
                    BeanQualifier beanQualifier = parameters[i].getAnnotation(BeanQualifier.class);
                    if (beanQualifier != null) {
                        Predicate<RegistryInfo> searchBean = searchInfo -> searchInfo.getName().equals(beanQualifier.value());
                        args[i] = getBean(searchBean);
                    } else {
                        args[i] = getBean(type);
                    }
                }
                try {
                    return constructor.newInstance(args);
                } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                    throw new BeanInstantiationException("Failed to instantiate bean", e);
                }
            } else {
                try {
                    return clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new BeanInstantiationException("Failed to instantiate bean", e);
                }
            }
        };
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        Bean bean = beanClass.getAnnotation(Bean.class);
        String beanName = bean != null && bean.name().trim().length() > 0 ? bean.name().trim() : null;
        Predicate<RegistryInfo> searchBean = info -> info.getName().equals(beanName) || info.getClazz().equals(beanClass) || info.getInterfaces().contains(beanClass);
        return getBean(searchBean);
    }

    @SuppressWarnings("unchecked")
    private <T> T getBean(Predicate<RegistryInfo> searchBean) {
        List<RegistryInfo> registryInfoList = beanRegistry
                .stream()
                .filter(searchBean)
                .collect(Collectors.toList());

        if (registryInfoList.size() > 1) {
            String multipleNames = registryInfoList.stream().map(RegistryInfo::getName).collect(Collectors.joining(", "));
            throw new NotUniqueBeanException("Multiple implementations found: " + multipleNames);
        } else {
            return (T) registryInfoList.stream().map(this::mapToBean).findFirst().orElse(null);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T mapToBean(RegistryInfo registryInfo) {
        T bean = (T) factoryBean.getBean(registryInfo);
        Set<RegistryInfo> availableInterceptors = beanRegistry.stream()
                .filter(RegistryInfo::isInterceptor)
                .filter(interceptorInfo -> registryInfo.getAnnotations()
                        .stream()
                        .anyMatch(a -> a.annotationType().equals(interceptorInfo.getInterceptor().clazz())))
                .collect(Collectors.toSet());

        if (availableInterceptors.isEmpty()) {
            return bean;
        } else {
            List<BeanInterceptor> interceptors = availableInterceptors.stream()
                    .map(interceptorInfo -> (BeanInterceptor) factoryBean.getBean(interceptorInfo))
                    .collect(Collectors.toList());
            return getBeanProxy(bean, registryInfo, interceptors);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T getBeanProxy(T bean, RegistryInfo info, List<BeanInterceptor> interceptors) {

        Class<?>[] toProxy = new Class[info.getInterfaces().size()];
        Class<?>[] interfaces = info.getInterfaces().toArray(toProxy);
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), interfaces,
                (proxy, method, args) -> {
                    try {
                        for (BeanInterceptor interceptor : interceptors) {
                            interceptor.before(proxy, bean, method, args);
                        }
                        Object invoked = method.invoke(bean, args);
                        for (BeanInterceptor interceptor : interceptors) {
                            interceptor.success(proxy, bean, method, args);
                        }
                        return invoked;
                    } catch (Exception e) {
                        for (BeanInterceptor interceptor : interceptors) {
                            interceptor.fail(proxy, bean, method, args);
                        }
                        throw new IllegalStateException("Exception during proxy invocation", e);
                    }
                });
    }

    @Override
    public <T> T getBean(String beanName) {
        Predicate<RegistryInfo> searchBean = info -> info.getName().equals(beanName) || info.getClazz().getSimpleName().equals(beanName);
        return getBean(searchBean);
    }

    @Override
    public <T> boolean removeBean(T bean) {

        RegistryInfo registryInfo = generateRegistryInfo(bean.getClass());
        return beanRegistry.remove(registryInfo);
    }

    @Override
    public void destroy() {
        factoryBean.destroy();
        beanRegistry.clear();
    }

    private RegistryInfo generateRegistryInfo(Class<?> beanClass) {

        Bean bean = beanClass.getAnnotation(Bean.class);
        if (bean == null) {
            throw new MissedAnnotationException(beanClass.getName() + " doesn't have @Bean annotation");
        }

        RegistryInfo info = new RegistryInfo();
        info.setClazz(beanClass);
        Class<?>[] interfaces = beanClass.getInterfaces();
        info.setInterfaces(Arrays.stream(interfaces).collect(Collectors.toSet()));

        Annotation[] annotations = beanClass.getAnnotations();
        info.setAnnotations(Arrays.stream(annotations).collect(Collectors.toSet()));

        Interceptor interceptor = beanClass.getAnnotation(Interceptor.class);
        if (interceptor != null) {
            info.setInterceptor(interceptor);
        }

        String beanName = bean.name();
        if (beanName.trim().length() > 0) {
            info.setName(beanName);
        } else if (beanClass.getInterfaces().length == 1) {
            info.setName(beanClass.getInterfaces()[0].getSimpleName());
        } else {
            info.setName(beanClass.getSimpleName());
        }
        return info;
    }


    private static class RegistryInfo {

        private String name;
        private Class<?> clazz;
        private Set<Class<?>> interfaces;
        private Set<Annotation> annotations;
        private Interceptor interceptor;
        private Supplier<?> factory;
        private Object concreteBean;
        boolean isInterceptor() {
            return this.interceptor != null;
        }


        public RegistryInfo() {
        }

        public Supplier<?> getFactory() {
            return factory;
        }

        public void setFactory(Supplier<?> factory) {
            this.factory = factory;
        }

        public Object getConcreteBean() {
            return concreteBean;
        }

        public void setConcreteBean(Object concreteBean) {
            this.concreteBean = concreteBean;
        }

        public Set<Annotation> getAnnotations() {
            return annotations;
        }

        public void setAnnotations(Set<Annotation> annotations) {
            this.annotations = annotations;
        }

        public Interceptor getInterceptor() {
            return interceptor;
        }

        public void setInterceptor(Interceptor interceptor) {
            this.interceptor = interceptor;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Class<?> getClazz() {
            return clazz;
        }

        public void setClazz(Class<?> clazz) {
            this.clazz = clazz;
        }

        public Set<Class<?>> getInterfaces() {
            return interfaces;
        }

        public void setInterfaces(Set<Class<?>> interfaces) {
            this.interfaces = interfaces;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RegistryInfo)) return false;
            RegistryInfo that = (RegistryInfo) o;
            return getName().equals(that.getName()) &&
                    getClazz().equals(that.getClazz()) &&
                    getInterfaces().equals(that.getInterfaces()) &&
                    getAnnotations().equals(that.getAnnotations()) &&
                    Objects.equals(isInterceptor(), that.isInterceptor()) &&
                    getFactory().equals(that.getFactory()) &&
                    getConcreteBean().equals(that.getConcreteBean());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getName(), getClazz(), getInterfaces(), getAnnotations(), isInterceptor(), getFactory(), getConcreteBean());
        }
    }
    private static class FactoryBean {

        private Map<RegistryInfo, Object> beans = new ConcurrentHashMap<>();

        Object getBean(RegistryInfo info) {
            if (info.getConcreteBean() != null) {
                beans.put(info, info.getConcreteBean());
            } else if (!beans.containsKey(info)) {
                final Object bean = info.getFactory().get();
                beans.put(info, bean);
            }
            return beans.get(info);
        }

        void destroy() {
            beans.clear();
        }
    }
}
