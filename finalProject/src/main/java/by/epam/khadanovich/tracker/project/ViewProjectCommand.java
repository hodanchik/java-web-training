package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.task.TaskService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = PROJECT_PAGE_NAME)
public class ViewProjectCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewProjectCommand.class);
    private ProjectService projectService;
    private TaskService taskService;
    private SecurityService securityService;

    public ViewProjectCommand(ProjectService projectService, TaskService taskService, SecurityService securityService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.securityService = securityService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            long projectId = Long.parseLong(req.getParameter("projectId"));
            long userId = (Long) req.getSession().getAttribute("user.id");
            if (securityService.canAccessToProject(userId, projectId)) {
                ProjectDto projectById = projectService.getById(projectId);
                req.getSession().setAttribute("projectId", projectId);
                req.getSession().setAttribute("project", projectById);
                Map<String, List<TaskDto>> allTasksOfProject = taskService.findAllTasksOfProjectSortedByStatus(projectId);
                req.setAttribute("allTasksOfProject", allTasksOfProject);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, PROJECT_PAGE_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            } else {
                LOG.info("Command ViewProjectCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("View project page error", e);
            throw new CommandException(e);
        }
    }
}
