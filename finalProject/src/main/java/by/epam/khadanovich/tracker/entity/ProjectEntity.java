package by.epam.khadanovich.tracker.entity;

public class ProjectEntity {
    private long id;
    private long creatorId;
    private String projectName;
    private String description;
    private int statusId;

    public ProjectEntity() {
    }

    public ProjectEntity(long creatorId, String projectName, String description) {
        this.creatorId = creatorId;
        this.projectName = projectName;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
}
