package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = EDIT_TASK_COMMAND)
public class EditTaskCommand implements Command {
    private final static Logger LOG = Logger.getLogger(EditTaskCommand.class);
    private TaskService taskService;
    private TaskValidator taskValidator;

    public EditTaskCommand(TaskService taskService, TaskValidator taskValidator) {
        this.taskService = taskService;
        this.taskValidator = taskValidator;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long id = Long.parseLong(req.getParameter("taskId"));
            String taskName = req.getParameter("task.taskName");
            long executorId = Long.parseLong(req.getParameter("task.executorId"));
            int taskStatusId = Integer.parseInt(req.getParameter("task.statusId"));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String deadlineTimeStr = req.getParameter("task.dateDeadline");
            Date deadlineTime = dateFormat.parse(deadlineTimeStr);
            TaskDto taskDto = new TaskDto(id, taskName, executorId, taskStatusId, deadlineTime);
            ValidationResult validate = taskValidator.validate(req);
            if (validate.isValid()) {
                TaskDto updateTask = taskService.update(taskDto);
                req.setAttribute("task", updateTask);
                String location = req.getContextPath() + "?commandName=" + ApplicationConstants.TASK_PAGE_NAME +
                        "&taskId=" + id+ "&page=1";
                RequestUtil.sendRedirect(resp, location);
            }
            else{
                Map<String, String> errorResult = validate.getResult();
                req.setAttribute("validationErrors", errorResult);
                req.setAttribute("task", taskDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_TASK_VIEW_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ParseException | ServiceException e) {
            LOG.error("Edit task view error", e);
            throw new CommandException(e);
        }
    }
}
