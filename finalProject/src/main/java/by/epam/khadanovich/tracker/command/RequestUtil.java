package by.epam.khadanovich.tracker.command;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RequestUtil {
    private final static Logger LOG = Logger.getLogger(RequestUtil.class);

    public static void forward(HttpServletRequest req, HttpServletResponse resp, String forwardPage) throws RequestException {
        try {
            req.getRequestDispatcher(forwardPage).forward(req, resp);
        } catch (ServletException | IOException e) {
            LOG.error("Forward error", e);
            throw new RequestException("Forward error", e);
        }
    }

    public static void sendRedirect(HttpServletResponse resp, String location) throws RequestException {
        try {
            resp.sendRedirect(location);
        } catch (IOException e) {
            LOG.error("Redirect error", e);
            throw new RequestException(e);
        }
    }
}

