package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.DELETE_COMMENT_CMD_NAME;

@Bean(name = DELETE_COMMENT_CMD_NAME)
public class DeleteCommentCommand implements Command {
    private final static Logger LOG = Logger.getLogger(DeleteCommentCommand.class);
    private CommentService commentService;

    public DeleteCommentCommand(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long commentId = Long.parseLong(req.getParameter("commentId"));
            int page = Integer.parseInt(req.getParameter("page"));
            commentService.delete(commentId);
            req.setAttribute("page", page);
            String location = req.getContextPath() + "?commandName=" + ApplicationConstants.VIEW_ALL_COMMENTS_CMD_NAME
                    + "&page=" + page;
            RequestUtil.sendRedirect(resp, location);
        } catch (ServiceException e) {
            LOG.error("Failed delete comment", e);
            throw new CommandException("Failed delete comment", e);
        }
    }
}
