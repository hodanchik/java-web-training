package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.service.ServiceException;

import java.util.List;
import java.util.function.Predicate;

public interface ProjectService {

    ProjectDto getById(Long id)  throws ServiceException;

    List<ProjectDto> find(Predicate<ProjectDto> criteria)  throws ServiceException;

    List<ProjectDto> findOwnProject(long id)  throws ServiceException;

    List<ProjectDto> findAll()  throws ServiceException;

    void delete(Long id)  throws ServiceException;

    boolean save(ProjectDto dto)  throws ServiceException;

    ProjectDto update(ProjectDto dto)  throws ServiceException;


}
