package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.ProjectDto;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = EDIT_PROJECT_VIEW_NAME)
public class EditProjectViewCommand implements Command {
    private final static Logger LOG = Logger.getLogger(EditProjectViewCommand.class);
    private ProjectService projectService;
    private SecurityService securityService;

    public EditProjectViewCommand(ProjectService projectService, SecurityService securityService) {
        this.projectService = projectService;
        this.securityService = securityService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            long projectId = Long.parseLong(req.getParameter("projectId"));
            long userId = (Long) req.getSession().getAttribute("user.id");
            if (!securityService.canAccessToProject(userId, projectId)) {
                LOG.info("Command CreateTaskViewCommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
            ProjectDto projectById = projectService.getById(projectId);
            req.setAttribute("projectId", projectId);
            req.setAttribute("project", projectById);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, EDIT_PROJECT_VIEW_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("Edit user view error", e);
            throw new CommandException(e);
        }
    }
}

