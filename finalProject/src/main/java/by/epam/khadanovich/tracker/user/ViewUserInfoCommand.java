package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_USER_PAGE_INFO)
public class ViewUserInfoCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewUserPageCommand.class);
    private UserService userService;

    public ViewUserInfoCommand(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            Long infoUserId = Long.valueOf(req.getParameter("infoUserId"));
            UserDto infoUserById = userService.getInfoById(infoUserId);
            req.getSession().setAttribute("infoUser", infoUserById);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_USER_PAGE_INFO);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("View user info page error", e);
            throw new CommandException(e);
        }
    }
}
