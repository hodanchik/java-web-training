package by.epam.khadanovich.tracker.entity;

import java.util.Date;
import java.util.Objects;

public class CommentEntity {
    private long id;
    private long taskId;
    private long creatorId;
    private Date createDate;
    private String text;

    public CommentEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommentEntity)) return false;
        CommentEntity that = (CommentEntity) o;
        return getId() == that.getId() &&
                getTaskId() == that.getTaskId() &&
                getCreatorId() == that.getCreatorId() &&
                getCreateDate().equals(that.getCreateDate()) &&
                getText().equals(that.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTaskId(), getCreatorId(), getCreateDate(), getText());
    }
}
