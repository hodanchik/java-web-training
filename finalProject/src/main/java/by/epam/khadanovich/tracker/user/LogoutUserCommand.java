package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.LOGOUT_CMD_NAME;

@Bean(name = LOGOUT_CMD_NAME)
public class LogoutUserCommand implements Command {
    private final static Logger LOG = Logger.getLogger(LogoutUserCommand.class);

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        req.getSession().removeAttribute("currentUser");
        req.getSession().invalidate();
        String location = req.getContextPath() + "?commandName=" + ApplicationConstants.LOGIN_VIEW_CMD_NAME;
        RequestUtil.sendRedirect(resp, location);
    }
}
