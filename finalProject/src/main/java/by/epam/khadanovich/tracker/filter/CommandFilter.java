package by.epam.khadanovich.tracker.filter;

import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.dto.UserDto;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@WebFilter(servletNames = {"app"}, filterName = "command_filter")
public class CommandFilter implements Filter {
    private final static Logger LOG = Logger.getLogger(CommandFilter.class);
    private static final String GUEST_ROLE = "guest";
    private List<String> commands = new ArrayList<>();
    private Map<String, List<String>> commandsRolesMap = new HashMap<>();
    private static final String SECURITY_PROPERTIES = "securityConfig.properties";
    private static final String ENCODING = "UTF-8";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SECURITY_PROPERTIES)) {
            Properties properties = new Properties();
            properties.load(inputStream);
            commands = Arrays.asList(properties.getProperty("security.commands").split(", "));
            commands.forEach(command -> {
                command = command.trim();
                List<String> roles = Arrays.asList(properties.getProperty("security.by.epam.khadanovich.tracker.command." + command).split(", "));
                roles.forEach(s -> s = s.trim());
                commandsRolesMap.put(command, roles);
            });
        } catch (IOException e) {
            throw new ServletException("Failed to read securityConfig.properties", e);
        }
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = ((HttpServletRequest) req);
        HttpServletResponse httpServletResponse = ((HttpServletResponse) resp);
        req.setCharacterEncoding(ENCODING);
        String command = req.getParameter(CMD_REQ_PARAMETER);
        HttpSession session = ((HttpServletRequest) req).getSession(false);
        String userRole;
        if (session != null) {
            UserDto user = (UserDto) session.getAttribute("currentUser");
            if (user != null) {
                userRole = user.getRoleName();
            } else {
                userRole = GUEST_ROLE;
            }
        } else {
            userRole = GUEST_ROLE;
        }
        List<String> commandRoles;
        if (command == null) {
            filterChain.doFilter(req, resp);
        }
        if ((commandRoles = commandsRolesMap.get(command)) != null) {
            if (commandRoles.contains(userRole)) {
                LOG.info("Command " + command + " was checked and completed");
                filterChain.doFilter(req, resp);
            } else {
                LOG.info("Command " + command + " wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(httpServletRequest, httpServletResponse, MAIN_LAYOUT);
            }
        } else {
            LOG.info("Command unknown");
            req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_BAD_REQUEST);
            RequestUtil.forward(httpServletRequest, httpServletResponse, MAIN_LAYOUT);
        }
    }


    @Override
    public void destroy() {
        commands.clear();
        commandsRolesMap.clear();
    }
}
