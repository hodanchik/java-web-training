package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = CREATE_TASK_CMD_NAME)
public class CreateTaskCommand implements Command {
    private final static Logger LOG = Logger.getLogger(CreateTaskCommand.class);
    private TaskService taskService;
    private TaskValidator taskValidator;

    public CreateTaskCommand(TaskService taskService, TaskValidator taskValidator) {
        this.taskService = taskService;
        this.taskValidator = taskValidator;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String taskName = req.getParameter("task.taskName");
            long projectId = (Long) req.getSession().getAttribute("projectId");
            Long creatorId = (Long) req.getSession().getAttribute("user.id");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String deadlineTimeStr = req.getParameter("task.dateDeadline");
            Date deadlineTime = dateFormat.parse(deadlineTimeStr);
            TaskDto taskDto = new TaskDto(taskName, projectId, creatorId, creatorId, deadlineTime);
            ValidationResult validate = taskValidator.validate(req);
            if (validate.isValid()) {
                if (taskService.save(taskDto)) {
                    String location = req.getContextPath() + "?commandName="
                            + ApplicationConstants.PROJECT_PAGE_NAME + "&projectId=" + projectId;
                    RequestUtil.sendRedirect(resp, location);
                } else {
                    req.setAttribute("task", taskDto);
                    req.setAttribute(VIEWNAME_REQ_PARAMETER, CREATE_TASK_VIEW_NAME);
                    RequestUtil.forward(req, resp, MAIN_LAYOUT);
                }
            } else {
                Map<String, String> errorResult = validate.getResult();
                req.setAttribute("validationErrors", errorResult);
                req.setAttribute("task", taskDto);
                req.setAttribute(VIEWNAME_REQ_PARAMETER, CREATE_TASK_VIEW_NAME);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException | ParseException e) {
            LOG.error("Create task error", e);
            throw new CommandException(e);
        }


    }
}
