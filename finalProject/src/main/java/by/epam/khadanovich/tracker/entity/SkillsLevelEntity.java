package by.epam.khadanovich.tracker.entity;

import java.util.Objects;

public class SkillsLevelEntity {
    private long id;
    private String skellLEvel;

    public SkillsLevelEntity() {
    }

    public SkillsLevelEntity(long id, String skellLEvel) {
        this.id = id;
        this.skellLEvel = skellLEvel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SkillsLevelEntity)) return false;
        SkillsLevelEntity that = (SkillsLevelEntity) o;
        return getId() == that.getId() &&
                getSkellLEvel().equals(that.getSkellLEvel());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSkellLEvel());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSkellLEvel() {
        return skellLEvel;
    }

    public void setSkellLEvel(String skellLEvel) {
        this.skellLEvel = skellLEvel;
    }
}
