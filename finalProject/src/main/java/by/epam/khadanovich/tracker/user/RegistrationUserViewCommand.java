package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = REGISTER_VIEW_CMD_NAME)
public class RegistrationUserViewCommand implements Command {
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        req.setAttribute(VIEWNAME_REQ_PARAMETER, REGISTER_VIEW_CMD_NAME);
        RequestUtil.forward(req, resp, MAIN_LAYOUT);
    }
}
