package by.epam.khadanovich.tracker.util;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.ROW_COUNT;

public class PaginationUtil {

    public static long calculateMaxPage(long count) {
        long maxPage = 1;
        if (count > ROW_COUNT) {
            if (count % ROW_COUNT > 0) {
                maxPage = (count / ROW_COUNT) + 1;
            } else maxPage = count / ROW_COUNT;
        }
        return maxPage;
    }

    public static long checkLastPage(long maxPage, long page) {
        if (maxPage < page) {
            page = maxPage;
        }
        return page;
    }
}
