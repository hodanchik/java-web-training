package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.ValidationResult;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = CREATE_COMMENT_COMMAND)
public class CreateCommentCommand implements Command {
    private final static Logger LOG = Logger.getLogger(CreateCommentCommand.class);
    private CommentService commentService;
    private CommentValidator commentValidator;

    public CreateCommentCommand(CommentService commentService, CommentValidator commentValidator) {
        this.commentService = commentService;
        this.commentValidator = commentValidator;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        String commentText = req.getParameter("comment.text");
        Long taskId = (Long) req.getSession().getAttribute("task.id");
        Long creatorId = (Long) req.getSession().getAttribute("user.id");
        long page = Long.parseLong(req.getParameter("page"));
        CommentDto comment = new CommentDto(taskId, creatorId, commentText);
        ValidationResult validate = commentValidator.validate(req);
        if (validate.isValid()) {
            try {
                if (commentService.save(comment)) {
                    String location = req.getContextPath() + "?commandName="
                            + TASK_PAGE_NAME + "&taskId=" + taskId + "&page=" + page;
                    RequestUtil.sendRedirect(resp, location);
                } else {
                    req.setAttribute("currentComment", comment);
                    req.setAttribute(VIEWNAME_REQ_PARAMETER, TASK_PAGE_NAME);
                    RequestUtil.forward(req, resp, MAIN_LAYOUT);
                }
            } catch (ServiceException e) {
                LOG.error("Create comment error", e);
                throw new CommandException(e);
            }
        } else {
            String location = req.getContextPath() + "?commandName="
                    + TASK_PAGE_NAME + "&taskId=" + taskId + "&page=" + page;
            RequestUtil.sendRedirect(resp, location);

        }
    }
}
