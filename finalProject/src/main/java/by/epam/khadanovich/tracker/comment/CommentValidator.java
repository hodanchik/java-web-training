package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.util.ValidationResult;
import by.epam.khadanovich.tracker.util.Validator;
import by.epam.khadanovich.tracker.util.ValidatorUtil;

import javax.servlet.http.HttpServletRequest;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.COMMENT_VALIDATOR;

@Bean(name = COMMENT_VALIDATOR)
public class CommentValidator implements Validator {
    private static final String COMMENT_TEXT_REGEX = ".{1,800}";

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();
        String textString = req.getParameter("comment.text");
        if (ValidatorUtil.isStringEmpty(textString)) {
            validationResult.addResult("text", "error.taskPage.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(textString, COMMENT_TEXT_REGEX)) {
            validationResult.addResult("text", "error.taskPage.field.text");
        }
        return validationResult;
    }
}
