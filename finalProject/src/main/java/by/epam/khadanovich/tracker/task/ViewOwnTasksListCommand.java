package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_OWN_TASKS_LIST_CMD_NAME)
public class ViewOwnTasksListCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewOwnTasksListCommand.class);
    private TaskService taskService;

    public ViewOwnTasksListCommand(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        UserDto currentUser = (UserDto) req.getSession().getAttribute("currentUser");
        try {
            long currentUserId = currentUser.getId();
            List<TaskDto> ownTasksList = taskService.findOwnTasks(currentUserId);
            req.setAttribute("ownTasksList", ownTasksList);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_OWN_TASKS_LIST_CMD_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("View own tasks command exception", e);
            throw new CommandException(e);
        }
    }
}

