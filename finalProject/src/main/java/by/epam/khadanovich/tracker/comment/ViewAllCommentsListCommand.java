package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.util.PaginationUtil;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = VIEW_ALL_COMMENTS_CMD_NAME)
public class ViewAllCommentsListCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ViewAllCommentsListCommand.class);
    private CommentService commentService;

    public ViewAllCommentsListCommand(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            long page = Long.parseLong(req.getParameter("page"));
            long count = commentService.findAllCount();
            long maxPage = PaginationUtil.calculateMaxPage(count);
            page = PaginationUtil.checkLastPage(maxPage, page);
            List<CommentDto> allComments = commentService.findAllLimit(page);
            req.setAttribute("allComments", allComments);
            req.setAttribute("maxPage", maxPage);
            req.setAttribute("page", page);
            req.setAttribute(VIEWNAME_REQ_PARAMETER, VIEW_ALL_COMMENTS_CMD_NAME);
            RequestUtil.forward(req, resp, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOG.error("View all comments command exception", e);
            throw new CommandException(e);
        }
    }
}