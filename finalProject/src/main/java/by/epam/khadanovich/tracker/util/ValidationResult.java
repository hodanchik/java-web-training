package by.epam.khadanovich.tracker.util;

import java.util.HashMap;
import java.util.Map;

public class ValidationResult {
    private Map<String, String> validatorResult;

    public ValidationResult() {
        validatorResult = new HashMap<>();
    }

    public boolean isValid() {
        return validatorResult.isEmpty();
    }

    public void addResult(String fieldName, String fieldMessage) {
        validatorResult.put(fieldName, fieldMessage);
    }
    public Map<String, String> getResult() {
        return validatorResult;
    }
}
