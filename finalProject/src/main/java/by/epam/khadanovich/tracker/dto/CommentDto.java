package by.epam.khadanovich.tracker.dto;

import java.sql.Timestamp;
import java.util.Date;

public class CommentDto {
    private long id;
    private long taskId;
    private long creatorId;
    private String creatorLogin;
    private Date createDate;
    private String text;
    private String taskName;

    public CommentDto() {
    }

    public CommentDto(long id, long taskId, long creatorId, String creatorLogin, Date createDate, String text) {
        this.id = id;
        this.taskId = taskId;
        this.creatorId = creatorId;
        this.creatorLogin = creatorLogin;
        this.createDate = createDate;
        this.text = text;
    }

    public CommentDto(long taskId, long creatorId, String text) {
        this.taskId = taskId;
        this.creatorId = creatorId;
        this.text = text;
    }

    public CommentDto(long commentId, long taskId, String taskName, long creatorId,
                      String creatorLogin, Timestamp createDate, String text) {
        this.id = commentId;
        this.taskId = taskId;
        this.taskName = taskName;
        this.creatorId = creatorId;
        this.creatorLogin = creatorLogin;
        this.createDate = createDate;
        this.text = text;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public long getId() {
        return id;
    }

    public String getCreatorLogin() {
        return creatorLogin;
    }

    public void setCreatorLogin(String creatorLogin) {
        this.creatorLogin = creatorLogin;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
