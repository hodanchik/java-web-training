package by.epam.khadanovich.tracker.project;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.util.ValidationResult;
import by.epam.khadanovich.tracker.util.Validator;
import by.epam.khadanovich.tracker.util.ValidatorUtil;

import javax.servlet.http.HttpServletRequest;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.PROJECT_VALIDATOR;

@Bean(name = PROJECT_VALIDATOR)
public class ProjectValidator implements Validator {
    private static final String PROJECT_NAME_REGEX = ".{1,45}";
    private static final String PROJECT_DESCRIPTION_REGEX = ".{1,350}";

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();

        validateName(validationResult, req.getParameter("project.projectName"));
        validateDescription(validationResult, req.getParameter("project.description"));

        return validationResult;
    }

    private void validateName(ValidationResult validationResult, String nameString) {
        if (ValidatorUtil.isStringEmpty(nameString)) {
            validationResult.addResult("projectName", "error.createProject.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(nameString, PROJECT_NAME_REGEX)) {
            validationResult.addResult("projectName", "error.createProject.field.projectName");
        }
    }

    private void validateDescription(ValidationResult validationResult, String descriptionString) {
        if (ValidatorUtil.isStringEmpty(descriptionString)) {
            validationResult.addResult("description", "error.createProject.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(descriptionString, PROJECT_DESCRIPTION_REGEX)) {
            validationResult.addResult("description", "error.createProject.field.description");
        }
    }

}
