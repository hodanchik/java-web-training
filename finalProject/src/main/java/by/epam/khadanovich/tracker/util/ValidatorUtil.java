package by.epam.khadanovich.tracker.util;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorUtil {

    public static boolean isStringMatchesRegex(String s, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);
        return matcher.find();
    }

    public static boolean isStringEmpty(String fieldString) {
        return (fieldString == null || fieldString.isEmpty()|| fieldString.trim().isEmpty());
    }

}
