package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.SecurityService;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.task.TaskService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.*;

@Bean(name = SELECT_USER_FOR_TASK)
public class SelectUserForTaskСommand implements Command {
    private final static Logger LOG = Logger.getLogger(SelectUserForTaskСommand.class);
    private TaskService taskService;
    private SecurityService securityService;

    public SelectUserForTaskСommand(TaskService taskService, SecurityService securityService) {
        this.taskService = taskService;
        this.securityService = securityService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            long executorId = Long.parseLong(req.getParameter("user.id"));
            long taskId = (long) req.getSession().getAttribute("taskId");
            TaskDto currentTask = taskService.getById(taskId);
            long projectId = currentTask.getProjectId();
            Long userId = (Long) req.getSession().getAttribute("user.id");
            if (securityService.canAccessToProject(userId, projectId)) {
                currentTask.setExecutorId(executorId);
                taskService.update(currentTask);
                req.getSession().setAttribute("currentTask", currentTask);
                String location = req.getContextPath() + "?commandName=" + ApplicationConstants.TASK_PAGE_NAME +
                        "&taskId=" + taskId + "&page=1";
                RequestUtil.sendRedirect(resp, location);
            } else {
                LOG.info("Command SelectUserForTaskСommand wasn't completed");
                req.setAttribute(VIEWNAME_REQ_PARAMETER, ERROR_PAGE_FORBIDDEN);
                RequestUtil.forward(req, resp, MAIN_LAYOUT);
            }
        } catch (ServiceException e) {
            LOG.error("Select user for task error", e);
            throw new CommandException(e);
        }
    }
}
