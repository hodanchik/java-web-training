package by.epam.khadanovich.tracker.application;


public class ApplicationConstants {
    public static final String MAIN_LAYOUT = "jsp/layout.jsp";
    public static final String REGISTER_VIEW_CMD_NAME = "registrationUserView";
    public static final String REGISTER_SAVE_CMD_NAME = "registerUserSave";
    public static final String VIEW_ALL_USERS_CMD_NAME = "viewAllUsers";
    public static final String VIEWNAME_REQ_PARAMETER = "viewName";
    public static final String LOGIN_CMD_NAME = "loginUser";
    public static final String LOGIN_VIEW_CMD_NAME = "loginView";
    public static final String PERSONAL_PAGE_NAME = "personalPage";
    public static final String VIEW_USER_PAGE_INFO = "infoPage";
    public static final String VIEW_USER_PAGE_CMD_NAME = "viewUserPage";
    public static final String ADMIN_PAGE_NAME = "adminPage";
    public static final String VIEW_OWN__PROJECT_LIST_CMD_NAME = "viewOwnProjectList";
    public static final String LOGOUT_CMD_NAME = "logoutUser";
    public static final String CREATE_PROJECT_CMD_NAME = "createProject";
    public static final String CREATE_PROJECT_VIEW_NAME = "createProjectView";
    public static final String PROJECT_PAGE_NAME = "projectPage";
    public static final String CMD_REQ_PARAMETER = "commandName";
    public static final String CHANGE_LANG_CMD_NAME = "changeLanguage";
    public static final String EDIT_USER_VIEW = "editUserView";
    public static final String EDIT_USER_COMMAND = "editUserCommand";
    public static final String CHANGE_USER_PASSWORD_COMMAND = "changeUserPasswordCommand";
    public static final String BLOCK_USER_COMMAND = "blockUser";
    public static final String UNBLOCK_USER_COMMAND = "unBlockUser";
    public static final String EDIT_PROJECT_VIEW_NAME = "editProjectView";
    public static final String EDIT_PROJECT_CMD = "editProjectCommand";
    public static final String CREATE_TASK_VIEW_NAME = "createTaskView";
    public static final String CREATE_TASK_CMD_NAME = "createTask";
    public static final String VIEW_OWN_TASKS_LIST_CMD_NAME = "viewOwnTasksList";
    public static final String VIEW_ALL_PROJECTS_CMD_NAME = "viewAllProjects";
    public static final String DELETE_PROJECT_COMMAND = "deleteProject";
    public static final String TASK_PAGE_NAME = "taskPage";
    public static final String CHANGE_TASK_STATUS_COMMAND = "changeTaskStatus";
    public static final String CREATE_COMMENT_COMMAND = "createComment";
    public static final String EDIT_TASK_VIEW_NAME = "editTaskView";
    public static final String EDIT_TASK_COMMAND = "editTaskCommand";
    public static final String SEARCH_USER_VIEW_NAME = "searchUserView";
    public static final String SEARCH_USER_COMMAND = "searchUserCommand";
    public static final String SELECT_USER_FOR_TASK = "selectUserForTask";
    public static final String VIEW_ALL_TASKS_CMD_NAME = "viewAllTasks";
    public static final String DELETE_TASKS_CMD_NAME = "deleteTask";
    public static final String USER_PASSWORD_VALIDATOR = "changePasswordValidator";
    public static final String USER_REGISTRATION_VALIDATOR = "registrationUserValidator";
    public static final String USER_EDIT_VALIDATOR = "editUserValidator";
    public static final String PROJECT_VALIDATOR = "projectValidator";
    public static final String ERROR_PAGE_FORBIDDEN = "404";
    public static final String ERROR_PAGE_BAD_REQUEST = "400";
    public static final String ERROR_PAGE_SERVER_ERROR = "500";
    public static final String VIEW_ALL_COMMENTS_CMD_NAME = "viewAllComments";
    public static final String DELETE_COMMENT_CMD_NAME = "deleteComment";
    public static final int ROW_COUNT = 4;
    public static final String USER_LOGIN_VALIDATOR = "loginUserValidator";
    public static final String TASK_VALIDATOR = "taskValidator";
    public static final String COMMENT_VALIDATOR = "commentValidator";

}
