package by.epam.khadanovich.tracker.application;

import by.epam.khadanovich.tracker.command.ChangeLanguageCommand;
import by.epam.khadanovich.tracker.comment.*;
import by.epam.khadanovich.tracker.core.BeanRegistry;
import by.epam.khadanovich.tracker.core.BeanRegistryImpl;
import by.epam.khadanovich.tracker.dao.ConnectionManager;
import by.epam.khadanovich.tracker.dao.ConnectionManagerImpl;
import by.epam.khadanovich.tracker.dao.DataSource;
import by.epam.khadanovich.tracker.dao.DataSourceImpl;
import by.epam.khadanovich.tracker.project.*;
import by.epam.khadanovich.tracker.service.SecurityServiceImpl;
import by.epam.khadanovich.tracker.service.TransactionInterceptor;
import by.epam.khadanovich.tracker.service.TransactionManager;
import by.epam.khadanovich.tracker.service.TransactionManagerImpl;
import by.epam.khadanovich.tracker.task.*;
import by.epam.khadanovich.tracker.user.*;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext implements BeanRegistry {

    private final static AtomicBoolean INITIALIZED = new AtomicBoolean(false);
    private final static Lock INITIALIZE_LOCK = new ReentrantLock();
    private static ApplicationContext INSTANCE;

    private BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static void initialize() {
        INITIALIZE_LOCK.lock();
        try {
            if (INSTANCE != null && INITIALIZED.get()) {
                throw new IllegalStateException("Context was already initialized");
            } else {
                ApplicationContext context = new ApplicationContext();
                context.init();
                INSTANCE = context;
                INITIALIZED.set(true);
            }
        } finally {
            INITIALIZE_LOCK.unlock();
        }
    }

    public static ApplicationContext getInstance() {
        if (INSTANCE != null && INITIALIZED.get()) {
            return INSTANCE;
        } else {
            throw new IllegalStateException("Context wasn't initialized");
        }
    }

    @Override
    public void destroy() {
        ApplicationContext context = getInstance();
        DataSource dataSource = context.getBean(DataSource.class);
        dataSource.close();
        beanRegistry.destroy();
    }

    private void init() {
        registerDataSource();
        registerClasses();
    }

    private void registerDataSource() {
        DataSource dataSource = new DataSourceImpl();
        TransactionManager transactionManager = new TransactionManagerImpl(dataSource);
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager, dataSource);
        TransactionInterceptor transactionInterceptor = new TransactionInterceptor(transactionManager);
        registerBean(dataSource);
        registerBean(transactionManager);
        registerBean(connectionManager);
        registerBean(transactionInterceptor);
    }

    private void registerClasses() {
        registerBean(CommentValidator.class);
        registerBean(TaskValidator.class);
        registerBean(LoginUserValidator.class);
        registerBean(RegistrationUserValidator.class);
        registerBean(EditUserValidator.class);
        registerBean(ChangePasswordValidator.class);
        registerBean(ProjectValidator.class);

        registerBean(UserDaoImpl.class);
        registerBean(ProjectDaoImpl.class);
        registerBean(TaskDaoImpl.class);
        registerBean(CommentDaoImpl.class);

        registerBean(SecurityServiceImpl.class);
        registerBean(UserServiceImpl.class);
        registerBean(ProjectServiceImpl.class);
        registerBean(TaskServiceImpl.class);
        registerBean(CommentServiceImpl.class);

        registerBean(ViewUserListCommand.class);
        registerBean(LoginUserCommand.class);
        registerBean(LogoutUserCommand.class);
        registerBean(RegistrationUserCommand.class);
        registerBean(RegistrationUserViewCommand.class);
        registerBean(ViewProjectCommand.class);
        registerBean(LoginUserViewCommand.class);
        registerBean(ViewUserPageCommand.class);
        registerBean(ViewProjectListCommand.class);
        registerBean(ChangeLanguageCommand.class);
        registerBean(BlockUserCommand.class);
        registerBean(EditUserViewCommand.class);
        registerBean(CreateProjectViewCommand.class);
        registerBean(CreateProjectCommand.class);
        registerBean(EditUserCommand.class);
        registerBean(ChangeUserPasswordCommand.class);
        registerBean(EditProjectViewCommand.class);
        registerBean(EditProjectCommand.class);
        registerBean(CreateTaskViewCommand.class);
        registerBean(CreateTaskCommand.class);
        registerBean(ViewOwnTasksListCommand.class);
        registerBean(ViewAllProjectsListCommand.class);
        registerBean(DeleteProjectCommand.class);
        registerBean(ViewTaskCommand.class);
        registerBean(ChangeTaskStatusCommand.class);
        registerBean(CreateCommentCommand.class);
        registerBean(ViewUserInfoCommand.class);
        registerBean(EditTaskViewCommand.class);
        registerBean(EditTaskCommand.class);
        registerBean(DeleteTaskCommand.class);
        registerBean(SearchUserCommand.class);
        registerBean(SearchUserViewCommand.class);
        registerBean(SelectUserForTaskСommand.class);
        registerBean(ViewAllTasksCommand.class);
        registerBean(UnblockUserCommand.class);
        registerBean(ViewAllCommentsListCommand.class);
        registerBean(DeleteCommentCommand.class);
    }

    @Override
    public <T> void registerBean(T bean) {

        this.beanRegistry.registerBean(bean);
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {

        this.beanRegistry.registerBean(beanClass);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return this.beanRegistry.getBean(beanClass);
    }

    @Override
    public <T> T getBean(String name) {
        return this.beanRegistry.getBean(name);
    }

    @Override
    public <T> boolean removeBean(T bean) {
        return this.beanRegistry.removeBean(bean);
    }
}
