package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.application.ApplicationConstants;
import by.epam.khadanovich.tracker.command.ChangeLanguageCommand;
import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.CommandException;
import by.epam.khadanovich.tracker.command.RequestUtil;
import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.CHANGE_TASK_STATUS_COMMAND;

@Bean(name = CHANGE_TASK_STATUS_COMMAND)
public class ChangeTaskStatusCommand implements Command {
    private final static Logger LOG = Logger.getLogger(ChangeLanguageCommand.class);
    private TaskService taskService;

    public ChangeTaskStatusCommand(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            long taskId = Long.parseLong(req.getParameter("taskId"));
            int statusId = Integer.parseInt(req.getParameter("statusId"));
            TaskDto taskDto = new TaskDto(taskId, statusId);
            TaskDto changeTask = taskService.changeStatus(taskDto);
            String location = req.getContextPath() + "?commandName=" +
                    ApplicationConstants.PROJECT_PAGE_NAME + "&projectId=" + changeTask.getProjectId();
            RequestUtil.sendRedirect(resp, location);
        } catch (ServiceException e) {
            LOG.error("Failed change task status", e);
            throw new CommandException("Failed change task status", e);
        }
    }
}
