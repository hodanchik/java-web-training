package by.epam.khadanovich.tracker.entity;

import java.util.Objects;

public class StatusEntity {
   private int id;
   private String statusName;

    public StatusEntity() {
    }

    public StatusEntity(int id, String statusName) {
        this.id = id;
        this.statusName = statusName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StatusEntity)) return false;
        StatusEntity that = (StatusEntity) o;
        return getId() == that.getId() &&
                getStatusName().equals(that.getStatusName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getStatusName());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
