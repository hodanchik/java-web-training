package by.epam.khadanovich.tracker.jdbc;

import org.apache.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.ResourceBundle;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPoolImpl implements ConnectionPool {

    private final static Logger LOGGER = Logger.getLogger(ConnectionPoolImpl.class);
    private static int poolCapacity;
    private static String url;
    private static String user;
    private static String pass;
    private static String dbDriver;

    private final static Lock connectionLock = new ReentrantLock();
    private final Condition emptyPool = connectionLock.newCondition();
    private final LinkedList<Connection> freeConnections = new LinkedList<>();
    private final LinkedList<Connection> usedConnections = new LinkedList<>();
    private static volatile ConnectionPoolImpl INSTANCE;

    public static ConnectionPoolImpl getInstance() {

        if (INSTANCE == null) {
            connectionLock.lock();
            try {
                if (INSTANCE == null) {
                    INSTANCE = new ConnectionPoolImpl();
                    INSTANCE.initDriver();
                }
            } finally {
                connectionLock.unlock();
            }
        }
        return INSTANCE;
    }

    private ConnectionPoolImpl() {
        ResourceBundle resource = ResourceBundle.getBundle("database");
        url = resource.getString("db.url");
        user = resource.getString("db.user");
        pass = resource.getString("db.password");
        dbDriver = resource.getString("db.driver");
        poolCapacity = Integer.parseInt(resource.getString("db.poolCapacity"));
    }

    private void initDriver() {
        try {
            Class.forName(dbDriver);
        } catch (ClassNotFoundException e) {
            LOGGER.error("Driver cannot be found", e);
        }
    }

    @Override
    public Connection getConnection() {
        connectionLock.lock();
        Connection proxyConnection = null;
        try {
            if (freeConnections.isEmpty() && usedConnections.size() == poolCapacity) {
                try {
                    emptyPool.await();
                } catch (InterruptedException e) {
                    LOGGER.error("Get connection error", e);
                }
            }
            if (freeConnections.isEmpty() && usedConnections.size() < poolCapacity) {
                Connection connection = DriverManager.getConnection(url, user, pass);
                freeConnections.add(connection);
            } else if (freeConnections.isEmpty()) {
                LOGGER.error("Get Maximum pool size was reached");
            }
            Connection connection = freeConnections.removeFirst();
            usedConnections.add(connection);
            proxyConnection = createProxyConnection(connection);
        } catch (SQLException e) {
            LOGGER.error("Get connection error", e);
        } finally {
            connectionLock.unlock();
        }
        return proxyConnection;
    }


    private Connection createProxyConnection(Connection connection) {

        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    void releaseConnection(Connection connection) {
        try {
            connectionLock.lock();
            if (freeConnections.size() >= poolCapacity) {
                LOGGER.error("Release Maximum pool size was reached");
            }
            usedConnections.remove(connection);
            freeConnections.add(connection);
            emptyPool.signal();
        } catch (Exception e) {
            LOGGER.error("Release connection error", e);
        } finally {
            connectionLock.unlock();
        }
    }

    @Override
    public void closeAllConnection() {
        try {
            for (Connection connection : freeConnections)
                connection.close();
            for (Connection connection : usedConnections)
                connection.close();
        } catch (SQLException e) {
            LOGGER.error("Close all connection error", e);
        }
    }
}




















