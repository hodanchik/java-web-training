package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.util.ValidationResult;
import by.epam.khadanovich.tracker.util.Validator;
import by.epam.khadanovich.tracker.util.ValidatorUtil;

import javax.servlet.http.HttpServletRequest;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.USER_EDIT_VALIDATOR;

@Bean(name = USER_EDIT_VALIDATOR)
public class EditUserValidator implements Validator {
    private static final String FIRST_NAME_REGEX = "^[_a-zA-Zа-яА-ЯёЁ ]{1,30}$";
    private static final String LAST_NAME_REGEX = "^[_a-zA-Zа-яА-ЯёЁ ]{1,30}$";
    private static final String EMAIL_REGEX = "^([a-zA-z0-9_-]+\\.)*[a-zA-z0-9_-]+@[a-zA-z0-9_-]+(\\.[a-zA-z0-9_-]+)*\\.[a-zA-z]{2,6}$";
    private static final String PHONE_REGEX = "^(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){10,14}(\\s*)?$";

    @Override
    public ValidationResult validate(HttpServletRequest req) {
        ValidationResult validationResult = new ValidationResult();

        validateFirstName(validationResult, req.getParameter("user.firstName"));
        validateLastName(validationResult, req.getParameter("user.lastName"));
        validateEmail(validationResult, req.getParameter("user.email"));
        validatePhone(validationResult, req.getParameter("user.phone"));

        return validationResult;

    }

    private void validateFirstName(ValidationResult validationResult, String firstNameString) {
        if (ValidatorUtil.isStringEmpty(firstNameString)) {
            validationResult.addResult("password", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(firstNameString, FIRST_NAME_REGEX)) {
            validationResult.addResult("password", "error.registration.field.firstName");
        }
    }

    private void validateLastName(ValidationResult validationResult, String lastNameString) {
        if (ValidatorUtil.isStringEmpty(lastNameString)) {
            validationResult.addResult("lastName", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(lastNameString, LAST_NAME_REGEX)) {
            validationResult.addResult("lastName", "error.registration.field.lastName");
        }
    }

    private void validateEmail(ValidationResult validationResult, String emailString) {
        if (ValidatorUtil.isStringEmpty(emailString)) {
            validationResult.addResult("email", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(emailString, EMAIL_REGEX)) {
            validationResult.addResult("email", "error.registration.field.email");
        }
    }

    private void validatePhone(ValidationResult validationResult, String phoneString) {
        if (ValidatorUtil.isStringEmpty(phoneString)) {
            validationResult.addResult("phone", "error.registration.field.null");
        } else if (!ValidatorUtil.isStringMatchesRegex(phoneString, PHONE_REGEX)) {
            validationResult.addResult("phone", "error.registration.field.phone");
        }
    }

}
