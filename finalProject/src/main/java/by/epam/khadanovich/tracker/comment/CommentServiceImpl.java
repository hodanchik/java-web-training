package by.epam.khadanovich.tracker.comment;

import by.epam.khadanovich.tracker.core.Bean;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.CommentDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import by.epam.khadanovich.tracker.service.TransactionSupport;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.function.Predicate;

import static by.epam.khadanovich.tracker.application.ApplicationConstants.ROW_COUNT;

@Bean
@TransactionSupport
public class CommentServiceImpl implements CommentService {
    private final static Logger LOG = Logger.getLogger(CommentServiceImpl.class);
    private CommentDao commentDao;

    public CommentServiceImpl(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Override
    public CommentDto getById(Long id) throws ServiceException {
        return null;
    }

    @Override
    public List<CommentDto> find(Predicate<CommentDto> criteria) throws ServiceException {
        return null;
    }

    @Override
    public List<CommentDto> findAllByTask(long id) throws ServiceException {
        try {
            return commentDao.findAllByTask(id);
        } catch (DaoException e) {
            LOG.error("Failed find all comment by task", e);
            throw new ServiceException("Failed find all comment by task", e);
        }
    }

    @Override
    public long findAllCount() throws ServiceException {
        try {
            return commentDao.findAllCount();
        } catch (DaoException e) {
            LOG.error("Failed find all comment count", e);
            throw new ServiceException("Failed find all comment count", e);
        }
    }

    @Override
    public long findAllByTaskCount(long taskId) throws ServiceException {
        try {
            return commentDao.findAllByTaskCount(taskId);
        } catch (DaoException e) {
            LOG.error("Failed find all  comment by task count", e);
            throw new ServiceException("Failed find all comment by task count", e);
        }
    }

    @Override
    public List<CommentDto> findAll() throws ServiceException {
        try {
            return commentDao.findAll();
        } catch (DaoException e) {
            LOG.error("Failed find all comment", e);
            throw new ServiceException("Failed find all comment", e);
        }
    }

    @Override
    public List<CommentDto> findAllLimit(long page) throws ServiceException {
        try {
            long firstRow = (page - 1) * ROW_COUNT;
            return commentDao.findAllLimit(firstRow);
        } catch (DaoException e) {
            LOG.error("Failed find all limit  comment", e);
            throw new ServiceException("Failed find all limit  comment", e);
        }
    }

    @Override
    public List<CommentDto> findAllByTaskLimit(long taskId, long page) throws ServiceException {
        try {
            long firstRow = (page - 1) * ROW_COUNT;
            return commentDao.findAllByTaskLimit(taskId, firstRow);
        } catch (DaoException e) {
            LOG.error("Failed find all limit  comment", e);
            throw new ServiceException("Failed find all limit  comment", e);
        }
    }

    @Override
    public void delete(Long id) throws ServiceException {
        try {
            commentDao.delete(id);
        } catch (DaoException e) {
            LOG.error("Failed find delete comment", e);
            throw new ServiceException("Failed delete comment", e);
        }
    }

    @Override
    public boolean save(CommentDto dto) throws ServiceException {
        try {
            return commentDao.save(dto) > 0;
        } catch (DaoException e) {
            LOG.error("Failed save comment", e);
            throw new ServiceException("Failed save comment", e);
        }
    }

    @Override
    public CommentDto update(CommentDto dto) throws ServiceException {
        return dto;
    }
}
