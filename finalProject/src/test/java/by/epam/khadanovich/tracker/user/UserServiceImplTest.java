package by.epam.khadanovich.tracker.user;

import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.UserDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RunWith(JUnit4.class)
public class UserServiceImplTest {
    private UserService userService;
    private UserDto testUser;
    private UserDto loginUser;
    private UserDto registerUserSuccess;
    private UserDto registerUnsuccessUser;
    private List<UserDto> userListByCriteria = new ArrayList<>();
    @Before
    public void setUp() throws SQLException, DaoException {
        testUser = new UserDto(1L, "test", "testName", "testLastName",
                "test@gmail.ru", "+375446346545", "dev", 3,
                "superSkill", 1, "active");
        loginUser = new UserDto("test", "pass");
        registerUnsuccessUser =new UserDto("test", "pass", "testName", "testLastName",
                "test@gmail.ru", "+375446346545",2,2);
        registerUserSuccess = new UserDto("origenalName", "pass", "testName", "testLastName",
                "test@gmail.ru", "+375446346545",3,2);
        userListByCriteria.add(testUser);
        UserDao mockUserDao = mockUserDao();
        userService = new UserServiceImpl(mockUserDao);
    }

    private UserDao mockUserDao() throws DaoException {
        UserDao mockUserDao = Mockito.mock(UserDao.class);
        Mockito.when(mockUserDao.checkLoginUnique(registerUnsuccessUser)).thenReturn(false);
        Mockito.when(mockUserDao.checkLoginUnique(registerUserSuccess)).thenReturn(true);
        Mockito.when(mockUserDao.loginUser(loginUser)).thenReturn(testUser);
        Mockito.when(mockUserDao.block(1L)).thenReturn(true);
        Mockito.when(mockUserDao.unblock(1L)).thenReturn(true);
        Mockito.when(mockUserDao.save(registerUserSuccess)).thenReturn(1L);
        Mockito.when(mockUserDao.changePassword(testUser)).thenReturn(testUser);
        Mockito.when(mockUserDao.getInfoById(1L)).thenReturn(testUser);
        Mockito.when(mockUserDao.getById(1L)).thenReturn(testUser);
        Mockito.when(mockUserDao.find("test")).thenReturn(userListByCriteria);
        Mockito.when(mockUserDao.saveContact(registerUserSuccess)).thenReturn(1L);
        
        return mockUserDao;
    }

    @Test
    public void shouldGetById() throws ServiceException {
        UserDto userById = userService.getById(1L);
        Assert.assertEquals(userById, testUser);
    }

    @Test
    public void shouldGetInfoById() throws ServiceException {
        UserDto userById = userService.getInfoById(1L);
        Assert.assertEquals(userById, testUser);
    }

    @Test
    public void shouldFindByCriteria() throws ServiceException {
        List<UserDto> usersListByCriteria = userService.find("test");
        Assert.assertEquals(usersListByCriteria, userListByCriteria);
    }
    
    @Test
    public void shouldBlock() throws ServiceException {
        boolean block = userService.block(1L);
        Assert.assertTrue(block);
    }

    @Test
    public void shouldUnblock() throws ServiceException {
        boolean unblock = userService.unblock(1L);
        Assert.assertTrue(unblock);
    }

    @Test
    public void shouldCheckLoginUniqueSuccess() throws ServiceException {
        boolean unique = userService.checkLoginUnique(registerUserSuccess);
        Assert.assertTrue(unique);
    }
    @Test
    public void shouldCheckLoginUniqueUnSuccess() throws ServiceException {
        boolean unique = userService.checkLoginUnique(registerUnsuccessUser);
        Assert.assertFalse(unique);
    }
    
    @Test
    public void save() throws ServiceException {
        boolean successSave = userService.save(registerUserSuccess);
        Assert.assertTrue(successSave);
    }

}