package by.epam.khadanovich.tracker.core;

import by.epam.khadanovich.tracker.command.Command;
import by.epam.khadanovich.tracker.command.TestCommand1;
import by.epam.khadanovich.tracker.command.TestCommand2;
import by.epam.khadanovich.tracker.command.TestCommand3;
import by.epam.khadanovich.tracker.dao.ConnectionManagerImpl;
import by.epam.khadanovich.tracker.user.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BeanRegistryTest {

    @Test
    public void shouldRegisterAndReturnServiceByInterface() {

        BeanRegistry provider = new BeanRegistryImpl();
        UserService serviceImpl = new UserServiceImpl(null);
        provider.registerBean(serviceImpl);
        UserService serviceFound = provider.getBean(UserService.class);
        Assert.assertEquals(serviceImpl, serviceFound);
    }

    @Test
    public void shouldRegisterAndReturnServiceByName() {

        BeanRegistry provider = new BeanRegistryImpl();
        UserService serviceImpl = new UserServiceImpl(null);
        provider.registerBean(serviceImpl);
        UserService serviceFound = provider.getBean("UserService");
        Assert.assertEquals(serviceImpl, serviceFound);
    }

    @Test
    public void shouldRegisterAndInjectAndReturnNewServiceByName() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(UserServiceImpl.class);
        provider.registerBean(LoginUserCommand.class);
        Command commandFound = provider.getBean("loginUser");
        Assert.assertNotNull(commandFound);
        Assert.assertTrue(commandFound instanceof LoginUserCommand);
        UserService userService = ((LoginUserCommand) commandFound).getUserService();
        Assert.assertNotNull(userService);
    }

    @Test
    public void shouldRegisterAndInjectAndReturnNewServiceByClass() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(RegistrationUserCommand.class);
        provider.registerBean(UserServiceImpl.class);
        RegistrationUserCommand serviceFound = provider.getBean(RegistrationUserCommand.class);
        Assert.assertNotNull(serviceFound);
        UserService userService = serviceFound.getUserService();
        Assert.assertNotNull(userService);
    }

    @Test
    public void shouldRegisterAndInjectSameAndReturnNewServiceByClass() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(RegistrationUserCommand.class);
        provider.registerBean(UserServiceImpl.class);
        provider.registerBean(UserDaoImpl.class);
        provider.registerBean(ConnectionManagerImpl.class);
        UserService firstService = provider.getBean("UserService");
        RegistrationUserCommand serviceFound = provider.getBean(RegistrationUserCommand.class);
        Assert.assertNotNull(serviceFound);
        UserService userService = serviceFound.getUserService();
        Assert.assertEquals(firstService, userService);
    }

    @Test(expected = NotUniqueBeanException.class)
    public void shouldThrowExceptionOnDuplicateName() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(TestCommand1.class);
        provider.registerBean(TestCommand2.class);
    }

    @Test()
    public void shouldRegisterMultipleImplementations() {

        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(TestCommand1.class);
        provider.registerBean(TestCommand3.class);
    }
}
