package by.epam.khadanovich.tracker.task;

import by.epam.khadanovich.tracker.comment.CommentDao;
import by.epam.khadanovich.tracker.dao.DaoException;
import by.epam.khadanovich.tracker.dto.TaskDto;
import by.epam.khadanovich.tracker.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskServiceImplTest {
    private TaskService taskService;

    private TaskDto createdTask;
    private TaskDto testTaskOne;
    private TaskDto testTaskTwo;
    private TaskDto testTaskThree;
    private List<TaskDto> tasksByProject = new ArrayList<>();
    private List<TaskDto> allTasks = new ArrayList<>();


    @Before
    public void setUp() throws SQLException, DaoException {
        createdTask = new TaskDto("testName", 1, 1, new Date());

        testTaskOne = new TaskDto(2L, "taskOne", 1, "testProj", 1, 1, "user",
                1, "new", new Date());
        testTaskTwo = new TaskDto(3L, "taskTwo", 1, "testProj", 1, 1, "user",
                2, "active", new Date());
        testTaskThree = new TaskDto(4L, "taskThree", 2, "testProj2", 2, 2, "userTwo",
                2, "active", new Date());
        tasksByProject.add(testTaskOne);
        tasksByProject.add(testTaskTwo);
        allTasks.add(testTaskOne);
        allTasks.add(testTaskTwo);
        allTasks.add(testTaskThree);
        TaskDao mockTaskDao = mockTaskDao();
        CommentDao mockCommentDao = Mockito.mock(CommentDao.class);
        taskService = new TaskServiceImpl(mockTaskDao, mockCommentDao);
    }

    private TaskDao mockTaskDao() throws DaoException {
        TaskDao mockTaskDao = Mockito.mock(TaskDao.class);
        Mockito.when(mockTaskDao.save(createdTask)).thenReturn(1L);
        Mockito.when(mockTaskDao.delete(2L)).thenReturn(true);
        Mockito.when(mockTaskDao.findAll()).thenReturn(allTasks);
        Mockito.when(mockTaskDao.getByProject(1L)).thenReturn(tasksByProject);
        Mockito.when(mockTaskDao.findOwnTasks(1L)).thenReturn(tasksByProject);
        Mockito.when(mockTaskDao.getById(3L)).thenReturn(testTaskTwo);
        Mockito.when(mockTaskDao.update(testTaskTwo)).thenReturn(true);
        return mockTaskDao;
    }

    @Test
    public void shouldGetById() throws ServiceException {
        TaskDto taskById = taskService.getById(3L);
        Assert.assertEquals(taskById, testTaskTwo);
    }


    @Test
    public void shouldFindAllTasksOfProject() throws ServiceException {
        List<TaskDto> allTasksOfProject = taskService.findAllTasksOfProject(1L);
        Assert.assertEquals(allTasksOfProject, tasksByProject);
    }

    @Test
    public void shouldFindOwnTasks() throws ServiceException {
        List<TaskDto> allTasksOfProject = taskService.findOwnTasks(1L);
        Assert.assertEquals(allTasksOfProject, tasksByProject);
    }

    @Test
    public void shouldFindAll() throws ServiceException {
        List<TaskDto> allTasks = taskService.findAll();
        Assert.assertEquals(allTasks, allTasks);
    }

    @Test
    public void shouldDelete() throws ServiceException {
        boolean delete = taskService.delete(2L);
        Assert.assertTrue(delete);
    }

    @Test
    public void shouldSave() throws ServiceException {
        boolean save = taskService.save(createdTask);
        Assert.assertTrue(save);
    }

    @Test
    public void shouldUpdate() throws ServiceException {
        TaskDto updateTask = taskService.update(testTaskTwo);
        Assert.assertEquals(updateTask, testTaskTwo);
    }

    @Test
    public void shouldchangeStatus() throws ServiceException {
        TaskDto taskDto = taskService.changeStatus(testTaskTwo);
        Assert.assertEquals(taskDto, testTaskTwo);
    }
}