package by.epam.khadanovich.tracker.command;

import by.epam.khadanovich.tracker.core.Bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "testCommand3")
public class TestCommand3 implements Command {
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) {

    }
}
