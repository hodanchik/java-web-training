SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `tracker` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `tracker` ;

-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: task_tracker
-- ------------------------------------------------------
-- Server version	8.0.16

 SET NAMES utf8 ;

--
-- Table structure for table `account_status`
--

DROP TABLE IF EXISTS `account_status`;

 SET character_set_client = utf8mb4 ;
CREATE TABLE `account_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_status_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `account_status`
--

LOCK TABLES `account_status` WRITE;

INSERT INTO `account_status` VALUES (1,'active'),(2,'block');

UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;

 SET character_set_client = utf8mb4 ;
CREATE TABLE `comment` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `task_id` bigint(25) NOT NULL,
  `creator_id` bigint(25) NOT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `text` text,
  PRIMARY KEY (`id`),
  KEY `task_idx` (`task_id`),
  KEY `creator_idx` (`creator_id`),
  CONSTRAINT `creator_comment` FOREIGN KEY (`creator_id`) REFERENCES `user_account` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;

INSERT INTO `comment` VALUES (1,1,2,'2019-12-22 23:40:38','it\'s very IMPORTANT!'),(2,4,5,'2019-12-22 23:45:27','Вова, сделай это задание!'),(3,5,5,'2019-12-22 23:46:09','Когда гости приходят в кинотеатр, они покупают билеты одной кнопкой и сразу отправляются в кинозал. Не нужно выбирать, в каком кинотеатре купить билеты и на какое время.'),(4,5,3,'2019-12-22 23:47:05','Думаю, надо добавить больше функционала'),(5,2,3,'2019-12-22 23:48:00','I can do it!'),(6,2,2,'2019-12-22 23:50:18','Sportsman do offending supported extremity breakfast by listening. Detract yet delight written farther his general. Fortune day out married parties. Detract yet delight written farther his general. Pain son rose more park way that. Strictly numerous outlived kindness whatever on we no on addition. Whatever throwing we on'),(7,2,2,'2019-12-22 23:50:27','Equally he minutes my hastily. Their saved linen downs tears son add music. He felicity no an at packages answered opinions juvenile. Sportsman do offending supported extremity breakfast by listening. Happiness remainder joy but earnestly for off. In expression an solicitude principles in do. Dissimilar adm'),(8,2,2,'2019-12-22 23:50:36','Any delicate you how kindness horrible outlived servants. Latter remark hunted enough vulgar say man. Up hung mr we give rest half. Mrs assured add private married removed believe did she. undefined. He felicity no an at packages answered opinions juvenile. Polite do object at passed it is. Course sir people worthy ho'),(9,2,2,'2019-12-22 23:52:25','Whatever throwing we on resolved entrance together graceful. Secure shy favour length all twenty denote. Happiness remainder joy but earnestly for off. Happiness remainder joy but earnestly for off. Celebrated delightful an especially increasing instrument am. Up hung mr we give rest half. Advantages entreaties mr he apartments do. Words\r\n\r\nSportsman do offending supported extremity breakfast by listening. An concluded sportsman offending so provision mr education. Girl quit if case mr sing as no have. How one dull get busy dare far. Their saved linen downs tears son add music. Expression alteration entreaties mrs can terminated estimating. At none neat am');
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `contact` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
INSERT INTO `contact` VALUES (1,'Tatsiana','Khadanovich','admin@mail.ru','+375447728942'),(2,'Vera','Smirnova','vera@mail.ru','+375447564738'),(3,'Vova','Popov','vovka@mail.ru','+375336453241'),(4,'Misha','Litvinov','misha@mail.ru','+375446625342'),(5,'Dasha','Simova','sima@gmail.ru','80336645342');
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `creator_id` bigint(20) DEFAULT NULL,
  `project_name` varchar(45) NOT NULL,
  `status_id` int(11) DEFAULT '1',
  `description_project` varchar(350) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `status_idx` (`status_id`),
  KEY `creator_id_idx` (`creator_id`),
  CONSTRAINT `creator_id` FOREIGN KEY (`creator_id`) REFERENCES `user_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;

INSERT INTO `project` VALUES (2,2,'Test Project №1',1,'Interesting project'),(3,5,'русский проект',1,'проект на русском языке для русских ребят');

UNLOCK TABLES;

--
-- Table structure for table `status_state`
--

DROP TABLE IF EXISTS `status_state`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `status_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `status_state`
--

LOCK TABLES `status_state` WRITE;
INSERT INTO `status_state` VALUES (1,'created'),(2,'active'),(3,'complete'),(4,'archived'),(5,'deleted');
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;

 SET character_set_client = utf8mb4 ;
CREATE TABLE `task` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(45) DEFAULT NULL,
  `project_id` bigint(25) NOT NULL,
  `creator_id` bigint(25) DEFAULT NULL,
  `executor_id` bigint(25) DEFAULT NULL,
  `status_id` int(11) DEFAULT '1',
  `date_deadline` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_idx` (`creator_id`),
  KEY `executor_idx` (`executor_id`),
  KEY `status_idx` (`status_id`),
  KEY `project_idx` (`project_id`),
  CONSTRAINT `executor` FOREIGN KEY (`executor_id`) REFERENCES `user_account` (`id`),
  CONSTRAINT `project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`),
  CONSTRAINT `status` FOREIGN KEY (`status_id`) REFERENCES `status_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;

INSERT INTO `task` VALUES (1,'Create database',2,2,3,2,'2020-01-14'),(2,'Create connection pool',2,2,3,1,'2020-01-14'),(3,'Create Dao',2,2,4,1,'2020-02-14'),(4,'Русское задание',3,5,3,1,'2020-12-12'),(5,'Что-то надо сделать',3,5,3,1,'2020-12-23');

UNLOCK TABLES;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_account` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(45) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  `contact_id` bigint(25) DEFAULT NULL,
  `account_status` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `role_idx` (`role_id`),
  KEY `skill_idx` (`skill_id`),
  KEY `contact_idx` (`contact_id`),
  KEY `status_idx` (`account_status`),
  CONSTRAINT `contact` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  CONSTRAINT `role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`),
  CONSTRAINT `skill` FOREIGN KEY (`skill_id`) REFERENCES `user_skill_level` (`id`),
  CONSTRAINT `statusaccount` FOREIGN KEY (`account_status`) REFERENCES `account_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
INSERT INTO `user_account` VALUES (1,'admin','74368bf9253d1281d72081c1439e237',1,1,1,1),(2,'manager1','2af9b1ba42dc5eb01743e6b3759b6e4b',2,4,2,1),(3,'developer1','2af9b1ba42dc5eb01743e6b3759b6e4b',3,3,3,1),(4,'developer2','2af9b1ba42dc5eb01743e6b3759b6e4b',3,5,4,1),(5,'manager2','2af9b1ba42dc5eb01743e6b3759b6e4b',2,4,5,1);
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
INSERT INTO `user_role` VALUES (1,'admin'),(2,'manager'),(3,'developer');
UNLOCK TABLES;

--
-- Table structure for table `user_skill_level`
--

DROP TABLE IF EXISTS `user_skill_level`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_skill_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_level_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


--
-- Dumping data for table `user_skill_level`
--

LOCK TABLES `user_skill_level` WRITE;

INSERT INTO `user_skill_level` VALUES (1,'superadmin'),(2,'junior'),(3,'middle'),(4,'senior'),(5,'lead');

UNLOCK TABLES;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- Dump completed on 2019-12-23  0:20:34
