package by.epam.training.riverferry;

import by.epam.training.riverferry.controller.FerryController;
import by.epam.training.riverferry.controller.VehicleController;
import by.epam.training.riverferry.model.Ferry;
import by.epam.training.riverferry.model.Vehicle;
import by.epam.training.riverferry.parser.LineParser;
import by.epam.training.riverferry.repository.VehicleRepositoryImpl;
import by.epam.training.riverferry.service.VehicleService;
import by.epam.training.riverferry.validator.DataReader;
import by.epam.training.riverferry.validator.FileValidator;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class Runner {

    private static String carFile = "cars.txt";
    private static String ferryFile  = "ferry.txt";
    private static ClassLoader classLoader = Runner.class.getClassLoader();

    public static void main(String[] args) {
        //preparation
        VehicleRepositoryImpl vehicleRepository = new VehicleRepositoryImpl();
        VehicleService vehicleService = new VehicleService(vehicleRepository);
        FileValidator fileValidator = new FileValidator();
        DataReader  dataReader = new DataReader();
        LineParser lineParser = new LineParser();
        VehicleController vehicleController = new VehicleController(vehicleService, fileValidator, dataReader, lineParser);
        String path = new File(Objects.requireNonNull(classLoader.getResource(carFile)).getFile()).getAbsolutePath();
        vehicleController.saveEntityFromFile(path);
        FerryController ferryController = new FerryController(fileValidator,dataReader, lineParser);
        String pathferry = new File(Objects.requireNonNull(classLoader.getResource(ferryFile)).getFile()).getAbsolutePath();
        Ferry  ferry = ferryController.getFerryFromFile(pathferry);
//Ferry work
        ferry.start();
        List<Vehicle> allVehicles = vehicleService.getAll();
        for (Vehicle vehicle : allVehicles) {
            vehicle.setFerry(ferry);
            vehicle.start();
        }
    }
}

