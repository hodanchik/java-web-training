package by.epam.training.riverferry.controller;

import by.epam.training.riverferry.builder.VehicleBuilderFactory;
import by.epam.training.riverferry.parser.LineParser;
import by.epam.training.riverferry.service.VehicleService;
import by.epam.training.riverferry.validator.DataReader;
import by.epam.training.riverferry.validator.FileValidator;
import by.epam.training.riverferry.validator.ValidatorResult;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class VehicleController {
    private static final Logger log = Logger.getLogger(VehicleController.class);
    private VehicleService vehicleService;
    private FileValidator fileValidator;
    private DataReader dataReader;
    private LineParser lineParser;

    public VehicleController(VehicleService vehicleService, FileValidator fileValidator, DataReader dataReader,
                             LineParser lineParser) {
        this.vehicleService = vehicleService;
        this.fileValidator = fileValidator;
        this.dataReader = dataReader;
        this.lineParser = lineParser;
    }

    public void saveEntityFromFile(String path) {
        ValidatorResult validatorResult = fileValidator.validateFile(path);
        if (validatorResult.isValidate()) {
            List<String> dataLine = dataReader.ReadData(path);
            for (String data : dataLine) {
                Map<String, String> dataMap = lineParser.parseLine(data);
                vehicleService.saveVehicle(new VehicleBuilderFactory()
                        .getBuilderByType(dataMap.get("transportType")).buildVehicle(dataMap));
            }
        } else {
            log.warn("File is not validate");
        }
    }
}
