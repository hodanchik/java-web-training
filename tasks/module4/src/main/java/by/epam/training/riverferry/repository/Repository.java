package by.epam.training.riverferry.repository;

import java.util.List;

public interface Repository<T> {
    void add(T entity);

    void remove(T entity);

    List<T> getAll();
}
