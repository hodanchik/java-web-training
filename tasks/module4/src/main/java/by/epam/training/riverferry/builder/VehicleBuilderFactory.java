package by.epam.training.riverferry.builder;

import by.epam.training.riverferry.model.TransportType;
import org.apache.log4j.Logger;

public class VehicleBuilderFactory {
    private static final Logger log = Logger.getLogger(VehicleBuilderFactory.class);

    public VehicleBuilder getBuilderByType(String type) {
        if (TransportType.fromString(type).isPresent()) {
            TransportType tourType = TransportType.fromString(type).get();
            switch (tourType) {
                case CAR:
                    return new CarBuilder();
                case TRUCK:
                    return new TruckBuilder();
            }
        }
        log.error("Incorrect data in VehicleBuilderFactory");
        return new DefaultVehicleBuilder();
    }
}
