package by.epam.training.riverferry.model;

import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Vehicle extends Thread {
    private static final Logger LOG = Logger.getLogger(Vehicle.class);
    private int weight;
    private int number;
    private Ferry ferry;
    private int square;
    private TransportType transportType;
    private ReentrantLock locker;
    private Condition condition;


    public Vehicle(TransportType transportType, int number, int weight, int square) {
        this.transportType = transportType;
        this.weight = weight;
        this.number = number;
        this.square = square;
        locker = new ReentrantLock();
        condition = locker.newCondition();
    }


    public void tryLoad() throws InterruptedException {
        int result = ferry.load(this);
        switch (result) {
            case 1: //vehicle loaded
                break;
            case 2: //vehicle unloaded
                LOG.info("There is no place for a vehicle № " + number + " on the ferry");
                TimeUnit.SECONDS.sleep(7);
                tryLoad();
                break;
            case 3://vehicle unloaded and ferry finished work
                LOG.info("Vehicle № " + number + " leaves to look for another ferry or will return tomorrow");
                TimeUnit.DAYS.sleep(1);
                break;
        }
    }

    @Override
    public void run() {
        try {
            tryLoad();
            LOG.info("Vehicle № " + number + " continued driving");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public Ferry getFerry() {
        return ferry;
    }

    public void setFerry(Ferry ferry) {
        this.ferry = ferry;
    }
}
