package by.epam.training.riverferry.builder;

import by.epam.training.riverferry.model.Vehicle;

import java.util.Map;

public interface VehicleBuilder {
    Vehicle buildVehicle(Map<String, String> validateMap);
}
