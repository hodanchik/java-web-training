package by.epam.training.riverferry.model;

import java.util.Optional;
import java.util.stream.Stream;

public enum TransportType {
    TRUCK, CAR;

    public static Optional<TransportType> fromString(String type) {
        return Stream.of(TransportType.values())
                .filter(t -> t.name().equalsIgnoreCase(type))
                .findFirst();
    }
}
