package by.epam.training.riverferry.builder;

import by.epam.training.riverferry.model.Car;
import by.epam.training.riverferry.model.TransportType;
import by.epam.training.riverferry.model.Vehicle;

import java.util.Map;

public class CarBuilder implements VehicleBuilder {
    private TransportType transportType;
    private int number;
    private int weight;
    private int square;
    private int seatPlaceCount;

    @Override
    public Vehicle buildVehicle(Map<String, String> validateMap) {
        transportType = TransportType.valueOf(validateMap.get("transportType"));
        number = Integer.parseInt(validateMap.get("number"));
        weight = Integer.parseInt(validateMap.get("weight"));
        square = Integer.parseInt(validateMap.get("square"));
        seatPlaceCount = Integer.parseInt(validateMap.get("seatPlaceCount"));
        return new Car(transportType, number, weight, square, seatPlaceCount);
    }
}
