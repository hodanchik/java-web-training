package by.epam.training.riverferry.builder;
import by.epam.training.riverferry.model.TransportType;
import by.epam.training.riverferry.model.Vehicle;

import java.util.Map;

public class DefaultVehicleBuilder implements VehicleBuilder {
    private TransportType transportType;
    private int weight;
    private int number;
    private int square;


    @Override
    public Vehicle buildVehicle(Map<String, String> validateMap) {
        //this code never will call
        return new Vehicle(transportType, number, weight, square) {
        };
    }
}
