package by.epam.training.riverferry.controller;

import by.epam.training.riverferry.builder.FerryBuilder;
import by.epam.training.riverferry.model.Ferry;
import by.epam.training.riverferry.parser.LineParser;
import by.epam.training.riverferry.validator.DataReader;
import by.epam.training.riverferry.validator.FileValidator;
import by.epam.training.riverferry.validator.ValidatorResult;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

public class FerryController {
    private static final Logger log = Logger.getLogger(FerryController.class);
    private FileValidator fileValidator;
    private DataReader dataReader;
    private LineParser lineParser;

    public FerryController(FileValidator fileValidator, DataReader dataReader,
                           LineParser lineParser) {
        this.fileValidator = fileValidator;
        this.dataReader = dataReader;
        this.lineParser = lineParser;
    }

    public Ferry getFerryFromFile(String path) {
        ValidatorResult validatorResult = fileValidator.validateFile(path);
        Ferry ferry = new Ferry();
        if (validatorResult.isValidate()) {
            List<String> dataLine = dataReader.ReadData(path);
            for (String data : dataLine) {
                Map<String, String> dataMap = lineParser.parseLine(data);
                ferry = new FerryBuilder().buildFerry(dataMap);
            }
        } else {
            log.warn("File is not validate");
        }
        return ferry;
    }
}

