package by.epam.training.riverferry.model;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Ferry extends Thread {
    private static final Logger LOG = Logger.getLogger(Ferry.class);
    private int countRace;
    private int maxWeight;
    private int optimalWeight;
    private int maxSquare;
    private int optimalSquare;
    private AtomicInteger currentSquare;
    private AtomicInteger currentWeight;
    private List<Vehicle> vehicleList ;
    private ReentrantLock locker;
    private Condition condition;

    private int currentRace;
    private AtomicBoolean loaded;
    public Ferry(){

    }
    public Ferry(int countRace, int maxWeight, int optimalWeight, int maxSquare, int optimalSquare) {
        this.countRace = countRace;
        this.maxWeight = maxWeight;
        this.optimalWeight = optimalWeight;
        this.maxSquare = maxSquare;
        this.optimalSquare = optimalSquare;
        locker = new ReentrantLock();
        condition = locker.newCondition();
        currentSquare = new AtomicInteger(0);
        currentWeight = new AtomicInteger(0);
        vehicleList= new ArrayList<>();
        currentRace = 1;
        loaded =  new AtomicBoolean(false);
    }

    public int load(Vehicle vehicle) {
        locker.lock();
        try {
            if (currentRace <= countRace) {
                if (currentWeight.intValue() + vehicle.getWeight() < maxWeight && currentSquare.intValue() + vehicle.getSquare() < maxSquare) {
                    currentWeight.addAndGet(vehicle.getWeight());
                    currentSquare.addAndGet(vehicle.getSquare());
                    vehicleList.add(vehicle);
                    LOG.info("Allowed loading vehicle № " + vehicle.getNumber());
                    LOG.info("Loading weight: " + currentWeight + " Loading square: " + currentSquare);
                    condition.await();
                    return 1;
                } else if ((currentWeight.intValue() + vehicle.getWeight() > maxWeight ||
                        currentSquare.intValue() + vehicle.getSquare() > maxSquare)
                        && (currentWeight.intValue() >= optimalWeight || currentSquare.intValue() >= optimalSquare)) {
                    loaded.set(true);
                    return 2;
                } else {
                    return 2;
                }
            } else {
                return 3;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            return 2;
        } finally {
            locker.unlock();
        }

    }

    public void unloading() {
        locker.lock();
        try {
            LOG.info("Transportation № " + currentRace);
            LOG.info("Transportation...");
            TimeUnit.SECONDS.sleep(2);
            LOG.info("Unloading...");
            TimeUnit.SECONDS.sleep(1);
            for (Vehicle vehicle : vehicleList) {
                LOG.info("vehicle № " + vehicle.getNumber() + " left the ferry");
            }
            currentRace++;
            condition.signalAll();
            vehicleList.clear();
            currentWeight.set(0);
            currentSquare.set(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            locker.unlock();
        }
    }


    @Override
    public void run() {
        LOG.info("Ferry ready for work");
        while (currentRace <= countRace) {
            if (!loaded.get()) {
                try {
                    //ferry is not loaded and waiting for vehicles
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                unloading();
                loaded.set(false);
            }
        }
        LOG.info("Ferry finished work");
    }
}


