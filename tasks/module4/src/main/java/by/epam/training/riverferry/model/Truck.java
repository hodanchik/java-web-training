package by.epam.training.riverferry.model;

public class Truck extends Vehicle {
    private int trailerWeight;

    public Truck(TransportType transportType, int number, int weight, int square, int trailerWeight) {
        super(transportType, number, weight, square);
        this.trailerWeight = trailerWeight;
    }
    public int getTrailerWeight() {
        return trailerWeight;
    }

    public void setTrailerWeight(int trailerWeight) {
        this.trailerWeight = trailerWeight;
    }
}
