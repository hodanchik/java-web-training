package by.epam.training.riverferry.model;

public class Car extends Vehicle {
    private int seatPlaceCount;

    public Car(TransportType transportType, int number, int weight, int square, int seatPlaceCount) {
        super(transportType, number, weight, square);
        this.seatPlaceCount = seatPlaceCount;
    }

    public int getSeatPlaceCount() {
        return seatPlaceCount;
    }

    public void setSeatPlaceCount(int seatPlaceCount) {
        this.seatPlaceCount = seatPlaceCount;
    }
}
