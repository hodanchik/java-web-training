package by.epam.training.riverferry.service;

import by.epam.training.riverferry.model.Vehicle;
import by.epam.training.riverferry.repository.VehicleRepositoryImpl;
import org.apache.log4j.Logger;

import java.util.List;


public class VehicleService {

    private static final Logger log = Logger.getLogger(VehicleService.class);
    private VehicleRepositoryImpl vehicleRepositoryImpl;

    public VehicleService(VehicleRepositoryImpl vehicleRepositoryImpl) {
        this.vehicleRepositoryImpl = vehicleRepositoryImpl;
    }

    public void saveVehicle(Vehicle vehicle) {
        vehicleRepositoryImpl.add(vehicle);
    }

    public List<Vehicle> getAll() {
        return vehicleRepositoryImpl.getAll();
    }
    public VehicleRepositoryImpl getVehicleRepositoryImpl() {
        return vehicleRepositoryImpl;
    }
}