package by.epam.training.riverferry.builder;

import by.epam.training.riverferry.model.Ferry;

import java.util.Map;

public class FerryBuilder {

    private int countRace;
    private int maxWeight;
    private int optimalWeight;
    private int maxSquare;
    private int optimalSquare;

    public Ferry buildFerry(Map<String, String> validateMap) {
        countRace = Integer.parseInt(validateMap.get("countRace"));
        maxWeight = Integer.parseInt(validateMap.get("maxWeight"));
        optimalWeight = Integer.parseInt(validateMap.get("optimalWeight"));
        maxSquare = Integer.parseInt(validateMap.get("maxSquare"));
        optimalSquare = Integer.parseInt(validateMap.get("optimalSquare"));
        return new Ferry(countRace, maxWeight, optimalWeight, maxSquare, optimalSquare);
    }
}
