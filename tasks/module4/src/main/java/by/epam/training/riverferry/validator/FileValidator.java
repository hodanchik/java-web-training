package by.epam.training.riverferry.validator;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {
    private static final Logger LOG = Logger.getLogger(FileValidator.class);
    public ValidatorResult validateFile(String pathString) {
        ValidatorResult validatorResult = new ValidatorResult();
        if (pathString == null) {
            validatorResult.addResult("File is no valid", "Null path");
            LOG.error("Null path");
            return validatorResult;
        }
        File file = new File(pathString);
        Path path = Paths.get(pathString);
        if (!file.isFile()) {
            validatorResult.addResult("File is no valid", "Nonexistent path to file");
            LOG.error("Nonexistent path to file");
            return validatorResult;
        }
        if (!Files.isReadable(path)) {
            validatorResult.addResult("File is no valid", "File can't be read");
            LOG.error("File can't be read");
            return validatorResult;
        }
        try {
            FileChannel textFileChannel = FileChannel.open(path);
            if (textFileChannel.size() == 0) {
                validatorResult.addResult("File is no valid", "Empty file");
                LOG.warn("Empty file");

            }
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("Incorrect file format");
        }
        return validatorResult;
    }
}

