package by.epam.training.riverferry.repository;

import by.epam.training.riverferry.model.Vehicle;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class VehicleRepositoryImpl implements Repository<Vehicle> {
    private static final Logger log = Logger.getLogger(VehicleRepositoryImpl.class);

    private List<Vehicle> vehicleList = new ArrayList<>();

    public VehicleRepositoryImpl() {
    }

    public VehicleRepositoryImpl(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    @Override
    public void add(Vehicle entity) {
        log.debug("add vehicle to repository");
        vehicleList.add(entity);
    }

    @Override
    public void remove(Vehicle entity) {
        log.debug("remove vehicle from repository");
        vehicleList.remove(entity);
    }

    @Override
    public List<Vehicle> getAll() {
        return new ArrayList<>(vehicleList);
    }
}

