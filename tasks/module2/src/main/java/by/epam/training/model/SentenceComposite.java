package by.epam.training.model;

import java.util.LinkedList;
import java.util.List;

public class SentenceComposite implements TextComposite {
    private List<TextLeaf> sentence = new LinkedList<>();
    private boolean newLine;
    private String separator = " ";
    private long id;

    public boolean isNewLine() {
        return newLine;
    }

    public void setNewLine(boolean newLine) {
        this.newLine = newLine;
    }
    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
    @Override
    public void addText(TextLeaf textLeaf) {
        sentence.add(textLeaf);
    }
    @Override
    public List<TextLeaf> getComponent() {
        return sentence;
    }
    @Override
    public String getText() {
        StringBuilder sb = new StringBuilder();
        for (TextLeaf textLeaf : sentence) {
            sb.append(textLeaf.getText());
            sb.append(separator);
        }
        if (isNewLine()) {
            sb.replace(sb.lastIndexOf(separator), sb.lastIndexOf(separator) + 1, "\n");
        }
        return String.valueOf(sb);
    }
}
