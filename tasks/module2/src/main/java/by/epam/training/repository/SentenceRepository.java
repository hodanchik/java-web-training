package by.epam.training.repository;

import by.epam.training.model.TextComposite;

import java.util.concurrent.atomic.AtomicLong;

public class SentenceRepository extends TextRepositoryImpl<TextComposite> {
    private AtomicLong id = new AtomicLong(1);

    @Override
    public long add(TextComposite entity) {
        this.text.put(id.get(), entity);
        return id.getAndIncrement();
    }
}
