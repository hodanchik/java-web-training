package by.epam.training.model;

import java.util.LinkedList;
import java.util.List;

public class EntireTextComposite implements TextComposite {
    private List<TextLeaf> text = new LinkedList<>();
    private long id;

    @Override
    public void addText(TextLeaf textLeaf) {
        text.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getComponent() {
        return text;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getText() {
        StringBuilder sb = new StringBuilder();
        for (TextLeaf textLeaf : text) {
            sb.append(textLeaf.getText());
        }
        return String.valueOf(sb);
    }
}
