package by.epam.training.service;

import by.epam.training.model.ParagraphСomposite;
import by.epam.training.repository.TextRepositoryImpl;

public class ParagraphService extends TextService<ParagraphСomposite> {
    public ParagraphService(TextRepositoryImpl textRepository) {
        super(textRepository);
    }
}
