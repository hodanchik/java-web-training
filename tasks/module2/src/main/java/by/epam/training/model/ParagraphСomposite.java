package by.epam.training.model;

import java.util.LinkedList;
import java.util.List;

public class ParagraphСomposite implements TextComposite {
    private List<TextLeaf> paragraph = new LinkedList<>();
    private long id;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
    @Override
    public void addText(TextLeaf textLeaf) {
        paragraph.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getComponent() {
        return paragraph;
    }
    @Override
    public String getText() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t");
        for (TextLeaf textLeaf : paragraph) {
            sb.append(textLeaf.getText());
        }
        return String.valueOf(sb);
    }
}
