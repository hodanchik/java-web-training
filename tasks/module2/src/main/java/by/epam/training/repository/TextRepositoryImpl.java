package by.epam.training.repository;

import by.epam.training.model.TextLeaf;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class TextRepositoryImpl<T extends TextLeaf> implements Repository<T> {
    protected Map<Long, T> text;

    public TextRepositoryImpl() {
        text = new HashMap<>();
    }

    @Override
    public boolean remove(long id) {
        if (!text.containsKey(id)) {
            return false;
        } else {
            text.remove(id);
        }
        return true;
    }

    @Override
    public boolean update(T entity) {
        if (!text.containsValue(entity)) {
            return false;
        } else {
            text.put(entity.getId(), entity);
        }
        return true;
    }

    @Override
    public T read(long id) {
        return text.get(id);
    }

    @Override
    public List<T> getAll() {
        return new LinkedList<T>(text.values());
    }

}
