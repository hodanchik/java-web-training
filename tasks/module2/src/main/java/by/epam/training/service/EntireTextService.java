package by.epam.training.service;

import by.epam.training.model.EntireTextComposite;
import by.epam.training.repository.TextRepositoryImpl;

public class EntireTextService extends TextService<EntireTextComposite> {
    public EntireTextService(TextRepositoryImpl textRepository) {
        super(textRepository);
    }

}
