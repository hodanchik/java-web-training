package by.epam.training.service;

import by.epam.training.model.WordLeaf;
import by.epam.training.repository.TextRepositoryImpl;

public class WordService extends TextService<WordLeaf> {
    public WordService(TextRepositoryImpl textRepository) {
        super(textRepository);
    }
}
