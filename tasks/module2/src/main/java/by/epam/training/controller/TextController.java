package by.epam.training.controller;

import by.epam.training.model.TextLeaf;
import by.epam.training.parser.ParserChain;
import by.epam.training.service.TextService;
import by.epam.training.validator.FileReader;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.ValidatorResult;
import org.apache.log4j.Logger;

public class TextController {
    private static final Logger log = Logger.getLogger(TextController.class);
    private FileValidator fileValidator;
    private FileReader fileReader;
    private ParserChain<TextLeaf> textParser;
    private TextService textService;

    public TextController(FileValidator fileValidator, FileReader fileReader, ParserChain textParser, TextService textService) {
        this.fileValidator = fileValidator;
        this.fileReader = fileReader;
        this.textParser = textParser;
        this.textService = textService;
    }

    public void saveTextFromFile(String path) {
        ValidatorResult validatorResult = fileValidator.validateFile(path);
        if (validatorResult.isValidate()) {
            String text = fileReader.readData(path);
            TextLeaf parseText = textParser.parse(text);
            textService.saveText(parseText);
        } else {
            log.warn("File is not validate");
        }
    }


}
