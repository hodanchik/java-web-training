package by.epam.training.service;

import by.epam.training.model.TextLeaf;
import by.epam.training.repository.TextRepositoryImpl;
import org.apache.log4j.Logger;

import java.util.List;

public class TextService<T extends TextLeaf> {
    private static final Logger log = Logger.getLogger(TextService.class);
    protected TextRepositoryImpl textRepository;

    public TextService(TextRepositoryImpl textRepository) {
        this.textRepository = textRepository;
    }

    public void saveText(T entity) {
        long id = textRepository.add(entity);
        entity.setId(id);
    }

    public List<T> getAllText() {
        return textRepository.getAll();
    }

    public boolean remove(long id) {
        return textRepository.remove(id);
    }

    public boolean update(T entity) {
        return textRepository.update(entity);
    }

    public T read(long id) {
        return (T) textRepository.read(id);
    }
}
