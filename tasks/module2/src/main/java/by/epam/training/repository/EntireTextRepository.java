package by.epam.training.repository;

import by.epam.training.model.EntireTextComposite;

import java.util.concurrent.atomic.AtomicLong;

public class EntireTextRepository extends TextRepositoryImpl<EntireTextComposite> {

    private AtomicLong id = new AtomicLong(1);

    @Override
    public long add(EntireTextComposite entity) {
        this.text.put(id.get(), entity);
        return id.getAndIncrement();
    }
}
