package by.epam.training.repository;

public interface Specification <T> {
    boolean match(T entity);
}
