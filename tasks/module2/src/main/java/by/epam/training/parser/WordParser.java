package by.epam.training.parser;

import by.epam.training.model.SentenceComposite;
import by.epam.training.model.TextLeaf;
import by.epam.training.model.WordLeaf;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser extends TextParser {
    private String regexWord = "([\\'\\(\\\"]+)?(\\w+([\\-\\']*\\w*)*)+([\\.\\!\\?\\\"\\)\\'\\,]+)?";
    private Pattern pattern = Pattern.compile(regexWord);
    private Matcher matcher;

    @Override
    public TextLeaf parse(String text) {
        matcher = pattern.matcher(text);
        SentenceComposite sentenceComposite = new SentenceComposite();
        sentenceComposite.setNewLine(parseNewLine(text));
        while (matcher.find()) {
            String start = matcher.group(1);
            String word = matcher.group(2);
            String ending = matcher.group(4);

            if (start == null) {
                start = "";
            }
            if (ending == null) {
                ending = "";
            }

            TextLeaf wordLeaf = new WordLeaf(start, word, ending);
            sentenceComposite.addText(wordLeaf);
        }
        return sentenceComposite;
    }

    private boolean parseNewLine(String sentence) {
        String regexNewLine = "\n";
        return sentence.endsWith(regexNewLine);
    }
}
