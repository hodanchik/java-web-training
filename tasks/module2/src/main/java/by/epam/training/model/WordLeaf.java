package by.epam.training.model;

import java.util.ArrayList;
import java.util.List;

public class WordLeaf implements TextLeaf {
    private String start;
    private String word;
    private String ending;
    private long id;



    public WordLeaf(String start, String word, String ending) {
        this.start = start;
        this.word = word;
        this.ending = ending;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }
    @Override
    public String getText() {
        StringBuilder sb = new StringBuilder();
        sb.append(start).append(word).append(ending);
        return String.valueOf(sb);
    }

    @Override
    public List<TextLeaf> getComponent() {
        return  new ArrayList<TextLeaf>();
    }
}
