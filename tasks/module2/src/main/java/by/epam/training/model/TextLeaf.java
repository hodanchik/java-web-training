package by.epam.training.model;

import java.util.List;

public interface TextLeaf {
    String getText();
    List<TextLeaf> getComponent();
    long getId();
    void setId(long id);
}
