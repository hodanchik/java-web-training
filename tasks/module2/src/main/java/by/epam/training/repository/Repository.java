package by.epam.training.repository;

import java.util.List;

public interface Repository<T> {
    long add(T entity);

    boolean remove(long id);

    boolean update(T entity);

    T read(long id);

    List<T> getAll();
}
