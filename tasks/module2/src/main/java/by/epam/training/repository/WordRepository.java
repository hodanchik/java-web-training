package by.epam.training.repository;

import by.epam.training.model.TextLeaf;

import java.util.concurrent.atomic.AtomicLong;

public class WordRepository extends TextRepositoryImpl<TextLeaf> {
    private AtomicLong id = new AtomicLong(1);

    @Override
    public long add(TextLeaf entity) {
        this.text.put(id.get(), entity);
        return id.getAndIncrement();
    }
}
