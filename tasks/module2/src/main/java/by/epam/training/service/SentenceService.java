package by.epam.training.service;

import by.epam.training.model.SentenceComposite;
import by.epam.training.model.TextLeaf;
import by.epam.training.repository.TextRepositoryImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SentenceService extends TextService<SentenceComposite> {

    public SentenceService(TextRepositoryImpl textRepository) {
        super(textRepository);
    }

    public SentenceComposite sortWordInSentenceIncrease(SentenceComposite sentence) {
        List<TextLeaf> componentList = sentence.getComponent();
        List<TextLeaf> wordList = new ArrayList<>(componentList);
        wordList.sort(Comparator.comparingInt(o -> o.getText().length()));
        SentenceComposite sentenceComposite = new SentenceComposite();
        for (TextLeaf word : wordList) {
            sentenceComposite.addText(word);
        }
        return sentenceComposite;
    }
    public SentenceComposite sortWordInSentenceDescend(SentenceComposite sentence) {
        List<TextLeaf> componentList = sentence.getComponent();
        List<TextLeaf> wordList = new ArrayList<>(componentList);
        wordList.sort(Comparator.comparingInt(o -> o.getText().length()));
        Collections.reverse(wordList);
        SentenceComposite sentenceComposite = new SentenceComposite();
        for (TextLeaf word : wordList) {
            sentenceComposite.addText(word);
        }
        return sentenceComposite;
    }
}

