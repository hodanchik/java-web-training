package by.epam.training.controller;

import by.epam.training.model.TextLeaf;
import by.epam.training.parser.ParagraphParser;
import by.epam.training.parser.ParserChain;
import by.epam.training.parser.SentenceParser;
import by.epam.training.parser.WordParser;
import by.epam.training.repository.EntireTextRepository;
import by.epam.training.repository.SentenceRepository;
import by.epam.training.repository.TextRepositoryImpl;
import by.epam.training.repository.WordRepository;
import by.epam.training.service.TextService;
import by.epam.training.validator.FileReader;
import by.epam.training.validator.FileValidator;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Objects;

public class TextControllerTest {
    private static ParserChain<TextLeaf> textParser;
    private static FileValidator fileValidator;
    private static FileReader fileReader;
    private static TextService textService;
    private static TextRepositoryImpl textRepository;
    private static TextController textController;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Test
    public void saveOneEntireTextFromFile() {
        int expectedCount = 1;
        fileValidator = new FileValidator();
        fileReader = new FileReader();
        textParser = new ParagraphParser();
        textRepository = new EntireTextRepository();
        textService = new TextService(textRepository);
        textParser.linkWith(new SentenceParser()).linkWith(new WordParser());
        textController = new TextController(fileValidator, fileReader, textParser, textService);
        String path = new File(Objects.requireNonNull(classLoader.getResource("textExample.txt"))
                .getFile()).getAbsolutePath();
        textController.saveTextFromFile(path);
        int textCountActual = textService.getAllText().size();
        Assert.assertEquals(textCountActual, expectedCount);
    }

    @Test
    public void saveSixSentenceFromFile() {
        int expectedCount = 6;
        fileValidator = new FileValidator();
        fileReader = new FileReader();
        textParser = new SentenceParser();
        textRepository = new SentenceRepository();
        textService = new TextService(textRepository);
        textParser.linkWith(new WordParser());
        textController = new TextController(fileValidator, fileReader, textParser, textService);
        String path = new File(Objects.requireNonNull(classLoader.getResource("textExample.txt"))
                .getFile()).getAbsolutePath();
        textController.saveTextFromFile(path);
        int actualSize = textService.read(1).getComponent().size();
        Assert.assertEquals(expectedCount, actualSize);
    }

    @Test
    public void saveWordsFromFile() {
        int expectedCount = 119;
        fileValidator = new FileValidator();
        fileReader = new FileReader();
        textParser = new WordParser();
        textRepository = new WordRepository();
        textService = new TextService(textRepository);
        textController = new TextController(fileValidator, fileReader, textParser, textService);
        String path = new File(Objects.requireNonNull(classLoader.getResource("textExample.txt"))
                .getFile()).getAbsolutePath();
        textController.saveTextFromFile(path);
        int actualSize = textService.read(1).getComponent().size();
        Assert.assertEquals(expectedCount, actualSize);
    }
}