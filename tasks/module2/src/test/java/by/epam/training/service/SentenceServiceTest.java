package by.epam.training.service;

import by.epam.training.model.SentenceComposite;
import by.epam.training.model.TextLeaf;
import by.epam.training.model.WordLeaf;
import by.epam.training.repository.SentenceRepository;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class SentenceServiceTest {
    private static SentenceComposite sentenceOne = new SentenceComposite();
    private static SentenceComposite sentenceTwo = new SentenceComposite();

    @BeforeClass
    public static void init() {
        sentenceOne.addText(new WordLeaf("", "It's", "...."));
        sentenceOne.addText(new WordLeaf("", "unsorted", ""));
        sentenceOne.addText(new WordLeaf("", "list", ","));
        sentenceOne.addText(new WordLeaf("", "SORTED", "!!!!!!"));
        sentenceOne.addText(new WordLeaf("", "this", ""));
        sentenceOne.addText(new WordLeaf("", "list", ":)"));
        sentenceTwo.addText(new WordLeaf("", "It's", ""));
        sentenceTwo.addText(new WordLeaf("", "sentence", ""));
        sentenceTwo.addText(new WordLeaf("", "for", ""));
        sentenceTwo.addText(new WordLeaf("", "test", ""));
        sentenceTwo.addText(new WordLeaf("", "service", ""));
        sentenceTwo.addText(new WordLeaf("", "layer", ":)"));
    }

    @Test
    public void saveText() {
        SentenceRepository sentenceRepo = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepo);
        sentenceService.saveText(sentenceOne);
        sentenceService.saveText(sentenceTwo);
        int expectSize = 2;
        int actualSize = sentenceService.getAllText().size();
        Assert.assertEquals(expectSize, actualSize);
    }

    @Test
    public void read() {
        SentenceRepository sentenceRepo = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepo);
        sentenceService.saveText(sentenceOne);
        sentenceService.saveText(sentenceTwo);
        TextLeaf readText = sentenceService.read(2);
        Assert.assertEquals(sentenceTwo, readText);
    }

    @Test
    public void update() {
        SentenceRepository sentenceRepo = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepo);
        sentenceService.saveText(sentenceOne);
        sentenceService.saveText(sentenceTwo);
        sentenceTwo.addText(new WordLeaf("UPDATE", "SENTENCE", ":)"));
        sentenceService.update(sentenceTwo);
        TextLeaf expectSentence = sentenceTwo;
        TextLeaf actualSentence = sentenceService.read(2);
        Assert.assertEquals(expectSentence, actualSentence);
    }

    @Test
    public void remove() {
        SentenceRepository sentenceRepo = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepo);
        sentenceService.saveText(sentenceOne);
        sentenceService.saveText(sentenceTwo);
        sentenceService.remove(1);
        int expectSize = 1;
        int actualSize = sentenceService.getAllText().size();
        Assert.assertEquals(expectSize, actualSize);
    }

    @Test
    public void sortWordInSentenceIncrease() {
        SentenceRepository sentenceRepo = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepo);
        sentenceService.saveText(sentenceOne);
        SentenceComposite readText = sentenceService.read(1);
        SentenceComposite sortSentenceComposite = sentenceService.sortWordInSentenceIncrease(readText);
        String expectedFirstWord = new WordLeaf("", "this", "").getText();
        String actualFirstWord = sortSentenceComposite.getComponent().get(0).getText();
        Assert.assertEquals(expectedFirstWord, actualFirstWord);
    }

    @Test
    public void sortWordInSentenceDescend() {
        SentenceRepository sentenceRepo = new SentenceRepository();
        SentenceService sentenceService = new SentenceService(sentenceRepo);
        sentenceService.saveText(sentenceOne);
        SentenceComposite readText = sentenceService.read(1);
        SentenceComposite sortSentenceComposite = sentenceService.sortWordInSentenceDescend(readText);
        String expectedFirstWord = new WordLeaf("", "SORTED", "!!!!!!").getText();
        String actualFirstWord = sortSentenceComposite.getComponent().get(0).getText();
        Assert.assertEquals(expectedFirstWord, actualFirstWord);
    }

}
