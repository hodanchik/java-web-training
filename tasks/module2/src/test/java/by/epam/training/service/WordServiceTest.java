package by.epam.training.service;

import by.epam.training.model.TextLeaf;
import by.epam.training.model.WordLeaf;
import by.epam.training.repository.TextRepositoryImpl;
import by.epam.training.repository.WordRepository;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class WordServiceTest {
    TextRepositoryImpl textRepo = new WordRepository();
    TextService textService = new WordService(textRepo);

    @Test
    public void saveText() {
        TextLeaf text = new WordLeaf("", "Example", "");
        TextLeaf textTwo = new WordLeaf("", "Example TWO", ".");
        textService.saveText(text);
        textService.saveText(textTwo);
        int expect = 2;
        int realSize = textService.getAllText().size();
        Assert.assertEquals(expect, realSize);
    }

    @Test
    public void getText() {
        TextLeaf textOne = new WordLeaf("", "Example", "");
        TextLeaf textTwo = new WordLeaf("", "Example TWO", ".");
        textService.saveText(textOne);
        textService.saveText(textTwo);
        List<TextLeaf> allText = textService.getAllText();
        TextLeaf textLeafActual = allText.get(1);
        Assert.assertEquals(textTwo, textLeafActual);
    }


}