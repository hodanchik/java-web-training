package by.epam.training.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T> {
    long add(T entity);
    boolean remove(T entity);
    boolean update(T entity);
    Optional<T> getById(long id);
    List<T> getAll();
}
