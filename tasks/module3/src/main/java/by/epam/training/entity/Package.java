package by.epam.training.entity;

import java.util.Objects;

public class Package {
    private PackageType packageType;
    private int count;
    private double price;
    private String measure;

    public Package(PackageType packageType, int count, double price, String measure) {
        this.packageType = packageType;
        this.count = count;
        this.price = price;
        this.measure = measure;
    }

    public Package() {
    }

    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Package)) return false;
        Package aPackage = (Package) o;
        return getCount() == aPackage.getCount() &&
                Double.compare(aPackage.getPrice(), getPrice()) == 0 &&
                getPackageType() == aPackage.getPackageType() &&
                getMeasure().equals(aPackage.getMeasure());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPackageType(), getCount(), getPrice(), getMeasure());
    }
}
