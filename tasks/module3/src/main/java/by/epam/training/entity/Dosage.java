package by.epam.training.entity;

import java.util.Objects;

public class Dosage {
    private double dose;
    private String measure;
    private String  reception;

    public Dosage(double dose, String measure, String reception) {
        this.dose = dose;
        this.measure = measure;
        this.reception = reception;
    }

    public Dosage() {
    }

    public double getDose() {
        return dose;
    }

    public void setDose(double dose) {
        this.dose = dose;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getReception() {
        return reception;
    }

    public void setReception(String reception) {
        this.reception = reception;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dosage)) return false;
        Dosage dosage = (Dosage) o;
        return Double.compare(dosage.getDose(), getDose()) == 0 &&
                getMeasure().equals(dosage.getMeasure()) &&
                getReception().equals(dosage.getReception());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDose(), getMeasure(), getReception());
    }
}
