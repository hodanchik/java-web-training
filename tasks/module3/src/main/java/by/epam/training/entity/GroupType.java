package by.epam.training.entity;

public enum GroupType {
    NSAID, ANTIBIOTIC, VITAMINS, MUCOLYTIC
}
