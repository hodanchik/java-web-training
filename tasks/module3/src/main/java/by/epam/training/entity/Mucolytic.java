package by.epam.training.entity;

import java.util.List;

public class Mucolytic extends Medicine {
    private boolean compatibleWithAlcohol;

    public Mucolytic(boolean compatibleWithAlcohol) {
        this.compatibleWithAlcohol = compatibleWithAlcohol;
    }

    public Mucolytic(long id, FormType formType, GroupType groupType, String firmName, String internationalName,
                     String pharmCompany, List<String> analog, Certificate certificate,
                     Package packageForm, Dosage dosage, boolean compatibleWithAlcohol) {
        super(id, formType, groupType, firmName, internationalName, pharmCompany, analog, certificate, packageForm, dosage);
        this.compatibleWithAlcohol = compatibleWithAlcohol;
    }

    public boolean isCompatibleWithAlcohol() {
        return compatibleWithAlcohol;
    }

    public void setCompatibleWithAlcohol(boolean compatibleWithAlcohol) {
        this.compatibleWithAlcohol = compatibleWithAlcohol;
    }
}
