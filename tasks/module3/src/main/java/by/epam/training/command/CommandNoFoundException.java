package by.epam.training.command;


public class CommandNoFoundException extends CommandException {
    public CommandNoFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
