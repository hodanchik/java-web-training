package by.epam.training.entity;

import java.util.List;

public class Antibiotic extends Medicine {
    private boolean broadAction;

    public Antibiotic(boolean broadAction) {
        this.broadAction = broadAction;
    }

    public Antibiotic(long id, FormType formType, GroupType groupType, String firmName, String internationalName,
                      String pharmCompany, List<String> analog, Certificate certificate,
                      Package packageForm, Dosage dosage, boolean broadAction) {
        super(id, formType, groupType, firmName, internationalName, pharmCompany, analog, certificate, packageForm, dosage);
        this.broadAction = broadAction;
    }

    public boolean isBroadAction() {
        return broadAction;
    }

    public void setBroadAction(boolean broadAction) {
        this.broadAction = broadAction;
    }
}
