package by.epam.training.validator;

import org.apache.log4j.Logger;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;


public class XmlValidator {

    private static final Logger LOG = Logger.getLogger(XmlValidator.class);
    private String xsdPath;
    public XmlValidator(String xsdPath) {
        this.xsdPath = xsdPath;
    }

    public ValidatorResult validateXmlByXsd(String xmlPath) {
        ValidatorResult validatorResult = new ValidatorResult();
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlPath));
        } catch (Exception e) {
            validatorResult.addResult("XML Validation exception",
                    "XML file " + xmlPath + " is no valid by xsd: " + xsdPath);
            LOG.error("File is no valid " + e.getMessage());
        }
        return validatorResult;
    }
}
