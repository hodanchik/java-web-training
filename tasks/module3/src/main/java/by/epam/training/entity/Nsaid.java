package by.epam.training.entity;

import java.util.List;
import java.util.Objects;

public class Nsaid extends Medicine {
   private boolean gastroprotection;

    public Nsaid(boolean gastroprotection) {
        this.gastroprotection = gastroprotection;
    }

    public Nsaid(long id, FormType formType, GroupType groupType, String firmName, String internationalName, String pharmCompany, List<String> analog, Certificate certificate, Package packageForm, Dosage dosage, boolean gastroprotection) {
        super(id, formType, groupType, firmName, internationalName, pharmCompany, analog, certificate, packageForm, dosage);
        this.gastroprotection = gastroprotection;
    }

    public boolean isGastroprotection() {
        return gastroprotection;
    }

    public void setGastroprotection(boolean gastroprotection) {
        this.gastroprotection = gastroprotection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Nsaid)) return false;
        Nsaid nsaid = (Nsaid) o;
        return getId() == nsaid.getId() &&
                getFormType() == nsaid.getFormType() &&
                getGroupType() == nsaid.getGroupType() &&
                getFirmName().equals(nsaid.getFirmName()) &&
                getInternationalName().equals(nsaid.getInternationalName()) &&
                getPharmCompany().equals(nsaid.getPharmCompany()) &&
                getAnalog().equals(nsaid.getAnalog()) &&
                getCertificate().equals(nsaid.getCertificate()) &&
                getPackageForm().equals(nsaid.getPackageForm()) &&
                getDosage().equals(nsaid.getDosage())&&
                isGastroprotection() == nsaid.isGastroprotection();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFormType(), getGroupType(), getFirmName(), getInternationalName(),
                getPharmCompany(), getAnalog(), getCertificate(), getPackageForm(), getDosage(),isGastroprotection());
    }
}
