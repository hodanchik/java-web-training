package by.epam.training.parser;

import by.epam.training.entity.Package;
import by.epam.training.entity.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DOMParser implements Parser<Medicine> {
    static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private static final Logger LOG = Logger.getLogger(DOMParser.class);
    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

    public List<Medicine> parse(String path) throws ParserException {
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new ParserException(e);
        }
        Document document;
        try {
            document = builder.parse(path);
        } catch (SAXException | IOException e) {
            throw new ParserException(e);
        }
        List<Medicine> medicineList = new ArrayList<>();
        NodeList nodeList = document.getDocumentElement().getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node instanceof Element) {
                Medicine medicine;
                try {
                    switch (node.getNodeName()) {
                        case "nsaid":
                            boolean gastroprotection = Boolean.parseBoolean(node.getAttributes()
                                    .getNamedItem("gastroprotection").getNodeValue());
                            medicine = new Nsaid(gastroprotection);
                            break;

                        case "antibiotic":
                            boolean broadAction = Boolean.parseBoolean(node.getAttributes()
                                    .getNamedItem("broadAction").getNodeValue());
                            medicine = new Antibiotic(broadAction);
                            ;
                            break;
                        case "mucolytic":
                            boolean compatibleWithAlcohol = Boolean.parseBoolean(node.getAttributes()
                                    .getNamedItem("compatibleWithAlcohol").getNodeValue());
                            medicine = new Mucolytic(compatibleWithAlcohol);
                            break;
                        case "vitamins":
                            boolean allowedPregnant = Boolean.parseBoolean(node.getAttributes()
                                    .getNamedItem("allowedPregnant").getNodeValue());
                            medicine = new Vitamins(allowedPregnant);
                            break;

                        default:
                            throw new ParserException("Incorrect node name");
                    }
                } catch (IllegalArgumentException e) {
                    throw new ParserException(e);
                }
                GroupType groupType = GroupType.valueOf(node.getAttributes().getNamedItem("group").getNodeValue().toUpperCase());
                medicine.setGroupType(groupType);
                long id = Long.parseLong(node.getAttributes().getNamedItem("id").getNodeValue());
                medicine.setId(id);
                FormType formType = FormType.valueOf(node.getAttributes().getNamedItem("form").getNodeValue().toUpperCase());
                medicine.setFormType(formType);
                NodeList childNodes = node.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node cNode = childNodes.item(j);
                    if (cNode instanceof Element) {
                        String content = cNode.getLastChild().getTextContent().trim();
                        switch (cNode.getNodeName()) {
                            case "firm_name":
                                medicine.setFirmName(content);
                                break;
                            case "international_name":
                                medicine.setInternationalName(content);
                                break;
                            case "pharm":
                                medicine.setPharmCompany(content);
                                break;
                            case "analogs":
                                List<String> analog = new ArrayList<>();
                                NodeList analogNodes = cNode.getChildNodes();
                                for (int a = 0; a < analogNodes.getLength(); a++) {
                                    Node aNode = analogNodes.item(a);
                                    if (aNode instanceof Element) {
                                        String analogContent = aNode.getLastChild().getTextContent().trim();
                                        analog.add(analogContent);
                                    }
                                }
                                medicine.setAnalog(analog);
                                break;
                            case "certificate":
                                Certificate certificate = new Certificate();
                                NodeList certificateNodes = cNode.getChildNodes();
                                for (int c = 0; c < certificateNodes.getLength(); c++) {
                                    Node certNode = certificateNodes.item(c);
                                    if (certNode instanceof Element) {
                                        String certContent = certNode.getLastChild().getTextContent().trim();
                                        switch (certNode.getNodeName()) {
                                            case "number":
                                                certificate.setNumber(Long.parseLong(certContent));
                                                break;
                                            case "date_of_issue":
                                                try {
                                                    Date parsingDate = ft.parse(certContent);
                                                    certificate.setIssueDate(parsingDate);
                                                } catch (ParseException e) {
                                                    LOG.error("Can't parse date_of_issue", e);
                                                    throw new ParserException("Can't parse date_of_issue", e);
                                                }
                                                break;
                                            case "expiration_date":
                                                try {
                                                    Date parsingDate = ft.parse(certContent);
                                                    certificate.setExpirationDate(parsingDate);
                                                } catch (ParseException e) {
                                                    LOG.error("Can't parse expiration_date", e);
                                                    throw new ParserException("Can't parse expiration_date", e);
                                                }
                                                break;
                                            case "registry_organisation":
                                                certificate.setRegistryOrganisation(certContent);
                                                break;
                                            default:
                                                throw new ParserException("Incorrect field name in entity certificate");
                                        }
                                    }
                                }
                                medicine.setCertificate(certificate);
                                break;

                            case "package":
                                Package packageMed = new Package();
                                packageMed.setPackageType(PackageType.valueOf(cNode.getAttributes().getNamedItem("type")
                                        .getNodeValue().toUpperCase()));
                                NodeList packageNodes = cNode.getChildNodes();
                                for (int p = 0; p < packageNodes.getLength(); p++) {
                                    Node packageNode = packageNodes.item(p);
                                    if (packageNode instanceof Element) {
                                        String packageContent = packageNode.getLastChild().getTextContent().trim();
                                        switch (packageNode.getNodeName()) {
                                            case "count":
                                                packageMed.setCount(Integer.parseInt(packageContent));
                                                break;
                                            case "price":
                                                packageMed.setPrice(Double.parseDouble(packageContent));
                                                break;
                                            case "currency":
                                                packageMed.setMeasure(packageContent);
                                                break;
                                            default:
                                                throw new ParserException("Incorrect field name in entity package");
                                        }
                                    }
                                }
                                medicine.setPackageForm(packageMed);
                                break;
                            case "dosage":
                                Dosage dosage = new Dosage();
                                dosage.setDose(Double.parseDouble(cNode.getAttributes().getNamedItem("dose").getNodeValue()));
                                dosage.setMeasure(cNode.getAttributes().getNamedItem("measure").getNodeValue());
                                NodeList dosageNodes = cNode.getChildNodes();
                                for (int p = 0; p < dosageNodes.getLength(); p++) {
                                    Node dosageNode = dosageNodes.item(p);
                                    if (dosageNode instanceof Element) {
                                        String dosageContent = dosageNode.getLastChild().getTextContent().trim();
                                        if ("frequency_of_reception".equals(dosageNode.getNodeName())) {
                                            dosage.setReception(dosageContent);
                                        } else {
                                            throw new ParserException("Incorrect field name in entity dosage");
                                        }
                                    }
                                }
                                medicine.setDosage(dosage);
                                break;
                            default:
                                throw new ParserException("Incorrect field name in entity medicine");
                        }
                    }
                }
                medicineList.add(medicine);
            }
        }
        return medicineList;
    }
}

