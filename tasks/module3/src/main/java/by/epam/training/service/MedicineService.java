package by.epam.training.service;

import by.epam.training.entity.Medicine;
import by.epam.training.repository.MedicineRepository;

import java.util.List;
import java.util.Optional;

public class MedicineService implements Service<Medicine> {
    private MedicineRepository medicineRepository;

    public MedicineService(MedicineRepository medicineRepository) {
        this.medicineRepository = medicineRepository;
    }

    @Override
    public long add(Medicine entity) {
        return medicineRepository.add(entity);
    }

    @Override
    public boolean remove(Medicine entity) {
        return medicineRepository.remove(entity);
    }

    @Override
    public Optional<Medicine> getById(long id) {
        return medicineRepository.getById(id);
    }

    @Override
    public List<Medicine> getAll() {
        return medicineRepository.getAll();
    }
}
