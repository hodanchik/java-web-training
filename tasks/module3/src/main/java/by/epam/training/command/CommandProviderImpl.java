package by.epam.training.command;

import java.util.HashMap;
import java.util.Map;

public class CommandProviderImpl<T> implements CommandProvider {
    private Map<CommandType, Command<T>> commands = new HashMap<>();

    @Override
    public Command getCommand(CommandType commandType) {
        return commands.get(commandType);
    }

    @Override
    public void addCommand(CommandType commandType, Command command) {
        commands.put(commandType, command);
    }
}
