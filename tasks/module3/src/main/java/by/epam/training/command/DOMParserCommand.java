package by.epam.training.command;

import by.epam.training.entity.Medicine;
import by.epam.training.parser.DOMParser;
import by.epam.training.parser.Parser;
import by.epam.training.parser.ParserException;

import java.util.List;
public class DOMParserCommand implements Command<Medicine> {

    private Parser<Medicine> parser = new DOMParser();

    @Override
    public List<Medicine> build(String path) throws CommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }
}