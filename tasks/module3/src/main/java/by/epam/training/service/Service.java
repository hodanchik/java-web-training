package by.epam.training.service;

import java.util.List;
import java.util.Optional;

public interface Service<T> {
    long add(T entity);
    boolean remove(T entity);
    Optional<T> getById(long id);
    List<T> getAll();
}

