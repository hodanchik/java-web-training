package by.epam.training.parser;

import by.epam.training.entity.Medicine;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SAXMedicineParser implements Parser<Medicine> {
    public List<Medicine> parse(String path) throws ParserException {
        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser;
        try {
            parser = parserFactor.newSAXParser();
        } catch (ParserConfigurationException | SAXException e) {
            throw new ParserException(e);
        }
        SAXHandler handler = new SAXHandler();
        try {
            parser.parse(path, handler);
        } catch (SAXException | IOException e) {
            throw new ParserException(e);
        }
        return handler.medicineList;
    }
}