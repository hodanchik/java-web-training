package by.epam.training.entity;

import java.util.List;

public class Vitamins extends Medicine {
    private boolean allowedPregnant;

    public Vitamins(boolean allowedPregnant) {
        this.allowedPregnant = allowedPregnant;
    }

    public Vitamins(long id, FormType formType, GroupType groupType, String firmName, String internationalName,
                    String pharmCompany, List<String> analog, Certificate certificate,
                    Package packageForm, Dosage dosage, boolean allowedPregnant) {
        super(id, formType, groupType, firmName, internationalName, pharmCompany, analog, certificate, packageForm, dosage);
        this.allowedPregnant = allowedPregnant;
    }

    public boolean isAllowedPregnant() {
        return allowedPregnant;
    }

    public void setAllowedPregnant(boolean allowedPregnant) {
        this.allowedPregnant = allowedPregnant;
    }
}
