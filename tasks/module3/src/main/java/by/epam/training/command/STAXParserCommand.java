package by.epam.training.command;

import by.epam.training.entity.Medicine;
import by.epam.training.parser.Parser;
import by.epam.training.parser.ParserException;
import by.epam.training.parser.STAXParserMedicine;

import java.util.List;

public class STAXParserCommand implements Command<Medicine> {
    private Parser<Medicine> parser = new STAXParserMedicine();

    @Override
    public List<Medicine> build(String path) throws CommandException {
        try {
            return parser.parse(path);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
    }
}
