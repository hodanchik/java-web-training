package by.epam.training.entity;

import java.util.List;

public abstract class Medicine {
    private long id;
    private FormType formType;
    private GroupType groupType;
    private String firmName;
    private String internationalName;
    private String pharmCompany;
    private List<String> analog;
    private Certificate certificate;
    private Package packageForm;
    private Dosage dosage;

    public Medicine() {
    }

    public Medicine(long id, FormType formType, GroupType groupType, String firmName, String internationalName,
                    String pharmCompany, List<String> analog, Certificate certificate,
                    Package packageForm, Dosage dosage) {
        this.id = id;
        this.formType = formType;
        this.groupType = groupType;
        this.firmName = firmName;
        this.internationalName = internationalName;
        this.pharmCompany = pharmCompany;
        this.analog = analog;
        this.certificate = certificate;
        this.packageForm = packageForm;
        this.dosage = dosage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FormType getFormType() {
        return formType;
    }

    public void setFormType(FormType formType) {
        this.formType = formType;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public void setGroupType(GroupType groupType) {
        this.groupType = groupType;
    }

    public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public String getInternationalName() {
        return internationalName;
    }

    public void setInternationalName(String internationalName) {
        this.internationalName = internationalName;
    }

    public String getPharmCompany() {
        return pharmCompany;
    }

    public void setPharmCompany(String pharmCompany) {
        this.pharmCompany = pharmCompany;
    }

    public List<String> getAnalog() {
        return analog;
    }

    public void setAnalog(List<String> analog) {
        this.analog = analog;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Package getPackageForm() {
        return packageForm;
    }

    public void setPackageForm(Package packageForm) {
        this.packageForm = packageForm;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "id=" + id +
                ", formType=" + formType +
                ", groupType=" + groupType +
                ", firmName='" + firmName + '\'' +
                ", internationalName='" + internationalName + '\'' +
                ", pharmCompany='" + pharmCompany + '\'' +
                ", analog=" + analog +
                ", certificate=" + certificate +
                ", packageForm=" + packageForm +
                ", dosage=" + dosage +
                '}';
    }
}
