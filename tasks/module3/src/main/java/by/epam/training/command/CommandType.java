package by.epam.training.command;

public enum CommandType {
    SAX_PARSER,
    STAX_PARSER,
    DOM_PARSER
}
