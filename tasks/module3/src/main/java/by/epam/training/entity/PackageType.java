package by.epam.training.entity;

public enum PackageType {
    BOX, BANK, TUBE, BOTTLE, AMPULE
}
