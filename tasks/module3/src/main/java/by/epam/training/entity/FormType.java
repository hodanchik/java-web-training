package by.epam.training.entity;

public enum FormType {
    TABLET, PULVIS, UNGUENT, CAPSULE, SYRUP, SOLUTION
}
