package by.epam.training.parser;

import by.epam.training.entity.Package;
import by.epam.training.entity.*;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class STAXParserMedicine implements Parser<Medicine> {
    private static final Logger LOG = Logger.getLogger(STAXParserMedicine.class);
    private List<Medicine> medicineList;
    private Medicine medicine;
    private String tagContent;
    private List<String> analog;
    private Certificate certificate;
    private Package packageMed;
    private Dosage dosage;
    private SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

    public List<Medicine> parse(String path) throws ParserException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader;
        try {
            reader = factory.createXMLStreamReader(new FileInputStream(path));
            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        try {
                            if ("nsaid".equals(reader.getLocalName())) {
                                boolean gastroprotection = Boolean.parseBoolean(reader.getAttributeValue(null, "gastroprotection"));
                                medicine = new Nsaid(gastroprotection);
                                long id = Long.parseLong(reader.getAttributeValue(null, "id"));
                                medicine.setId(id);
                                FormType formType = FormType.valueOf(reader.getAttributeValue(null, "form").toUpperCase());
                                medicine.setFormType(formType);
                                GroupType groupType = GroupType.valueOf(reader.getAttributeValue(null, "group").toUpperCase());
                                medicine.setGroupType(groupType);
                            }
                            if ("antibiotic".equals(reader.getLocalName())) {
                                boolean broadAction = Boolean.parseBoolean(reader.getAttributeValue(null, "broadAction"));
                                medicine = new Antibiotic(broadAction);
                                long id = Long.parseLong(reader.getAttributeValue(null, "id"));
                                medicine.setId(id);
                                FormType formType = FormType.valueOf(reader.getAttributeValue(null, "form").toUpperCase());
                                medicine.setFormType(formType);
                                GroupType groupType = GroupType.valueOf(reader.getAttributeValue(null, "group").toUpperCase());
                                medicine.setGroupType(groupType);
                            }
                            if ("vitamins".equals(reader.getLocalName())) {
                                boolean allowedPregnant = Boolean.parseBoolean(reader.getAttributeValue(null, "allowedPregnant"));
                                medicine = new Vitamins(allowedPregnant);
                                long id = Long.parseLong(reader.getAttributeValue(null, "id"));
                                medicine.setId(id);
                                FormType formType = FormType.valueOf(reader.getAttributeValue(null, "form").toUpperCase());
                                medicine.setFormType(formType);
                                GroupType groupType = GroupType.valueOf(reader.getAttributeValue(null, "group").toUpperCase());
                                medicine.setGroupType(groupType);
                            }
                            if ("mucolytic".equals(reader.getLocalName())) {
                                boolean compatibleWithAlcohol = Boolean.parseBoolean(reader.getAttributeValue(null, "compatibleWithAlcohol"));
                                medicine = new Vitamins(compatibleWithAlcohol);
                                long id = Long.parseLong(reader.getAttributeValue(null, "id"));
                                medicine.setId(id);
                                FormType formType = FormType.valueOf(reader.getAttributeValue(null, "form").toUpperCase());
                                medicine.setFormType(formType);
                                GroupType groupType = GroupType.valueOf(reader.getAttributeValue(null, "group").toUpperCase());
                                medicine.setGroupType(groupType);
                            }
                            if ("medicines".equals(reader.getLocalName())) {
                                medicineList = new ArrayList<>();
                            }
                            if ("analogs".equals(reader.getLocalName())) {
                                analog = new ArrayList<>();
                            }
                            if ("certificate".equals(reader.getLocalName())) {
                                certificate = new Certificate();
                            }
                            if ("package".equals(reader.getLocalName())) {
                                packageMed = new Package();
                                packageMed.setPackageType(PackageType.valueOf(reader.getAttributeValue(0).toUpperCase()));
                            }
                            if ("dosage".equals(reader.getLocalName())) {
                                dosage = new Dosage();
                                dosage.setDose(Double.parseDouble(reader.getAttributeValue(0)));
                                dosage.setMeasure(reader.getAttributeValue(1));
                            }
                        } catch (IllegalArgumentException e) {
                            throw new ParserException(e);
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        tagContent = reader.getText().trim();
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        switch (reader.getLocalName()) {
                            case "medicines":
                                break;
                            case "nsaid":
                            case "antibiotic":
                            case "vitamins":
                            case "mucolytic":
                                medicineList.add(medicine);
                                break;
                            case "analogs":
                                medicine.setAnalog(analog);
                                break;
                            case "certificate":
                                medicine.setCertificate(certificate);
                                break;
                            case "package":
                                medicine.setPackageForm(packageMed);
                                break;
                            case "dosage":
                                medicine.setDosage(dosage);
                                break;
                            case "firm_name":
                                medicine.setFirmName(tagContent);
                                break;
                            case "international_name":
                                medicine.setInternationalName(tagContent);
                                break;
                            case "pharm":
                                medicine.setPharmCompany(tagContent);
                                break;
                            case "analog":
                                analog.add(tagContent);
                                break;
                            case "number":
                                certificate.setNumber(Long.parseLong(tagContent));
                                break;
                            case "date_of_issue":
                                try {
                                    Date parsingDate = ft.parse(tagContent);
                                    certificate.setIssueDate(parsingDate);
                                } catch (ParseException e) {
                                    LOG.error("Can't parse date_of_issue", e);
                                    throw new ParserException("Can't parse date_of_issue");
                                }
                                break;
                            case "expiration_date":
                                try {
                                    Date parsingDate = ft.parse(tagContent);
                                    certificate.setExpirationDate(parsingDate);
                                } catch (ParseException e) {
                                    LOG.error("Can't parse expiration_date", e);
                                    throw new ParserException("Can't parse expiration_date");
                                }
                                break;
                            case "registry_organisation":
                                certificate.setRegistryOrganisation(tagContent);
                                break;
                            case "count":
                                packageMed.setCount(Integer.parseInt(tagContent));
                                break;
                            case "price":
                                packageMed.setPrice(Double.parseDouble(tagContent));
                                break;
                            case "currency":
                                packageMed.setMeasure(tagContent);
                                break;
                            case "frequency_of_reception":
                                dosage.setReception(tagContent);
                                break;
                            default:
                                throw new ParserException("Incorrect field end element");
                        }
                        break;
                    case XMLStreamConstants.START_DOCUMENT:
                        medicineList = new ArrayList<>();
                        break;
                }

            }
        } catch (XMLStreamException | FileNotFoundException e) {
            throw new ParserException(e);
        }
        return medicineList;
    }
}
