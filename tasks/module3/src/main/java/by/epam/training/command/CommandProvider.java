package by.epam.training.command;

public interface CommandProvider<T> {
    Command getCommand(CommandType commandType);
    void addCommand(CommandType commandType, Command<T> command);
}
