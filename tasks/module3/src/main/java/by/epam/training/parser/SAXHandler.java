package by.epam.training.parser;

import by.epam.training.entity.Package;
import by.epam.training.entity.*;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private static final Logger LOG = Logger.getLogger(SAXHandler.class);
    List<Medicine> medicineList;
    Medicine medicine = null;
    String content = null;
    List<String> analog = null;
    Certificate certificate = null;
    Package packageMed = null;
    Dosage dosage = null;
    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "medicines":
                medicineList = new ArrayList<>();
                break;
            case "nsaid":
                medicine = new Nsaid(Boolean.parseBoolean(attributes.getValue("gastroprotection")));
                medicine.setId(Long.parseLong(attributes.getValue("id")));
                medicine.setFormType(FormType.valueOf(attributes.getValue("form").toUpperCase()));
                medicine.setGroupType(GroupType.valueOf(attributes.getValue("group").toUpperCase()));
                break;
            case "antibiotic":
                medicine = new Antibiotic(Boolean.parseBoolean(attributes.getValue("broadAction")));
                medicine.setId(Long.parseLong(attributes.getValue("id")));
                medicine.setFormType(FormType.valueOf(attributes.getValue("form").toUpperCase()));
                medicine.setGroupType(GroupType.valueOf(attributes.getValue("group").toUpperCase()));
                break;
            case "vitamins":
                medicine = new Vitamins(Boolean.parseBoolean(attributes.getValue("allowedPregnant")));
                medicine.setId(Long.parseLong(attributes.getValue("id")));
                medicine.setFormType(FormType.valueOf(attributes.getValue("form").toUpperCase()));
                medicine.setGroupType(GroupType.valueOf(attributes.getValue("group").toUpperCase()));
                break;
            case "mucolytic":
                medicine = new Vitamins(Boolean.parseBoolean(attributes.getValue("compatibleWithAlcohol")));
                medicine.setId(Long.parseLong(attributes.getValue("id")));
                medicine.setFormType(FormType.valueOf(attributes.getValue("form").toUpperCase()));
                medicine.setGroupType(GroupType.valueOf(attributes.getValue("group").toUpperCase()));
                break;
            case "analogs":
                analog = new ArrayList<>();
                break;
            case "certificate":
                certificate = new Certificate();
                break;
            case "package":
                packageMed = new Package();
                packageMed.setPackageType(PackageType.valueOf(attributes.getValue("type").toUpperCase()));
                break;
            case "dosage":
                dosage = new Dosage();
                dosage.setDose(Double.parseDouble(attributes.getValue("dose")));
                dosage.setMeasure(attributes.getValue("measure"));
                break;
        }
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "medicines":
                break;
            case "nsaid":
            case "antibiotic":
            case "vitamins":
            case "mucolytic":
                medicineList.add(medicine);
                break;
            case "analogs":
                medicine.setAnalog(analog);
                break;
            case "certificate":
                medicine.setCertificate(certificate);
                break;
            case "package":
                medicine.setPackageForm(packageMed);
                break;
            case "dosage":
                medicine.setDosage(dosage);
                break;
            case "firm_name":
                medicine.setFirmName(content);
                break;
            case "international_name":
                medicine.setInternationalName(content);
                break;
            case "pharm":
                medicine.setPharmCompany(content);
                break;
            case "analog":
                analog.add(content);
                break;
            case "number":
                certificate.setNumber(Long.parseLong(content));
                break;
            case "date_of_issue":
                try {
                    Date parsingDate = ft.parse(content);
                    certificate.setIssueDate(parsingDate);
                } catch (ParseException e) {
                    LOG.error("Can't parse date_of_issue", e);
                    throw new SAXException("Can't parse date_of_issue");
                }
                break;
            case "expiration_date":
                try {
                    Date parsingDate = ft.parse(content);
                    certificate.setExpirationDate(parsingDate);
                } catch (ParseException e) {
                    LOG.error("Can't parse expiration_date", e);
                    throw new SAXException("Can't parse expiration_date");
                }
                break;
            case "registry_organisation":
                certificate.setRegistryOrganisation(content);
                break;
            case "count":
                packageMed.setCount(Integer.parseInt(content));
                break;
            case "price":
                packageMed.setPrice(Double.parseDouble(content));
                break;
            case "currency":
                packageMed.setMeasure(content);
                break;
            case "frequency_of_reception":
                dosage.setReception(content);
                break;
            default:
                throw new SAXException("Incorrect field name in entity medicine");
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content = String.copyValueOf(ch, start, length).trim();
    }
}

