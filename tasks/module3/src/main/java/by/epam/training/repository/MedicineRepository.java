package by.epam.training.repository;

import by.epam.training.entity.Medicine;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

public class MedicineRepository implements Repository<Medicine> {
    private static final AtomicLong ID = new AtomicLong(1);
    private static final Logger LOG = Logger.getLogger(MedicineRepository.class);
    private List<Medicine> medicines = new ArrayList<>();

    @Override
    public long add(Medicine entity) {
        long id = entity.getId();
        if (getById(id).isPresent()) {
            update(entity);
            return id;
        } else {
            if (id <= 0) {
                id = ID.getAndIncrement();
            }
        }
        entity.setId(id);
        medicines.add(entity);
        LOG.info("Medicine was added to repository");
        return id;
    }
    @Override
    public boolean remove(Medicine entity) {
        Optional<Medicine> optional = getById(entity.getId());
        if (optional.isPresent()) {
            optional.ifPresent(medicine -> {
                medicines.remove(medicine);
                LOG.info("Medicine was removed from repository");
            });
            return true;
        } else {
            LOG.info("Repository hasn't medicine");
            return false;
        }
    }
    @Override
    public boolean update(Medicine entity) {
        Optional<Medicine> optional = getById(entity.getId());
        if (optional.isPresent()) {
            remove(optional.get());
            medicines.add(entity);
            LOG.info("Medicine was updated");
            return true;
        }else {
            LOG.info("Repository hasn't medicine");
            return false;
        }
    }
    @Override
    public Optional<Medicine> getById(long id) {
        return medicines.stream().filter(medicine -> medicine.getId() == id)
                .findFirst();
    }
    @Override
    public List<Medicine> getAll() {
        return new ArrayList<>(medicines);
    }
}
