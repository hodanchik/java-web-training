package by.epam.training.entity;

import java.util.Date;
import java.util.Objects;

public class Certificate {
    private long number;
    private Date issueDate;
    private  Date expirationDate;
    private  String registryOrganisation;

    public Certificate() {
    }

    public Certificate(long number, Date issueDate, Date expirationDate, String registryOrganisation) {
        this.number = number;
        this.issueDate = issueDate;
        this.expirationDate = expirationDate;
        this.registryOrganisation = registryOrganisation;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getRegistryOrganisation() {
        return registryOrganisation;
    }

    public void setRegistryOrganisation(String registryOrganisation) {
        this.registryOrganisation = registryOrganisation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Certificate)) return false;
        Certificate that = (Certificate) o;
        return getNumber() == that.getNumber() &&
                getRegistryOrganisation().equals(that.getRegistryOrganisation());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNumber(), getRegistryOrganisation());
    }
}
