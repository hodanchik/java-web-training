package by.epam.training.validator;

import java.util.HashMap;
import java.util.Map;

public class ValidatorResult {
    Map<String, String> validatorResult = new HashMap<>();
    public boolean isValidate() {
        return validatorResult.isEmpty();
    }
    public void addResult(String exceptionName, String exceptionMessage) {
        validatorResult.put(exceptionName, exceptionMessage);
    }

    @Override
    public String toString() {
        if (validatorResult.isEmpty()) return "No validation errors";
        StringBuilder sb = new StringBuilder();
        for (String name:validatorResult.keySet()){
            sb.append(name).append(": ");
            String message = validatorResult.get(name);
                sb.append(message);
            }
        return sb.toString();
    }
}

