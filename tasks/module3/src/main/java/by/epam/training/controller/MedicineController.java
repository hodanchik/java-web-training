package by.epam.training.controller;

import by.epam.training.command.*;
import by.epam.training.entity.Medicine;
import by.epam.training.service.MedicineService;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.ValidatorResult;
import by.epam.training.validator.XmlValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class MedicineController {
    private static final Logger LOG = Logger.getLogger(MedicineController.class);
    private CommandProvider commandProvider;
    private FileValidator fileValidator;
    private XmlValidator xmlValidator;
    private MedicineService medicineService;
    public MedicineController(CommandProvider commandProvider, FileValidator fileValidator, XmlValidator xmlValidator,
                              MedicineService medicineService) {
        this.commandProvider = commandProvider;
        this.fileValidator = fileValidator;
        this.xmlValidator = xmlValidator;
        this.medicineService = medicineService;
    }

    public void uploadFromXml(String xmlPath, String xsdPath, CommandType commandType) {
        ValidatorResult xmlValidation = fileValidator.validateFile(xmlPath);
        ValidatorResult xsdValidation = fileValidator.validateFile(xsdPath);
        if (!xmlValidation.isValidate() | !xsdValidation.isValidate()) {
            LOG.error("XML file validation :" + xmlValidation);
            LOG.error("XSD file validation :" + xsdValidation);
            return;
        }
        ValidatorResult xmlByXsdValidation = xmlValidator.validateXmlByXsd(xmlPath);
        if (!xmlByXsdValidation.isValidate()) {
            LOG.error(xmlByXsdValidation);
            return;
        }
        try {
        Command command = commandProvider.getCommand(commandType);
        List medicineList = command.build(xmlPath);
            medicineList.forEach(entity -> medicineService.add((Medicine) entity));
        } catch (CommandException e) {
            LOG.error(e);
        }
    }
}

