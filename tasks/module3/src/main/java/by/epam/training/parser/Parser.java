package by.epam.training.parser;
import java.util.List;

public interface Parser<T> {
    List<T> parse(String xmlPath) throws ParserException;
}

