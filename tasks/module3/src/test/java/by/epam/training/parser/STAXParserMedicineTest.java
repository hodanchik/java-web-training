package by.epam.training.parser;

import by.epam.training.command.CommandException;
import by.epam.training.entity.*;
import by.epam.training.entity.Package;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

public class STAXParserMedicineTest {
    private static ArrayList<String> analogList ;
    private static ArrayList<Medicine> medicineList;
    private STAXParserMedicine staxParser = new STAXParserMedicine();
    private ClassLoader classLoader = getClass().getClassLoader();
    private String pathXml = new File(Objects.requireNonNull(classLoader.getResource("medicinesValidTwo.xml"))
            .getFile()).getAbsolutePath();
    @BeforeClass
    public static void prepareBeforeClass() {
        analogList = new ArrayList<>();
        medicineList = new ArrayList<>();
        analogList.add("Profen-express");
        analogList.add("Ibufen D");
        analogList.add("Naprofen");
        analogList.add("MIG");
        Medicine nsaid = new Nsaid(1, FormType.TABLET, GroupType.NSAID, "Ibufen", "Ibuprofen",
                "Dr.Reddy's", analogList, new Certificate(346, new Date(2017 - 04 - 07),
                new Date(2023 - 04 - 07), "National Company"), new Package(PackageType.BOX, 28,
                3.99, "USD"), new Dosage(200, "mg", "one tab twice a day"),
                false);
        medicineList.add(nsaid);
    }

    @Test
    public void parse() throws ParserException {
        List<Medicine> actualList = staxParser.parse(pathXml);
        List<Medicine> expectedList = medicineList;
        Assert.assertEquals(expectedList, actualList);
    }
}
