package by.epam.training.controller;

import by.epam.training.command.*;
import by.epam.training.entity.Medicine;
import by.epam.training.repository.MedicineRepository;
import by.epam.training.service.MedicineService;
import by.epam.training.validator.FileValidator;
import by.epam.training.validator.XmlValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class MedicineControllerTest {
    private CommandProviderImpl commandProvider;
    private FileValidator fileValidator;
    private XmlValidator xmlValidator;
    private MedicineRepository medicineRepository;
    private MedicineService medicineService;
    MedicineController medicineController;
    private ClassLoader classLoader = getClass().getClassLoader();
    String pathXsd = new File(Objects.requireNonNull(classLoader.getResource("medicines.xsd"))
            .getFile()).getAbsolutePath();
    @Before
    public void init() {
        commandProvider = new CommandProviderImpl();
        commandProvider.addCommand(CommandType.DOM_PARSER, new DOMParserCommand());
        commandProvider.addCommand(CommandType.SAX_PARSER, new SAXParserCommand());
        commandProvider.addCommand(CommandType.STAX_PARSER, new STAXParserCommand());
        fileValidator = new FileValidator();
        xmlValidator = new XmlValidator(pathXsd);
        medicineRepository = new MedicineRepository();
        medicineService = new MedicineService(medicineRepository);
        medicineController = new MedicineController(commandProvider, fileValidator, xmlValidator,
                medicineService);
    }

    @Test
    public void uploadFromXml() {
        String pathXml = new File(Objects.requireNonNull(classLoader.getResource("medicinesValid.xml"))
                .getFile()).getAbsolutePath();
        String pathXsd = new File(Objects.requireNonNull(classLoader.getResource("medicines.xsd"))
                .getFile()).getAbsolutePath();
        medicineController.uploadFromXml(pathXml, pathXsd, CommandType.DOM_PARSER);
        List<Medicine> allMedicine = medicineService.getAll();
        int actualSize = allMedicine.size();
        int expectdSize = 16;
        Assert.assertEquals(expectdSize, actualSize);
    }
}