package by.epam.training.validator;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Objects;

public class FileValidatorTest {
    private ClassLoader classLoader = getClass().getClassLoader();
    private FileValidator fileValidator = new FileValidator();
    private String pathXsd = new File(Objects.requireNonNull(classLoader.getResource("medicines.xsd"))
            .getFile()).getAbsolutePath();
    private XmlValidator xmlValidator = new XmlValidator(pathXsd);

    @Test
    public void validateFileSuccess() {
        String path = new File(Objects.requireNonNull(classLoader.getResource("medicines.xsd"))
                .getFile()).getAbsolutePath();
        ValidatorResult validatorResultSuccess = fileValidator.validateFile(path);
        ValidatorResult validatorResultExpends = new ValidatorResult();
        Assert.assertEquals(validatorResultSuccess.isValidate(), validatorResultExpends.isValidate());
    }

    @Test
    public void invalidFilePath() {
        ValidatorResult validatorResultSuccess = fileValidator.validateFile("InvalidPath.txt");
        ValidatorResult validatorResultExpends = new ValidatorResult();
        validatorResultExpends.addResult("File is no valid", "Nonexistent path to file");
        Assert.assertEquals(validatorResultSuccess.isValidate(), validatorResultExpends.isValidate());
    }

    @Test
    public void emptyInvalidFile() {
        String path = new File(Objects.requireNonNull(classLoader.getResource("empty.xml"))
                .getFile()).getAbsolutePath();
        ValidatorResult validatorResultSuccess = fileValidator.validateFile(path);
        ValidatorResult validatorResultExpends = new ValidatorResult();
        validatorResultExpends.addResult("File is no valid", "Empty file");
        Assert.assertEquals(validatorResultSuccess.isValidate(), validatorResultExpends.isValidate());
    }

    @Test
    public void validateXmlByXsdSuccess() {
        String pathXml = new File(Objects.requireNonNull(classLoader.getResource("medicinesValid.xml"))
                .getFile()).getAbsolutePath();
        ValidatorResult validatorResultSuccess = xmlValidator.validateXmlByXsd(pathXml);
        ValidatorResult validatorResultExpends = new ValidatorResult();
        Assert.assertEquals(validatorResultSuccess.isValidate(), validatorResultExpends.isValidate());
    }

    @Test
    public void InvalidValidateXmlByXsd() {
        String pathXml = new File(Objects.requireNonNull(classLoader.getResource("medicinesInvalid.xml"))
                .getFile()).getAbsolutePath();
        ValidatorResult validatorResultActual = xmlValidator.validateXmlByXsd(pathXml);
        ValidatorResult validatorResultExpends = new ValidatorResult();
        validatorResultExpends.addResult("XML Validation exception",
                "XML file " + pathXml + " is no valid by xsd: " + pathXsd);
        Assert.assertEquals(validatorResultExpends.toString(), validatorResultActual.toString());
    }
}