package by.epam.training.command;

import by.epam.training.entity.Medicine;
import org.junit.Assert;
import org.junit.Test;
import java.io.File;
import java.util.List;
import java.util.Objects;

public class SAXParserCommandTest {
    private SAXParserCommand saxParserCommand = new SAXParserCommand();
    private ClassLoader classLoader = getClass().getClassLoader();
    private String pathXml = new File(Objects.requireNonNull(classLoader.getResource("medicinesValid.xml"))
            .getFile()).getAbsolutePath();
    private String pathXmlInvalid = new File(Objects.requireNonNull(classLoader.getResource("medicinesInvalid.xml"))
            .getFile()).getAbsolutePath();
    @Test
    public void buildSuccess() throws CommandException {
        List<Medicine> buildList = saxParserCommand.build(pathXml);
        int expectedSize = 16;
        int actualSize = buildList.size();
        Assert.assertEquals(expectedSize, actualSize);
    }
    @Test(expected = CommandException.class)
    public void buildUnSuccess() throws CommandException {
        List<Medicine> buildList = saxParserCommand.build(pathXmlInvalid);
    }
}
