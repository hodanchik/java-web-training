package by.epam.training.repository;

import by.epam.training.entity.Package;
import by.epam.training.entity.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class MedicineRepositoryTest {
    private MedicineRepository medicineRepository = new MedicineRepository();
    private static Medicine nsaidOne;
    private static Medicine nsaidTwo;

    @BeforeClass
    public static void prepareBeforeClass() {
        ArrayList<String> analogList = new ArrayList<>();
        analogList.add("Profen-express");
        analogList.add("Ibufen D");
        analogList.add("Naprofen");
        analogList.add("MIG");
        nsaidOne = new Nsaid(1, FormType.TABLET, GroupType.NSAID, "Ibufen", "Ibuprofen",
                "Dr.Reddy's", analogList, new Certificate(346, new Date(2017 - 04 - 07),
                new Date(2023 - 04 - 07), "National Company"), new Package(PackageType.BOX, 28,
                3.99, "USD"), new Dosage(200, "mg", "one tab twice a day"),
                false);
        nsaidTwo = new Nsaid(2, FormType.CAPSULE, GroupType.NSAID, "MIG", "Ibuprofen",
                "Berlin Chemi", analogList, new Certificate(132, new Date(2019 - 04 - 04),
                new Date(2022 - 04 - 04), "National Company"), new Package(PackageType.BOX, 30,
                4.87, "USD"), new Dosage(400, "mg", "one tab once a day"),
                false);
    }

    @Test
    public void addTwoMedicine() {
        medicineRepository.add(nsaidOne);
        medicineRepository.add(nsaidTwo);
        int repoSize = 2;
        int actualSize = medicineRepository.getAll().size();
        Assert.assertEquals(repoSize, actualSize);
    }

    @Test
    public void removeOneMedicine() {
        medicineRepository.add(nsaidOne);
        medicineRepository.add(nsaidTwo);
        medicineRepository.remove(nsaidOne);
        int repoSize = 1;
        int actualSize = medicineRepository.getAll().size();
        Assert.assertEquals(repoSize, actualSize);
    }

    @Test
    public void update() {
        medicineRepository.add(nsaidOne);
        medicineRepository.add(nsaidTwo);
        String updateName = "New-Super-Tablet";
        nsaidTwo.setFirmName(updateName);
        medicineRepository.update(nsaidTwo);
        String actualUpdateName = medicineRepository.getById(2).get().getFirmName();
        Assert.assertEquals(updateName, actualUpdateName);
    }

    @Test
    public void getById() {
        medicineRepository.add(nsaidOne);
        medicineRepository.add(nsaidTwo);
        Medicine actualMedicine = medicineRepository.getById(2).get();
        Assert.assertEquals(nsaidTwo, actualMedicine);
    }

    @Test
    public void getAll() {
        medicineRepository.add(nsaidOne);
        medicineRepository.add(nsaidTwo);
        List<Medicine> expectedRepo = new ArrayList<>();
        expectedRepo.add(nsaidOne);
        expectedRepo.add(nsaidTwo);
        List<Medicine> actualRepo = medicineRepository.getAll();
        Assert.assertEquals(expectedRepo, actualRepo);
    }
}